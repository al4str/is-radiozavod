<section class="merchandise">
	<div class="fixed-block">
		<div class="island merchandise__island">
			<header class="island__header">
				<p class="island__title">Продукция радиозавода "Юпитер"</p>
			</header>
			<div class="table merchandise__table">
				<div class="table__wrp">
					<div class="table__r table__head merchandise__table-head">
						<div class="table__c table__h merchandise__table-cell merchandise__icon-cell">Символ</div>
						<div class="table__c table__h merchandise__table-cell merchandise__name-cell">Наименование</div>
						<div class="table__c table__h merchandise__table-cell merchandise__code-cell">Кодовое имя</div>
						<div class="table__c table__h merchandise__table-cell merchandise__done-cell">Произведено</div>
						<div class="table__c table__h merchandise__table-cell merchandise__demand-cell">Требуется</div>
					</div>
					{{for product in this['merchandise'].production :}}
					<div class="table__r merchandise__table-row">
						<div class="table__c merchandise__table-cell merchandise__icon-cell">
							<!--suppress HtmlUnknownTarget -->
							<img class="merchandise__product" src="graphics-product-{{-product.icon}}.svg" alt="{{-product.name}}" />
						</div>
						<div class="table__c merchandise__table-cell merchandise__name-cell">{{-product.name}}</div>
						<div class="table__c merchandise__table-cell merchandise__code-cell">"{{-product.nickname}}"</div>
						<div class="table__c merchandise__table-cell merchandise__done-cell">{{-product.done}} шт.</div>
						<div class="table__c merchandise__table-cell merchandise__demand-cell">{{-product.on_demand}} шт.</div>
					</div>
					{{end}}
				</div>
			</div>
		</div>
	</div>
</section>