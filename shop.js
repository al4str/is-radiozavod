const m = {};

m.path = require('path');
m.diet = require('diet');
m.opn = require('opn');

m.diet_render = require('./diet-render');
m.diet_static = require('./diet-static');
m.db = require('./db');


String.prototype.sql_date = function() {
	let day, month, year, split = this.split('-');

	if (split.length !== 3) {
		console.warn('String.sql_date:', 'not a sql date string');
		return false;
	}
	day = parseInt(split[0]);
	month = Date.prototype.sql_months.indexOf(split[1]);
	month += 1;
	year = parseInt(split[2]);
	if (isNaN(day) || month === -1 || isNaN(year)) {
		console.warn('String.sql_date:', 'not a valid sql date string');
		return false;
	}
	return new Date(year + '/' + month + '/' + day + ' 12:00:00');
};
Date.prototype.sql_months = [
	'JAN', 'FEB', 'MAR', 'APR',
	'MAY', 'JUN', 'JUL', 'AUG',
	'SEP', 'OCT', 'NOV', 'DEC'
];
Date.prototype.sql_date = function() {
	var day, month, year;

	day = this.getDate();
	if (day < 10) day = '0' + day;
	month = Date.prototype.sql_months[this.getMonth()];
	year = this.getFullYear();
	return day + '-' + month + '-' + year;
};
Date.prototype.string_date = function() {
	var day, month, year;

	day = this.getDate();
	month = this.getMonth()+1;
	year = this.getFullYear();
	if (day < 10) day = '0' + day;
	if (month < 10) month = '0' + month;
	return day + '.' + month + '.' + year;
};


const app = {};

app.const = {};
app.server = m.diet();
if (!app.server.path) app.server.path = module.path;
app.const.app_config = require('./shop_config.json');
app.q = m.db;
app.server.listen(app.const.app_config.host);
app.render = m.diet_render({
	path: app.server.path + '/views/html/',
	cache: false
});


app.static = m.diet_static({
	path: m.path.join(app.server.path, '/views/html/')
});
app.server.header(function($){
	console.info('shop.js', 'request for', $.url.href);
	$.stop = stop.bind(this, $);
	$.return();
});
app.server.header(app.render);
app.server.footer(app.static);


function stop($) {
	$.status('500', 'Server Error');
	$.end(`${$.statusCode} ${$.statusMessage}\n${$.stop_message}`);
}


app.server.missing(function($){
	console.info('shop.js', 'universal hook missing url', $.url.href);
	if ($.method.toLowerCase() == 'get' && !m.path.extname($.url.pathname)) {
		$.redirect('/404/');
	}
	else {
		$.status('404', 'Page Not Found');
		$.end($.statusCode + ' ' + $.statusMessage);
	}
});
app.server.get('/', function($) {
	console.info('shop.js', 'get hook for /');
	$.redirect('/shop/');
});
app.server.get('/shop/', function($) {
	console.info('shop.js', 'get hook for /shop/');
	$.data['html-head'] = {
		title: 'Интернет-магазин Радиозавод "Юпитер"'
	};
	$.data['order-form'] = {
		production: []
	};
	app.q(false, {
		query: `
		USE JUPITER;\n
		SELECT ID, NAME, NICKNAME FROM PRODUCTION
		`
	})
	.catch((errors) => {
		$.stop_message = errors;
		$.stop();
	})
	.then((result) => {
		if (!result) {
			$.stop_message = 'Bad T-SQL query: DB response is empty';
			$.stop();
			return;
		}
		result.map((value) => {
			let production = $.data['order-form'].production;

			production.push({
				id: value['ID'],
				name: value['NAME'],
				nickname: value['NICKNAME']
			});
		});
		$.html('shop/index.html');
	});
});
app.server.get('/404/', function($) {
	console.info('shop.js', 'get hook for /404/');
	$.data['html-head'] = {
		title: 'Страница не найдена'
	};
	$.html('404/index.html');
});

app.server.post('/shop/', function($) {
	console.info('shop.js', 'post hook for /shop/');
	console.log($.body);
	let response_error = {
			result: 'error',
			response: [
				{
					code: 500,
					name: 'default',
					description: 'Не передан ORDER_ID'
				}
			]
		},
		response_empty_fields = {
			result: 'error',
			response: [
				{
					code: 500,
					name: 'empty_fields',
					description: []
				}
			]
		},
		response_least_one_item = {
			result: 'error',
			response: [
				{
					code: 500,
					name: 'least_one_item',
					description: 'Должен быть заполнен хотя бы один продукт'
				}
			]
		},
		response_no_more_400 = {
			result: 'error',
			response: [
				{
					code: 500,
					name: 'no_more_400',
					description: 'Кол-во продукции не должно превышать 400'
				}
			]
		},
		response_no_more_200 = {
			result: 'error',
			response: [
				{
					code: 500,
					name: 'no_more_200',
					description: 'Кол-во продукции не должно превышать 200'
				}
			]
		},
		response_no_more_20 = {
			result: 'error',
			response: [
				{
					code: 500,
					name: 'no_more_20',
					description: 'Кол-во продукции не должно превышать 20'
				}
			]
		},
		response_success = {
			result: 'success',
			response: []
		};

	let required_fields = ['NAME', 'ADDRESS', 'CONTACTS'];
	for (let field_name in $.body) {
		if (!$.body.hasOwnProperty(field_name)) continue;
		let field_value = $.body[field_name];

		if (required_fields.indexOf(field_name) === -1) continue;
		if (!field_value || ('' + field_value).length == 0) {
			response_empty_fields.response[0].description.push(field_name);
		}
	}
	if (response_empty_fields.response[0].description.length > 0) {
		$.json(response_empty_fields);
		return;
	}

	let PURCHASED_ITEMS_INSERTS = ``;
	let SUM = 0;

	for (let product_id in $.body['PURCHASED_ITEMS']) {
		if (!$.body['PURCHASED_ITEMS'].hasOwnProperty(product_id)) continue;
		let quantity = $.body['PURCHASED_ITEMS'][product_id];

		if (!(quantity > 0)) continue;
		SUM += parseInt(quantity);
		PURCHASED_ITEMS_INSERTS += `INSERT PURCHASED_ITEMS VALUES (@ORDER_ID, ${product_id}, ${quantity});\n`;
	}
	/*$.body['PURCHASED_ITEMS'].map((quantity, product_id) => {
		if (!(quantity > 0)) return;
		PURCHASED_ITEMS_INSERTS += `INSERT PURCHASED_ITEMS VALUES (@ORDER_ID, ${product_id}, ${quantity});\n`;
	});*/
	if (PURCHASED_ITEMS_INSERTS == ``) {
		$.json(response_least_one_item);
		return;
	}
	if ($.body['TYPE'] == 'company' && SUM > 400) {
		$.json(response_no_more_400);
		return;
	}
	else if ($.body['TYPE'] == 'individual' && SUM > 200) {
		$.json(response_no_more_200);
		return;
	}
	else if ($.body['TYPE'] == 'e-shop' && SUM > 20) {
		$.json(response_no_more_20);
		return;
	}
	app.q(false, {
		query: `
		USE JUPITER;\n
		DECLARE @CLIENT_ID_TABLE TABLE (ID int);\n 
		INSERT CLIENTS OUTPUT INSERTED.ID INTO @CLIENT_ID_TABLE VALUES ('${$.body['TYPE']}', '${$.body['NAME']}', '${$.body['ADDRESS']}', '${$.body['CONTACTS']}', DEFAULT, DEFAULT, DEFAULT);\n 
		DECLARE @ORDER_ID_TABLE TABLE (ID int);\n 
		DECLARE @ORDER_ID int;\n 
		INSERT ORDERS OUTPUT INSERTED.ID INTO @ORDER_ID_TABLE VALUES ((SELECT ID FROM @CLIENT_ID_TABLE), DEFAULT, DEFAULT);\n 
		SELECT @ORDER_ID = (SELECT ID FROM @ORDER_ID_TABLE);\n 
		${PURCHASED_ITEMS_INSERTS} 
		INSERT SCHEDULE VALUES (@ORDER_ID, NULL, DEFAULT, DEFAULT); 
		`
	})
	.catch((errors) => {
		$.stop_message = errors;
		$.stop();
	})
	.then(() => {
		$.json(response_success);
	});
});

m.opn(app.const.app_config.host + '/');