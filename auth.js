const m = {};

m.path = require('path');
m.db = require('./db');

function Auth(signal) {
	this.$ = signal;
	//noinspection JSUnresolvedVariable
	this.token = this.$.cookies['token'] || null;
	console.info('auth.js', this.$.url.href, (!this.token)?'not authed':'authed');
	if (this.$.cookies === undefined) {
		console.error('auth.js', 'cookie manager is undefined');
		return {};
	}
	return this;
}

Auth.prototype.is_authed = function() {
	var is_authed = false;
	if (this.token == 1337) is_authed = true;
	console.info('auth.js', this.$.url.href, 'user is', (!is_authed)?'not authed':'authed');
	return is_authed;
};

Auth.prototype.log_in = function() {
	console.log(this.$.body);
	//this.token = 1337;
	//this.$.cookies.set('token', this.token, {httpOnly: true});
};

Auth.prototype.log_out = function() {
	this.token = null;
	this.$.cookies.delete('token');
};

module.exports = function($) {
	if (!m.path.extname($.url.pathname)) {
		$.auth = new Auth($);
	}
	$.return();
};