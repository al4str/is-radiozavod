<section class="page-not-found">
	<div class="fixed-block row row__ou-xs_md about">
		<div class="row__col-xs-12 row__col-sm-12 row__col-md-12 row__col-lg-12 row__col-hg-12">
			<div class="page-not-found__wrp">
				<h1 class="h1 page-not-found__page-title">Страница не найдена</h1>
				<i class="page-not-found__icon">
					<svg class="page-not-found__svg" viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg">
						<g fill="none" stroke="inherit" stroke-miterlimit="10" stroke-width="2">
							<circle cx="36" cy="36" r="35"></circle>
							<line x1="70.65" y1="1.35" x2="1.35" y2="70.65"></line>
						</g>
					</svg>
				</i>
				<p class="page-not-found__text">Проверьте корректность введенного адреса или попробуйте перейти на главную страницу</p>
				<!--suppress HtmlUnknownTarget -->
				<a class="link link_btn link_btn_color_green page-not-found__link" href="/">
					<span class="link__cap">На главную страницу</span>
				</a>
			</div>
		</div>
	</div>
</section>