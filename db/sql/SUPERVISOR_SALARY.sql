CREATE TABLE SUPERVISOR_SALARY (
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	SUPERVISOR_ID int NOT NULL FOREIGN KEY
		REFERENCES SUPERVISORS (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	PAYDAY smalldatetime NOT NULL,
	EFFORT_FROM_WORKERS int DEFAULT 0,
	BONUS_FROM_WORKERS int DEFAULT 0,
	FIXED_RATE int DEFAULT 0,
	CURRENT_SALARY AS (EFFORT_FROM_WORKERS + BONUS_FROM_WORKERS + FIXED_RATE)
);