App.c.blocks.Dashboard = function() {
	var self = this;

	self.opt = {};
	self.dom = {};
	self.render = function() {
		App.h.i(self.dom.$block.querySelectorAll('.dashboard__chart'), function($node){
			var max_amount = 0, items = [];

			App.h.i($node.querySelectorAll('.dashboard__chart-item'), function($item){
				var item = {};

				item.$node = $item;
				item.$bar = item.$node.querySelector('.dashboard__chart-bar');
				item.amount = parseInt(item.$node.querySelector('.dashboard__chart-amount').textContent);
				if (isNaN(item.amount)) item.amount = 0;
				if (item.amount > max_amount) max_amount = item.amount;
				items.push(item);
			});
			App.h.i(items, function(item){
				var height;

				height = 100*item.amount/max_amount;
				if (isNaN(height) || height < 0) height = 0;
				if (height > 100) height = 100;
				item.$bar.style.height = height + '%';
			});
		});
	};
	self.initiate = function() {
		self.dom.$block = App.d.d.querySelector('.dashboard');
		if (!self.dom.$block) {
			self = null;
			return false;
		}
		self.render();
	};
	self.initiate();
	return {self: self};
};