/**
 * Элемент для работы с модальными окнами
 *
 * @namespace App.c.tools
 * @class CalendarBlock
 * @returns {object}
 */
App.c.tools.CalendarBlock = function($block) {
	var self = this;

	self.dom = {};
	self.opt = {};
	self.disable = function() {
		self.dom.$native_field.disabled = true;
		self.dom.$field.disabled = true;
		self.dom.$btn.disabled = true;
		App.h.trigger(self.dom.$block, 'is-disabled');
	};
	self.enable = function() {
		self.opt.is_initialy_disabled = false;
		if (isMobile.any) {
			self.dom.$native_field.disabled = false;
			self.dom.$field.disabled = true;
		} else {
			self.dom.$field.disabled = false;
			self.dom.$native_field.disabled = false;
		}
		self.dom.$btn.disabled = false;
		App.h.trigger(self.dom.$block, 'is-enabled');
	};
	self.if_mobile = function() {
		self.dom.$native_field.disabled = false;
		self.dom.$native_field.tabIndex = 0;
		self.dom.$native_field.style.display = 'block';
		self.dom.$field.disabled = true;
		self.dom.$field.tabIndex = -1;
		self.dom.$field.style.display = 'none';
	};
	self.if_desktop = function() {
		self.dom.$field.disabled = false;
		self.dom.$native_field.disabled = false;
		self.dom.$native_field.tabIndex = -1;
		self.dom.$native_field.style.display = 'none';
	};
	self.initiate_calendar = function() {
		if (self.dom.$native_field.value.trim() != '') {
			self.opt.prefilled_native_value = self.dom.$native_field.value.trim();
		}
		self.opt.calendar = new Pikaday({
			field: self.dom.$field,
			onSelect: function() {
				self.dom.$field.value = App.h.get_date_string(this.getDate());
				self.dom.$native_field.value = App.h.get_date_input_string(this.getDate());
				if (!self.opt.is_initialy_disabled) App.h.trigger(self.dom.$block, 'is-changed');
			},
			firstDay: 1,
			i18n: {
				previousMonth : '<',
				nextMonth : '>',
				months : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
				weekdays : ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
				weekdaysShort : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
			}
		});
		if (self.opt.prefilled_native_value != '') {
			self.opt.calendar.setDate(new Date(self.opt.prefilled_native_value));
		} else {
			self.opt.calendar.setDate(new Date());
			self.dom.$native_field.value = '';
			self.dom.$field.value = '';
		}
	};
	self.clear_field = function() {
		if (self.opt.calendar && typeof self.opt.calendar.setDate === 'function') self.opt.calendar.setDate(new Date());
		self.opt.prefilled_native_value = '';
		self.dom.$native_field.value = '';
		self.dom.$field.value = '';
	};
	self.render = function() {
		self.opt.calendar = null;
		self.opt.prefilled_native_value = '';
		self.dom.$native_field = self.dom.$block.querySelector('.calendar-block__native-field');
		self.dom.$field = self.dom.$block.querySelector('.calendar-block__field');
		self.dom.$btn = self.dom.$block.querySelector('.calendar-block__btn');
		self.dom.$btn.onclick = self.clear_field;
		self.opt.is_initialy_disabled = self.dom.$native_field.disabled;
		if (isMobile.any) {
			self.if_mobile();
		} else {
			self.if_desktop();
			self.initiate_calendar();
		}
		if (self.opt.is_initialy_disabled) self.disable();
		self.dom.$block.js = self;
		App.h.trigger(self.dom.$block, 'is-ready');
	};
	self.initiate = function() {
		self.dom.$block = $block;
		if (!self.dom.$block) {
			self = null;
			return false;
		}
		self.render();
	};
	self.initiate();
	return self;
};