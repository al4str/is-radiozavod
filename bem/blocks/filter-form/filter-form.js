/**
 * Фильтр для результатов выдачи
 *
 * @namespace App.c.blocks
 * @class FilterForm
 * @returns {object}
 */
App.c.blocks.FilterForm = function() {
	var self = this;

	self.opt = {};
	self.dom = {};
	self.render = function() {
		var form = {};
		
		form.opt = {};
		form.dom = {};
		form.dom.$node = this;
		form.dom.$distance_block = form.dom.$node.querySelector('.distance-block');
		form.opt.distane_block = new App.c.tools.DistanceBlock(form.dom.$distance_block);
		form.dom.$node.js = form;
	};
	self.initiate = function() {
		self.dom.$blocks = App.d.d.querySelectorAll('.filter-form');
		if (!self.dom.$blocks || self.dom.$blocks.length == 0) {
			self = null;
			return false;
		}
		App.h.i(self.dom.$blocks, function($node){
			self.render.call($node);
		});
	};
	self.initiate();
	return {self: self};
};