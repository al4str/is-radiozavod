<section class="stats">
	<div class="fixed-block stats__wrp">
		<div class="island stats__island">
			<p class="stats__value">250</p>
			<p class="stats__desc">надежных посредников по всей стране</p>
		</div>
		<div class="island stats__island">
			<p class="stats__value">617</p>
			<p class="stats__desc">отличных поставщиков, готовых к работе</p>
		</div>
		<div class="island stats__island">
			<p class="stats__value">10</p>
			<p class="stats__desc">видов продукции, которой мы гордимся</p>
		</div>
		<div class="island stats__island">
			<p class="stats__value">9000</p>
			<p class="stats__desc">довольных клиетов и радиолюбителей</p>
		</div>
	</div>
</section>