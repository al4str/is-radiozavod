<div class="pagination" data-sim="if(NOT $data.nav ? ignore())">
	<div class="pagination__wrp" data-sim="foreach($data.nav as $nav)">
		<a class="link link_btn pagination__prev-link" data-sim="if($nav.index NE 0 ? ignore()); if(NOT $data.arrows.prev.link ? attributes(data-disabled,'')); if($data.arrows.prev.link ? attributes(href,$data.arrows.prev.link));">
			<span class="link__cap"></span>
			<i class="link__icon">
				<svg class="link__svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
					<g fill="none" stroke="inherit" stroke-width="1">
						<path d="M12,1 L4,8 L12,15"></path>
					</g>
				</svg>
			</i>
		</a>
		<p class="pagination__text" data-sim="if($nav.item.link ? ignore()); if(NOT $nav.item.link ? content($nav.item.name));">1</p>
		<a class="link link_btn pagination__page-link" href="javascript:void(0);" data-sim="if(NOT $nav.item.link ? ignore()); if($nav.item.link ? attributes(href,$nav.item.link));">
			<span class="link__cap" data-sim="content($nav.item.name)">2</span>
		</a>
		<a data-sim="ignore()" class="link link_btn pagination__page-link" href="javascript:void(0);">
			<span class="link__cap">3</span>
		</a>
		<p data-sim="ignore()" class="pagination__text">...</p>
		<a data-sim="ignore()" class="link link_btn pagination__page-link" href="javascript:void(0);">
			<span class="link__cap">13</span>
		</a>
		<a class="link link_btn pagination__next-link" data-sim="if($nav.index NE $data.end ? ignore()); if(NOT $data.arrows.next.link ? attributes(data-disabled,'')); if($data.arrows.next.link ? attributes(href,$data.arrows.next.link));">
			<span class="link__cap"></span>
			<i class="link__icon">
				<svg class="link__svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
					<g fill="none" stroke="inherit" stroke-width="1">
						<path d="M4,1 L12,8 L4,15"></path>
					</g>
				</svg>
			</i>
		</a>
	</div>
</div>