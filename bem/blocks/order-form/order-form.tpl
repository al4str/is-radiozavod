<!--suppress HtmlUnknownTarget -->
<form class="order-form" action="/shop/" method="post" autocomplete="off">
	<div class="fixed-block order-form__wrp">
		<p class="order-form__title">Оформление заказа</p>
		<div class="field-block field-block_inline order-form__block">
			<p class="field-block__cap order-form__cap">Выберите тип организации</p>
			<div class="field-block__grp">
				<div class="select-block order-type">
					<label class="select-block__label" tabindex="-1">
						<select class="field select-block__field" name="TYPE" tabindex="-1">
							<option value="company" selected>ООО</option>
							<option value="individual">ИП</option>
							<option value="e-shop">Физ. лицо</option>
						</select>
					</label>
					<button class="btn select-block__btn" type="button">
						<span class="btn__cap select-block__btn-cap">ООО</span>
						<i class="btn__icon select-block__btn-icon">
							<svg class="btn__svg" viewBox="0 0 14 8" xmlns="http://www.w3.org/2000/svg">
								<g fill="none" stroke="inherit" stroke-width="1">
									<path d="M0,-1 L7,7 L14,-1"></path>
								</g>
							</svg>
						</i>
					</button>
				</div>
			</div>
		</div>
		<div class="field-block field-block_inline order-form__block order-name-corp">
			<p class="field-block__cap order-form__cap">Наименование</p>
			<div class="field-block__grp">
				<input class="field" type="text" value="" name="NAME" placeholder="" />
			</div>
		</div>
		<div class="field-block field-block_inline order-form__block order-name-fio" style="display:none;">
			<p class="field-block__cap order-form__cap">Фамилия Имя Отчество</p>
			<div class="field-block__grp">
				<input class="field" type="text" disabled name="NAME" placeholder="" />
			</div>
		</div>
		<div class="field-block field-block_inline order-form__block">
			<p class="field-block__cap order-form__cap">Адрес</p>
			<div class="field-block__grp">
				<input class="field" type="text" value="" name="ADDRESS" placeholder="" />
			</div>
		</div>
		<div class="field-block field-block_inline order-form__block">
			<p class="field-block__cap order-form__cap">Контактные данные (e-mail)</p>
			<div class="field-block__grp">
				<input class="field" type="text" value="" name="CONTACTS" placeholder="" />
			</div>
		</div>
		<div class="order-form__bottom">
			<p class="order-form__bottom-title">Ваш заказ</p>
		</div>
		<ul class="order-form__list">
			{{for product in this['order-form'].production :}}
			<li class="order-form__item order-form__block">
				<div class="order-form__item-cell order-form__name-cell">{{-product.name}}</div>
				<div class="order-form__item-cell order-form__code-cell">"{{-product.nickname}}"</div>
				<div class="order-form__item-cell order-form__amount-cell">
					<div class="field-grp field-grp_amount">
						<p class="field-grp__postfix">шт.</p>
						<input class="field field-grp__field" type="tel" value="0" name="PURCHASED_ITEMS[{{-product.id}}]" placeholder="" autocomplete="off" />
					</div>
				</div>
			</li>
			{{end}}
		</ul>
		<div class="form-messages order-form__messages" data-type="" data-is-hidden>
			<p class="form-messages__msg" data-type="success" data-id="default">Изменения сохранены</p>
			<p class="form-messages__msg" data-type="error" data-id="default">Произошла ошибка</p>
			<p class="form-messages__msg" data-type="error" data-id="empty_fields">Все поля обязательны к заполнению</p>
			<p class="form-messages__msg" data-type="error" data-id="least_one_item">Должен быть заполнен хотя бы один продукт</p>
			<p class="form-messages__msg" data-type="error" data-id="no_more_400">Кол-во продукции не должно превышать 400</p>
			<p class="form-messages__msg" data-type="error" data-id="no_more_200">Кол-во продукции не должно превышать 200</p>
			<p class="form-messages__msg" data-type="error" data-id="no_more_20">Кол-во продукции не должно превышать 20</p>
		</div>
		<div class="order-form__actions">
			<p class="order-form__cost">Предварительная стоимость заказа: <span class="order-cost">0 руб.</span></p>
			<button class="btn btn_size_lg btn_color_green order-form__submit-btn" data-url="/shop/" data-params='{}' type="button">
				<span class="btn__cap">Отправить заявку</span>
			</button>
		</div>
	</div>
</form>