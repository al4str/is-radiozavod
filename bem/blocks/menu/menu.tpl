<nav class="menu">
{{for link in this['menu'].links :}}
	<!--suppress HtmlUnknownTarget, HtmlUnknownAttribute -->
	<a class="link menu__link" href="{{-link.href}}" data-active="{{-link.is_active}}">
		<span class="link__cap menu__link-cap">{{-link.cap}}</span>
	</a>
{{end}}
</nav>