/**
 * Отвечает за большую часть форм на сайте
 * Связь фронт-энда форм и сервера, обработка ошибок и типовая отправка данных
 *
 * @namespace App.c.tools
 * @class FormHandler
 * @returns {object}
 */
App.c.tools.FormHandler = function(options) {
	var self = this;

	self.opt = {};
	self.dom = {};
	/**
	 * Реакция на ответ сервера
	 * Проверка на статус ответа
	 * Вызов соответсвующих реакций в зависимости от типа ответа
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class on_form_response
	 *
	 * @param {string} string — Входящая json-строка
	 * @returns {boolean}
	 */
	self.on_form_response = function(string) {
		//console.info('FormHandler:on_form_response');
		
		self.opt.is_submitting = false;
		var response = App.h.get_json(string);

		//console.info('FormHandler:on_form_response:response', response);
		if (!response) {
			console.error('FormHandler: сервер ответил невалидной json строкой');
			App.h.trigger(self.dom.$block, 'fh:error', {error_id: null});
			return false;
		}
		if (response['result'] == 'error') {
			var error_ids = response['response'],
				first_error_id = error_ids[0];

			if (first_error_id) {
				if (first_error_id['description'] && first_error_id['description'].length > 0) self.show_error(first_error_id['description']);
				App.h.trigger(self.dom.$block, 'fh:error', {
					error_id: first_error_id['name'],
					error_code: first_error_id['code']
				});
			} else App.h.trigger(self.dom.$block, 'fh:error', {error_id: null});
		} else
		if (response['result'] == 'success') {
			App.h.trigger(self.dom.$block, 'fh:success', response);
		} else {
			App.h.trigger(self.dom.$block, 'fh:error', {error_id: null});
		}
	};
	/**
	 * Реакция на отправку формы
	 * Проверка входящих параметров
	 * Подготовка дополнительных параметров для отправки
	 * Отправка данных на сервер
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class on_form_submit
	 *
	 * @param {object} e — Объект события
	 * @returns {boolean}
	 */
	self.on_form_submit = function(e) {
		//console.info('FormHandler:on_form_submit', e['detail']);

		if (!e || !e['detail'] || !e['detail']['url']) {
			console.error('FormHandler: при сабмите формы не были переданы необходимые параметры', self.dom.$block);
			return false;
		}
		if (self.opt.is_submitting) return false;
		self.opt.is_submitting = true;
		self.on_submit();
		var method = e['detail']['method'] || 'POST',
			form_data = {};

		App.h.i(e['detail']['params'], function(value, param){
			form_data[param] = value;
		});
		App.xhr.json(e['detail']['url'], self.on_form_response, form_data);
	};
	/**
	 * Реакция на неуспешных ответ сервера
	 * Инициализация показа сообщения об ошибке
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class on_form_errors
	 *
	 * @param {object} e — Объект события
	 */
	self.on_form_errors = function(e) {
		//console.info('FormHandler:on_form_errors', e['detail']);
		self.on_response();
		if (self.opt.messages) {
			self.opt.messages.show({
				type: 'error',
				id: e['detail']['error_id'],
				code: e['detail']['error_code']
			});
		}
	};
	/**
	 * Реакция на успешный ответ сервера
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class on_form_success
	 */
	self.on_form_success = function() {
		//console.info('FormHandler:on_form_success');
		self.on_response();
	};
	/**
	 * Вешает индикатор загрузки на кнопку сабмита
	 * Очищает поля от подстветки красным
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class on_submit
	 */
	self.on_submit = function() {
		if (self.dom.$submit_btn) App.h.load_node(self.dom.$submit_btn);
		self.clear_errors();
	};
	/**
	 * Убирает индикатор загрузки с кнопки сабмита
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class on_response
	 */
	self.on_response = function() {
		if (self.dom.$submit_btn) App.h.load_node(self.dom.$submit_btn, true);
	};
	/**
	 * Начинает загрузку файла на сервер
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class upload
	 *
	 * @param {object} e — Объект события
	 * @returns {boolean}
	 */
	self.upload = function(e) {
		if (!e || !e['detail'] || !e['detail']['url']) {
			console.error('FormHandler: при сабмите формы не были переданы необходимые параметры', self.dom.$block);
			return false;
		}
		if (self.opt.is_submitting) return false;
		self.opt.is_submitting = true;
		self.on_submit();
		var method = e['detail']['method'] || 'POST',
			form_data = new FormData(self.dom.$block);

		App.h.i(e['detail']['params'], function(value, param){
			form_data.append(param, value);
		});
		App.xhr.upload(e['detail']['url'], form_data, method, self.on_form_response, e['detail']['on_progress']);
	};
	/**
	 * Убирает подсветку полей красным
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class clear_errors
	 */
	self.clear_errors = function() {
		//console.info('FormHandler:clear_errors');
		if (self.opt.messages) self.opt.messages.hide();
		App.h.i(self.opt.error_fields_cache, function($field){
			$field.classList.remove('field_is-error');
		});
		self.opt.error_fields_cache = {};
	};
	/**
	 * Подсвечивает необходимые поля красным
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class show_error
	 *
	 * @param {object} field_names — Список полей
	 */
	self.show_error = function(field_names) {
		self.opt.error_fields_cache = {};
		App.h.i(field_names, function(field_name){
			var $field = self.dom.$block.querySelector('[name=' + field_name + ']');
			
			if ($field) {
				$field.classList.add('field_is-error');
				self.opt.error_fields_cache[field_name] = $field;
			}
		});
	};
	/**
	 * Инициализация логики компонента
	 * Кеширование необходимых входящих опций
	 * Подписка на события формы
	 * Дефолтные опции
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class render
	 */
	self.render = function() {
		//console.info('FormHandler:render', options);
		self.dom.$block = options['$form'];
		self.dom.$submit_btn = options['$submit_btn'];
		self.dom.$messages = options['$messages'];
		self.opt.messages = (self.dom.$messages)?new App.c.tools.FormMessages(self.dom.$messages):null;
		self.dom.$block.addEventListener('fh:error', self.on_form_errors, false);
		self.dom.$block.addEventListener('fh:success', self.on_form_success, false);
		self.dom.$block.addEventListener('fh:submit', self.on_form_submit, false);
		self.opt.is_submitting = false;
		self.is_ready = true;
	};
	/**
	 * Инициализация компонента
	 * Простейшая проверка на наличие входящих опций
	 *
	 * @namespace App.c.tools.FormHandler
	 * @class initiate
	 *
	 * @returns {boolean}
	 */
	self.initiate = function() {
		if (!options || !options['$form']) {
			self.is_ready = null;
			return false;
		}
		self.render();
	};
	self.initiate();
	return self;
};