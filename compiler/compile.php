<?php
// Возвращает массив настроек выполнения сборки
// $file_name - имя конфигурационного файла
function load_configuration_file($file_name = false) {
	if (!$file_name) {
		echo 'load_configuration_file: configuration file name is not defined' . "\n";
		exit(1);
	}
	if (!file_exists($file_name)) {
		echo 'load_configuration_file: configuration file does not exist' . "\n";
		exit(1);
	}
	$config = json_decode(file_get_contents($file_name), true);
	if (!isset($config)) {
		echo 'load_configuration_file: configuration file contains invalid json' . "\n";
		exit(1);
	}
	return $config;
}
// Возвращает текущее задание для сборки
// $file_name - имя файла задания
function load_task_file($file_name = false) {
	if (!$file_name) {
		echo 'load_task_file: task file name is not defined' . "\n";
		exit(1);
	}
	if (!file_exists($file_name)) {
		echo 'load_task_file: task file does not exist' . "\n";
		exit(1);
	}
	$task = json_decode(file_get_contents($file_name), true);
	if (!isset($task)) {
		echo 'load_task_file: task file contains invalid json' . "\n";
		exit(1);
	}
	return $task;
}
// Обработка _REQUEST и входных параметров
function handle_tasks() {
	if (isset($_SERVER['REQUEST_METHOD'])) {
		$method = $_SERVER['REQUEST_METHOD'];
		/** @noinspection HtmlUnknownTarget */
		echo 'GENERATED PAGES ARE <a href="index.php" target="_blank">HERE</a><br/>';
		if ($method === 'POST') {
			$tasks = 'all';
			$generated_pages = array();
			$pages = $_POST['pages'];

			if (is_array($pages)) {
				foreach ($pages as $page_name) {
					$page_name = trim(htmlspecialchars($page_name));
					if ($page_name != '') array_push($generated_pages, $page_name);
				}
				echo implode(', ', $generated_pages) . "<br/>";
			}
			if (sizeof($generated_pages) > 0) $tasks = $generated_pages;
			execute_tasks($tasks);
		}
		if ($method === 'GET' || $method === 'POST') {
			global $p;
			echo '<form action="compile.php" method="post" style="padding:15px 30px;">';
			echo '<label style="display:block;margin-bottom:5px;">';
			echo '<input type="checkbox" name="pages[]" value="" />ALL PAGES</label>';
			foreach (new DirectoryIterator($p['sp']) as $directory) {
				if ($directory->isDir() && !$directory->isDot()) {
					echo '<label style="display:block;margin-bottom:5px;">';
					echo '<input type="checkbox" name="pages[]" value="' . $directory->getFilename() . '" />';
					echo $directory->getFilename();
					echo '</label>';
				}
			}
			echo '<button type="submit" style="margin-top:30px;">TIME TO WORK, BLACK GUY</button>';
			echo '</form>';
			exit(0);
		}
	}
	else {
		global $task;
		$tasks = $task;
		$generated_pages = array();
		$params = array(
			'p::' => 'pages::'
		);
		$options = getopt(implode('', array_keys($params)), $params);
		if (isset($options['pages']) || isset($options['p'])) {
			$pages = isset($options['pages'])?$options['pages']:$options['p'];
			$pages = explode(',',$pages);
			foreach ($pages as $page_name) {
				$page_name = trim($page_name);
				if ($page_name != '') array_push($generated_pages, $page_name);
			}
		}
		if (sizeof($generated_pages) > 0) $tasks = $generated_pages;
		execute_tasks($tasks);
	}
}
// Возвращает массив с путями к необходимым для сборщика директориям 
function generate_paths() {
	global $config;
	$absolute_path = __DIR__ . '/';
	$source_blocks_path = $absolute_path . $config['sources']['blocks'];
	$source_pages_path = $absolute_path . $config['sources']['pages'];
	$output_compiled_path = $absolute_path . $config['directories_names']['compiled'] . '/';
	$output_blocks_path = $absolute_path . $config['directories_names']['compiled'] . '/' . $config['directories_names']['blocks'] . '/';
	$output_pages_path = $absolute_path . $config['directories_names']['compiled'] . '/' . $config['directories_names']['pages'] . '/';
	return array(
		'a'=> $absolute_path,
		'sb'=> $source_blocks_path,
		'sp'=> $source_pages_path,
		'oc'=> $output_compiled_path,
		'ob'=> $output_blocks_path,
		'op'=> $output_pages_path
	);
}
// Создаёт необходимые директории для размещения собранных файлов
function handle_directories() {
	global $p;
	
	if (!is_dir($p['oc']) && !mkdir($p['oc'])) {
		echo 'handle_directories: cannot make output directory "' . $p['oc'] . "\"\n";
		exit(1);
	}
	if (!is_dir($p['ob']) && !mkdir($p['ob'])) {
		echo 'handle_directories: cannot make blocks output directory "' . $p['oc'] . "\"\n";
		exit(1);
	}
	if (!is_dir($p['op']) && !mkdir($p['op'])) {
		echo 'handle_directories: cannot make pages output directory "' . $p['oc'] . "\"\n";
		exit(1);
	}
}
// Удаляет и папки (рекурсивно) и отдельные файлы
function erase($path = false) {
	if (!$path) {
		echo 'erase: path is not defined' . "\n";
		return false;
	}
	if (is_dir($path)) {
		foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $file) {
			if ($file->isDir()){
				rmdir($file->getRealPath());
			} else {
				unlink($file->getRealPath());
			}
		}
		rmdir($path);
		return true;
	}
	return false;
}
// Выполняет задание
function execute_tasks($tasks = false) {
	if (!$tasks) {
		echo 'execute_tasks: $tasks is not defined' . "\n";
		exit(1);
	}
	$block_file_name = 'block.nigger';
	if (file_exists($block_file_name)) {
		echo "\nNigger is busy. Good working nigger we have :)\n";
		exit(0);
	} else {
		touch($block_file_name);
	}
	if ($tasks == 'all') {
		global $p;
		foreach (new DirectoryIterator($p['sp']) as $directory) {
			if ($directory->isDir() && !$directory->isDot()) generate_page($directory->getFilename());
		}
	}
	elseif (is_array($tasks)) {
		foreach ($tasks as $name) {
			generate_page($name);
		}
	}
	unlink($block_file_name);
	echo "\nNigger has finished. What a goood nigger\n";
}
// Возвращает имя файла, если он существует
// $path - путь до графического файла
function get_graphics_file($path = false) {
	if (!$path) {
		echo 'get_graphics_files: path is not defined' . "\n";
		return false;
	}
	if (!file_exists($path)) {
		echo 'get_graphics_files: file does not exist' . "\n";
		return false;
	}
	preg_match('/graphics-.*/i', $path, $file_name);
	if ($file_name[0]) return $file_name[0];
	else return false;
}
// Добавляет стили или скрипты в html вёрстку страницы
// $name - название страницы
// $html - html вёрстка
// $type - тип (либо скрипт, либо стиль)
function insert_assets($name = false, &$html = false, $type = false) {
	if (!$name) {
		echo 'insert_assets: name is not defined' . "\n";
		return false;
	}
	if (!$html) {
		echo 'insert_assets: html is not defined' . "\n";
		return false;
	}
	if (!$type) {
		echo 'insert_assets: type is not defined' . "\n";
		return false;
	}
	global $config;
	$insert = '';
	if ($type == 'css') {
		if (isset($config['external_css']) && is_array($config['external_css'])) {
			foreach ($config['external_css'] as $url) {
				$insert .= '<link href="' . $url .'" rel="stylesheet">' . "\n";
			}
		}
		/** @noinspection HtmlUnknownTarget */
		$insert .= '<link href="styles.css" rel="stylesheet"/>';
		$anchor = $config['page_styles_anchor'];
	}
	elseif ($type == 'js') {
		/** @noinspection HtmlUnknownTarget */
		$insert .= '<script src="scripts.js"></script>';
		$anchor = $config['page_scripts_anchor'];
	}
	else {
		echo 'insert_assets: type has incorrect value' . "\n";
		return false;
	}
	preg_match("/(\t*)#" . $anchor . '#/i', $html, $tabs);
	$tabs_length = strlen($tabs[1]);
	if ($tabs_length) {
		$lines = explode("\n", $insert);
		foreach ($lines as $line_index => $line_text) {
			$lines[$line_index] = str_repeat("\t", $tabs_length) . $line_text;
		}
		$insert = implode("\n", $lines);
	}
	$html = preg_replace('/(\t*#' . $anchor . '#)/i', $insert, $html);
	return true;
}
// Инициализирует и собирает страницу со всеми зависимостями и блоками
// $name - название страницы
function generate_page($name = false) {
	if (!$name) {
		echo 'generate_page: page name is not defined' . "\n";
		return false;
	}
	global $p;
	global $used;
	$used['css'] = array();
	$block = parse_block($name, $p['sp']);
	if (!$block) {
		echo 'generate_page: page "' . $name . '" was not parsed correctly' . "\n";
		return false;
	}
	insert_assets($block['name'], $block['html'], 'css');
	insert_assets($block['name'], $block['html'], 'js');
	$block['compiled'] = array();
	$block['compiled']['directory'] = $p['op'] . $block['name'] . '/';
	erase($block['compiled']['directory']);
	if (!is_dir($block['compiled']['directory']) && !mkdir($block['compiled']['directory'])) {
		echo 'generate_page: cannot make directory "' . $block['compiled']['directory'] . "\"\n";
		return false;
	}
	$block['compiled']['results'] = array();
	$block['compiled']['results']['html'] = file_put_contents($block['compiled']['directory'] . 'index.html', $block['html']);
	$block['compiled']['results']['css'] = file_put_contents($block['compiled']['directory'] . 'styles.css', $block['css']);
	$block['compiled']['results']['js'] = file_put_contents($block['compiled']['directory'] . 'scripts.js', $block['js']);
	foreach ($block['graphics'] as $file_name=>$graphics_path) {
		copy($graphics_path, $block['compiled']['directory'] . $file_name);
	}
	return true;
}
// Инициализирует и собирает отдельный блок со всеми зависимостями и подблоками
// $name - название блока
function generate_block($name = false, $html = false, $alternative_name = false) {
	if (!$name) {
		echo 'generate_block: block name is not defined' . "\n";
		return false;
	}
	if (!$alternative_name || empty($alternative_name)) {
		$alternative_name = $name;
	}
	global $p;
	global $used;
	$used['css'] = array();
	$block = parse_block($name);
	if (!$block) {
		echo 'generate_block: block "' . $alternative_name . '" was not parsed correctly' . "\n";
		return false;
	}
	$block['compiled'] = array();
	$block['compiled']['directory'] = $p['ob'] . $alternative_name . '/';
	erase($block['compiled']['directory']);
	if (!is_dir($block['compiled']['directory']) && !mkdir($block['compiled']['directory'])) {
		echo 'generate_block: cannot make directory "' . $block['compiled']['directory'] . "\"\n";
		return false;
	}
	if (!$html || empty($html)) {
		$html = $block['html'];
	}
	$block['compiled']['results'] = array(
		'css'=> file_put_contents($block['compiled']['directory'] . $alternative_name. '.css', $block['css']),
		'js'=> file_put_contents($block['compiled']['directory'] . $alternative_name. '.js', $block['js']),
		'html'=> file_put_contents($block['compiled']['directory'] . $alternative_name. '.html', $html)
	);
	foreach ($block['graphics'] as $file_name=>$graphics_path) {
		copy($graphics_path, $block['compiled']['directory'] . $file_name);
	}
	return true;
}
// Генерирует зависимости хитро и непредсказуемо, основываясь на кванотовой неопределённости
function generate_dependency($dependency = false, $html = false, $alternative_name = false) {
	if (!$dependency) {
		echo 'generate_dependency: $dependency is not defined' . "\n";
		return false;
	}
	global $config;
	if (isset($config['global_page_assets']) && $config['global_page_assets'] == true) {
		if (!isset($used['blocks'][$dependency['name']])) {
			$used['blocks'][$dependency['name']] = true;
			generate_block($dependency['name'], $html, $alternative_name);
		}
		if (!isset($used['blocks'][$dependency['full_name']])) {
			$used['blocks'][$dependency['full_name']] = true;
			generate_block($dependency['full_name'], $html, $alternative_name);
		}
	}
	return true;
}
// Рекурсивная функция. Возвращает массив со всей информацией о запрашиваемом блоке
// $block_name - название блока
// $directory - путь до папки с блоком (если не указан, то берёт путь из файла настроек)
function parse_block($block_name = false, $directory = false) {
	if (!$block_name) {
		echo 'parse_block: block name is not defined' . "\n";
		return false;
	}
	if (!$directory) {
		global $p;
		$directory = $p['sb'];
	}
	global $used;
	$block = array();
	$exploded_name = explode('_', $block_name);
	$block['name'] = $exploded_name[0];
	$block['template_name'] = (isset($exploded_name[1]))?$exploded_name[1]:'null';
	$block['full_name'] = $block_name;
	$block['directory'] = $directory . $block['name'];
	$block['options'] = array(
		'path'=> $block['directory'] . '/' . $block['name'] . '.json',
		'content'=> ''
	);
	$block['tpl'] = array(
		'template_path'=> $block['directory'] . '/' . $block['template_name'] . '/' . $block['name'] . '.tpl',
		'path'=> $block['directory'] . '/' . $block['name'] . '.tpl',
		'content'=> ''
	);
	$block['html'] = array(
		'template_path'=> $block['directory'] . '/' . $block['template_name'] . '/' . $block['name'] . '.html',
		'path'=> $block['directory'] . '/' . $block['name'] . '.html',
		'content'=> ''
	);
	$block['css'] = array(
		'template_path'=> $block['directory'] . '/' . $block['template_name'] . '/' . $block['name'] . '.css',
		'path'=> $block['directory'] . '/' . $block['name'] . '.css',
		'content'=> ''
	);
	$block['js'] = array(
		'path'=> $block['directory'] . '/' . $block['name'] . '.js',
		'content'=> ''
	);
	$block['dependencies'] = null;
	$block['graphics'] = array();
	if (!is_dir($block['directory'])) {
		echo 'parse_block: path "' . $block['directory'] . '" is not a directory' . "\n";
		return false;
	}
	foreach (glob($block['directory'] . "/graphics-*.*") as $graphics_path) {
		$file_name = get_graphics_file($graphics_path);
		if ($file_name && !isset($block['graphics'][$file_name])) $block['graphics'][$file_name] = $graphics_path;
	}
	if (file_exists($block['tpl']['template_path'])) {
		$block['tpl']['content'] = file_get_contents($block['tpl']['template_path']);
	}
	elseif (file_exists($block['tpl']['path'])) {
		$block['tpl']['content'] = file_get_contents($block['tpl']['path']);
	}
	elseif (file_exists($block['html']['template_path'])) {
		$block['tpl']['content'] = file_get_contents($block['html']['template_path']);
	}
	elseif (file_exists($block['html']['path'])) {
		$block['tpl']['content'] = file_get_contents($block['html']['path']);
	}
	$block['html']['content'] .= $block['tpl']['content'];
	if (file_exists($block['options']['path'])) {
		$block['options']['content'] = file_get_contents($block['options']['path']);
		if ($block['options']['content']) {
			$block['options']['content'] = json_decode($block['options']['content'], true);
			$block['dependencies'] = array();
			foreach ($block['options']['content']['dependencies'] as $dependence_name) {
				$dependency = parse_block($dependence_name);
				$block['css']['content'] .= $dependency['css'];
				$block['js']['content'] .= $dependency['js'];
				$alternative_name = $dependence_name;
				if (empty($block['options']['content']['mixins'])) {
					generate_dependency($dependency);
				}
				foreach ($block['options']['content']['mixins'] as $mixin) {
					if (!isset($mixin['tag']) || !isset($mixin['class']) || !isset($mixin['mixin'])) {
						echo 'parse_block: cannot add mixin ' . json_encode($mixin) . ' of the block "' . $block['options']['content']['name'] . "\"\n";
						continue;
					}
					$pattern = '/(<' . $mixin['tag'] . '.*?class=\".*?)(' . $mixin['class'] . ')([ |\"])/i';
					$replace = '${1}${2} ' . $mixin['mixin'] . '${3}';
					if (preg_match($pattern, $dependency['html']) > 0) {
						$alternative_name = $block['full_name'] . '_' . $dependency['name'];
						generate_dependency($dependency, preg_replace($pattern, $replace, $dependency['html']), $alternative_name);
					} else {
						generate_dependency($dependency);
					}
				}
				$output_html = '';
				$output_html .= '<!--block:' . $alternative_name . '-->' . "\n";
				$output_html .= $dependency['html'];
				$output_html .= "\n" . '<!--end:' . $alternative_name . '-->' . "\n";
				$dependency['html'] = $output_html;
				if (preg_match("/(\t*)#" . $dependence_name . '#/i', $block['html']['content'], $tabs)) {
					$tabs_length = strlen($tabs[1]);
					if ($tabs_length) {
						$lines = explode("\n", $dependency['html']);
						foreach ($lines as $line_index => $line_text) {
							$lines[$line_index] = str_repeat("\t", $tabs_length) . $line_text;
						}
						$dependency['html'] = implode("\n", $lines);
					}
					//preg_match("/(\t*)#test#/", $s, $matches);
					$block['html']['content'] = preg_replace('/(\t*#' . $dependence_name . '#)/i', $dependency['html'], $block['html']['content']);
				}
				$block['dependencies'][] = $dependency;
				foreach ($dependency['graphics'] as $file_name=>$graphics_path) {
					if ($file_name && !isset($block['graphics'][$file_name])) $block['graphics'][$file_name] = $graphics_path;
				}
			}
			foreach ($block['options']['content']['mixins'] as $mixin) {
				if (!isset($mixin['tag']) || !isset($mixin['class']) || !isset($mixin['mixin'])) {
					echo 'parse_block: cannot add mixin ' . json_encode($mixin) . ' of the block "' . $block['options']['content']['name'] . "\"\n";
					continue;
				}
				$pattern = '/(<' . $mixin['tag'] . '\s*class=\"(?:[a-z\s_-]*\s)?)(' . $mixin['class'] . ')([\s|\"])/i';
				$replace = '${1}${2} ' . $mixin['mixin'] . '${3}';
				$preg_result = preg_replace($pattern, $replace, $block['html']['content']);
				if ($preg_result != NULL) $block['html']['content'] = $preg_result;
			}
		}
	}
	if (file_exists($block['css']['path'])) {
		$css = '';
		if (!array_key_exists($block['name'], $used['css'])) {
			$css .= file_get_contents($block['css']['path']);
			$used['css'][$block['name']] = $block['css']['path'];
		}
		if (!array_key_exists($block['full_name'], $used['css']) && file_exists($block['css']['template_path'])) {
			$css .= file_get_contents($block['css']['template_path']);
			$used['css'][$block['full_name']] = $block['css']['template_path'];
		}
		$block['css']['content'] .= '/* Styles from block ' . $block['full_name'] . " */\n";
		$block['css']['content'] .= $css;
		$block['css']['content'] .= "\n";
		$block['css']['content'] .= '/******************************************/' . "\n";
		$block['css']['content'] .= "\n";
	}
	if (file_exists($block['js']['path'])) {
		$js = file_get_contents($block['js']['path']);
		$block['js']['content'] .= '/* Script from block ' . $block['full_name'] . " */\n";
		$block['js']['content'] .= $js;
		$block['js']['content'] .= "\n";
		$block['js']['content'] .= '/******************************************/' . "\n";
		$block['js']['content'] .= "\n";
	}
	return array(
		'name'=> $block['name'],
		'template_name'=> $block['template_name'],
		'full_name'=> $block['full_name'],
		'directory'=> $block['directory'],
		'tpl'=> $block['tpl']['content'],
		'html' => $block['html']['content'],
		'css' => $block['css']['content'],
		'js' => $block['js']['content'],
		'dependencies'=> $block['dependencies'],
		'graphics'=> $block['graphics']
	);
}

// Глобальные настройки сборщика
$config = load_configuration_file(__DIR__ . '/' . 'config.json');
// Задание сборщика
$task = load_task_file(__DIR__ . '/' . 'task.json');
// Пути ко всем директориям
$p = $paths = generate_paths();
// Массив для регистрации блоков (для уникальности и борьбы с дублированием)
$used = array(
	'blocks'=> array(),
	'css'=> array(),
	'js'=> array()
);
// Создание директорий
handle_directories();
// Кеш-дата
$cache_timestamp = time();
// Инициализация сборки
handle_tasks();