<form class="modal login-modal" id="login-modal" action="" method="post">
	<button class="btn modal__close-btn modal__close-action" type="button">
		<i class="btn__icon">
			<svg class="btn__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"><g fill="none" stroke="inherit" stroke-width="inherit"><path d="M0,0 L16,16 M0,16 L16,0"></path></g></svg>
		</i>
	</button>
	<div class="modal__content">
		<div class="login-modal__desc-side">
			<div class="logo-link login-modal__logo">
				<svg class="logo-link__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 82 25.49">
					<title>Бетон24 логотип</title>
					<path class="logo-link__frame" d="M56.36,0H82V24.16H76.3v-3H79V3H59.38v8.35a5.13,5.13,0,0,0-3,1.61Z" transform="translate(-0.07 0)"></path>
					<g class="logo-link__name">
						<path d="M8,18.17A3.51,3.51,0,0,0,7,17.44a3.27,3.27,0,0,0-1.3-.27H3.07v-2h5V12.65h-8V24.18H5.75A3.27,3.27,0,0,0,7,23.91a3.51,3.51,0,0,0,1.07-.73,3.45,3.45,0,0,0,.73-1.08,3.25,3.25,0,0,0,.27-1.3V20.58a3.25,3.25,0,0,0-.34-1.33A3.45,3.45,0,0,0,8,18.17Zm-2.14,3h0a1,1,0,0,1-.86.5h-2v-2h2A1,1,0,0,1,5.91,21.15Z" transform="translate(-0.07 0)"></path>
						<polygon points="10.8 24.13 17.95 24.13 17.95 21.66 13.82 21.66 13.82 19.54 17.47 19.54 17.47 16.99 13.82 16.99 13.82 15.11 17.95 15.11 17.95 12.6 10.8 12.6 10.8 24.13"></polygon>
						<polygon points="19.87 15.61 22.86 15.61 22.86 24.13 25.88 24.13 25.88 15.61 28.94 15.61 28.94 12.6 19.87 12.6 19.87 15.61"></polygon>
						<path d="M40.63,15a5.77,5.77,0,0,0-6.47-2.07,5.64,5.64,0,0,0-1.58.82,5.79,5.79,0,0,0-2.06,2.84,5.87,5.87,0,0,0,0,3.65A5.72,5.72,0,0,0,32.58,23a5.62,5.62,0,0,0,1.58.82,5.81,5.81,0,0,0,3.63,0A5.69,5.69,0,0,0,39.37,23,5.8,5.8,0,0,0,40.63,15Zm-2.11,4.47a2.77,2.77,0,0,1-1.46,1.47,2.72,2.72,0,0,1-1.07.22,2.68,2.68,0,0,1-1.06-.22,2.71,2.71,0,0,1-.86-0.62,2.77,2.77,0,0,1,0-3.9,2.72,2.72,0,0,1,3.87,0A2.77,2.77,0,0,1,38.52,19.45Z" transform="translate(-0.07 0)"></path>
						<path d="M63.29,18.59l0.11-.16A3.72,3.72,0,0,0,63.64,18a4.13,4.13,0,0,0,.24-0.67A3.33,3.33,0,0,0,64,16.47a3.8,3.8,0,0,0-.17-1.12,4.1,4.1,0,0,0-.5-1,3.87,3.87,0,0,0-.78-0.87,3.58,3.58,0,0,0-2.28-.81,4.23,4.23,0,0,0-1.31.2,3.76,3.76,0,0,0-1.9,1.4,3.82,3.82,0,0,0-.69,2.18h2.47a1.4,1.4,0,0,1,.18-0.7,1.33,1.33,0,0,1,.48-0.49,1.31,1.31,0,0,1,.67-0.18,1.33,1.33,0,0,1,.56.12,1.45,1.45,0,0,1,.43.32,1.42,1.42,0,0,1,.28.43,1.13,1.13,0,0,1,.1.45,1.48,1.48,0,0,1-.14.62,2.54,2.54,0,0,1-.36.56l-4.64,6.58h8V21.72H61.07Z" transform="translate(-0.07 0)"></path>
						<path d="M73.07,19.3V12.66h-3L65.41,19.3v2.46h5.26V24.2h2.4V21.76h2.12V19.3H73.07Zm-2.42,0h-2.3L70.65,16V19.3Z" transform="translate(-0.07 0)"></path>
						<polygon points="50.46 16.72 46.81 16.72 46.81 12.62 43.78 12.62 43.78 24.14 46.81 24.14 46.81 19.75 50.46 19.75 50.46 24.14 53.49 24.14 53.49 12.62 50.46 12.62 50.46 16.72"></polygon>
					</g>
				</svg>
			</div>
			<p class="login-modal__cap">Вход в личный кабинет</p>
			<p class="login-modal__desc">Введите номер телефона и пароль</p>
		</div>
		<fieldset class="login-modal__form-side">
			<div class="field-block login-modal__item">
				<p class="field-block__cap">Номер телефона</p>
				<div class="field-block__grp">
					<div class="field-grp field-grp_phone">
						<input class="field field-grp__field" type="tel" maxlength="20" placeholder="" />
						<p class="field-grp__prefix">+7</p>
					</div>
				</div>
			</div>
			<div class="field-block login-modal__item">
				<p class="field-block__cap">Пароль</p>
				<div class="field-block__grp">
					<div class="password-block">
						<input class="field password-block__field" type="password" name="<NAME>" placeholder="" />
						<button class="btn password-block__btn" title="Скрыть/Показать пароль" type="button">
							<i class="btn__icon password-block__btn-icon-hidden">
								<svg class="btn__svg" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
									<g fill="inherit">
										<circle cx="8" cy="8" r="1.85"></circle>
										<path d="M8,3C5.16,3,1,8,1,8s4.16,5,7,5,7-5,7-5S10.85,3,8,3Zm0,9C6,12,3.05,9.93,3,8S6,4,8,4s5,2.07,5,4S10,12,8,12Z"></path>
									</g>
								</svg>
							</i>
							<i class="btn__icon password-block__btn-icon-shown">
								<svg class="btn__svg" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
									<g fill="inherit">
										<circle cx="8" cy="8" r="1.85"></circle>
										<path d="M8,3C5.16,3,1,8,1,8s4.16,5,7,5,7-5,7-5S10.85,3,8,3Zm0,9C6,12,3.05,9.93,3,8S6,4,8,4s5,2.07,5,4S10,12,8,12Z"></path>
										<path d="M0,13.5 L.5,14.5 L16,2.5 L15.5,1.5 L0,13.5"></path>
									</g>
								</svg></i>
						</button>
					</div>
				</div>
			</div>
			<div class="login-modal__item">
				<div class="checkbox-block">
					<label class="checkbox-block__wrp">
						<input class="checkbox-block__field" type="checkbox" checked name="<NAME>" value="" />
						<i class="checkbox-block__icon">
							<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
						</i>
						<span class="checkbox-block__cap">Запомнить пароль</span>
					</label>
				</div>
			</div>
			<div class="login-modal__item">
				<button class="btn btn_fluid btn_color_primary btn_size_lg" type="submit">
					<span class="btn__cap">Войти</span>
				</button>
			</div>
			<div class="login-modal__links">
				<div>
					<a class="link link_muted" href="javascript:void(0);">
						<span class="link__cap">Забыли пароль?</span>
					</a>
				</div>
				<div class="login-modal__link">
					<a class="link link_muted" href="javascript:void(0);">
						<span class="link__cap">Нет аккаунта?</span>
					</a>
				</div>
			</div>
		</fieldset>
	</div>
</form>