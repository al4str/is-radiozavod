<div class="form-messages" data-type="" data-is-hidden>
	<p class="form-messages__msg" data-type="success" data-id="default">Изменения сохранены</p>
	<p class="form-messages__msg" data-type="error" data-id="default">Произошла ошибка</p>
	<p class="form-messages__msg" data-type="error" data-id="bad_reputation">Нельзя взять заказ от компании с низкой репутацией</p>
	<p class="form-messages__msg" data-type="error" data-id="least_one_worker">Нужно назначить хотя бы одного инженера</p>
</div>