<section class="dashboard">
	<div class="fixed-block">
		{{ if this['dashboard'].current_orders :}}
		<div class="island dashboard__item">
			<header class="island__header">
				<p class="island__title">Текущие заказы</p>
			</header>
			<ul class="dashboard__list">
				<li class="dashboard__list-item">
					<p class="dashboard__order-item">#003АП11 ООО "Сибирский Технопром" (интернет-магазин)</p>
				</li>
			</ul>
		</div>
		{{ end }}
		<div class="island dashboard__item">
			<header class="island__header">
				<p class="island__title">Произведено продукции</p>
			</header>
			<div class="dashboard__chart">
				<div class="dashboard__chart-item" title='Резистор РБ-1 "Эдельвейс"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">180</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-01.svg" alt='Резистор РБ-1 "Эдельвейс"' />
				</div>
				<div class="dashboard__chart-item" title='Атненна АМ-4 "Борей"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">120</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-02.svg" alt='Атненна АМ-4 "Борей"' />
				</div>
				<div class="dashboard__chart-item" title='Транзистор Т28 "Солярис"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">60</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-03.svg" alt='Транзистор Т28 "Солярис"' />
				</div>
				<div class="dashboard__chart-item" title='Радиоточка РС "Звезда"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">40</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-04.svg" alt='Радиоточка РС "Звезда"' />
				</div>
				<div class="dashboard__chart-item" title='Тумблер 5Л "Титан"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">200</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-05.svg" alt='Тумблер 5Л "Титан"' />
				</div>
				<div class="dashboard__chart-item" title='Отражатель ОБ "Байкал"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">140</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-06.svg" alt='Отражатель ОБ "Байкал"' />
				</div>
				<div class="dashboard__chart-item" title='Концентратор 10К "Эльбрус"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">80</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-07.svg" alt='Концентратор 10К "Эльбрус"' />
				</div>
				<div class="dashboard__chart-item" title='Радиостанция РЗ "Зенит"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">38</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-08.svg" alt='' />
				</div>
				<div class="dashboard__chart-item" title='Накопитель 6-1Н "Олимп"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">42</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-09.svg" alt='Накопитель 6-1Н "Олимп"' />
				</div>
				<div class="dashboard__chart-item" title='Усилитель УС-7 "Победа"'>
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">240</p>
						</div>
					</div>
					<img class="dashboard__product" src="graphics-product-10.svg" alt='Усилитель УС-7 "Победа"' />
				</div>
			</div>
		</div>
		<div class="island dashboard__item">
			<header class="island__header">
				<p class="island__title">Выполнено заказов</p>
			</header>
			<div class="dashboard__chart">
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">8</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Май</p>
						<p class="dashboard__chart-cap">2016</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">16</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Июнь</p>
						<p class="dashboard__chart-cap">2016</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">10</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Июль</p>
						<p class="dashboard__chart-cap">2016</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">6</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Август</p>
						<p class="dashboard__chart-cap">2016</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">3</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Сентябрь</p>
						<p class="dashboard__chart-cap">2016</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">19</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Октябрь</p>
						<p class="dashboard__chart-cap">2016</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">9</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Ноябрь</p>
						<p class="dashboard__chart-cap">2016</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">4</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Декабрь</p>
						<p class="dashboard__chart-cap">2016</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">12</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Январь</p>
						<p class="dashboard__chart-cap">2017</p>
					</div>
				</div>
				<div class="dashboard__chart-item">
					<div class="dashboard__chart-height">
						<div class="dashboard__chart-bar">
							<p class="dashboard__chart-amount">10</p>
						</div>
					</div>
					<div class="dashboard__chart-desc">
						<p class="dashboard__chart-cap">Февраль</p>
						<p class="dashboard__chart-cap">2017</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>