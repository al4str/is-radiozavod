/**
 * Универсальное сообщение-ответ сервера
 *
 * @namespace App.c.tools
 * @class FormMessages
 * @returns {object}
 */
App.c.tools.FormMessages = function($block) {
	var self = this;

	self.opt = {};
	self.dom = {};
	/**
	 * Показ блока с анимацией
	 * Проверка на входные параметры
	 * Проверка на наличие типа и идентификатора сообщения
	 * При наличии типа, но отсутствии идентификатора - выводится дефолтное значение
	 *
	 * @namespace App.c.tools.FormMessages
	 * @class show
	 *
	 * @param {object} options — Входящие параметры
	 * @returns {boolean}
	 */
	self.show = function(options) {
		if (!options || typeof options !== 'object' || !options['type']) {
			console.error('FormMessages: не смогу показать сообщение, пока ты не научишься трифорсить');
			return false;
		}
		if (!options.type || !self.opt.msgs[options.type]) {
			console.error('FormMessages: не передан тип сообщения');
			return false;
		}
		if (!options.id || !self.opt.msgs[options.type][options.id]) {
			console.error('FormMessages: id не передан или неверен');
			options.id = 'default';
		}
		self.dom.$block.innerHTML = '';
		var msg = self.opt.msgs[options.type][options.id],
			text = (options.id=='default' && options.code)?msg + ' — ' + options.code:msg,
			$msg = App.h.get_node('form-messages__msg', text, 'p');

		self.dom.$block.setAttribute('data-type', options.type);
		self.dom.$block.appendChild($msg);
		self.dom.$block.removeAttribute('data-is-hidden');
	};
	/**
	 * Скрытие блока сообщений с анимацией
	 *
	 * @namespace App.c.tools.FormMessages
	 * @class hide
	 */
	self.hide = function() {
		self.dom.$block.innerHTML = '';
		self.dom.$block.setAttribute('data-is-hidden', '');
	};
	/**
	 * Инициализация логики компонента
	 * Кеширование Node элементов
	 * Проверка на наличие сообщений
	 * Кеширование сообщений по ключу
	 * Удаление лишних Node элементов 
	 *
	 * @namespace App.c.tools.FormMessages
	 * @class render
	 *
	 * @returns {boolean}
	 */
	self.render = function() {
		self.dom.$msgs = self.dom.$block.querySelectorAll('.form-messages__msg');
		if (!self.dom.$msgs || self.dom.$msgs.length == 0) {
			console.error('FormMessages: нет сообщений');
			return false;
		}
		App.h.i(self.dom.$msgs, function($node){
			var type = $node.getAttribute('data-type'),
				id = $node.getAttribute('data-id'),
				text = $node.textContent.trim();

			if (type && id && text) {
				if (!self.opt.msgs[type]) self.opt.msgs[type] = {};
				self.opt.msgs[type][id] = text;
			}
			App.h.remove_node($node);
		});
		self.dom.$block.js = self;
	};
	/**
	 * Инициализация компонента
	 * Простейшая проверка на наличие Node
	 *
	 * @namespace App.c.tools.FormMessages
	 * @class initiate
	 *
	 * @returns {boolean}
	 */
	self.initiate = function() {
		self.dom.$block = $block;
		if (!self.dom.$block) {
			self = null;
			return false;
		}
		self.opt.msgs = {};
		self.render();
	};
	self.initiate();
	return self;
};