// routing
// etc template
// ms sql data to vue to etc template
// users
// groups
// pages
// auth
const m = {};

m.path = require('path');
m.diet = require('diet');
m.opn = require('opn');
m.minimist = require('minimist');
m.crypto = require('crypto');

m.diet_render = require('./diet-render');
m.diet_cookies = require('./diet-cookies');
m.diet_static = require('./diet-static');
m.db = require('./db');


String.prototype.sql_date = function() {
	let day, month, year, split = this.split('-');

	if (split.length !== 3) {
		console.warn('String.sql_date:', 'not a sql date string');
		return false;
	}
	day = parseInt(split[0]);
	month = Date.prototype.sql_months.indexOf(split[1]);
	month += 1;
	year = parseInt(split[2]);
	if (isNaN(day) || month === -1 || isNaN(year)) {
		console.warn('String.sql_date:', 'not a valid sql date string');
		return false;
	}
	return new Date(year + '/' + month + '/' + day + ' 12:00:00');
};
Date.prototype.sql_months = [
	'JAN', 'FEB', 'MAR', 'APR',
	'MAY', 'JUN', 'JUL', 'AUG',
	'SEP', 'OCT', 'NOV', 'DEC'
];
Date.prototype.sql_date = function() {
	var day, month, year;

	day = this.getDate();
	if (day < 10) day = '0' + day;
	month = Date.prototype.sql_months[this.getMonth()];
	year = this.getFullYear();
	return day + '-' + month + '-' + year;
};
Date.prototype.string_date = function() {
	var day, month, year;

	day = this.getDate();
	month = this.getMonth()+1;
	year = this.getFullYear();
	if (day < 10) day = '0' + day;
	if (month < 10) month = '0' + month;
	return day + '.' + month + '.' + year;
};


const app = {};

app.const = {};
app.server = m.diet();
if (!app.server.path) app.server.path = module.path;
app.const.app_config = require('./app_config.json');
app.q = m.db;
app.server.listen(app.const.app_config.host);
app.render = m.diet_render({
	path: app.server.path + '/views/html/',
	cache: false
});
app.const.params = m.minimist(process.argv.slice(2));
app.const.debug = app.const.params['dev'];
//console.dir(app.const.params);
//process.exit(0);


app.static = m.diet_static({
	path: m.path.join(app.server.path, '/views/html/')
});
app.server.header(function($){
	console.info('index.js', 'request for', $.url.href);
	$.stop = stop.bind(this, $);
	$.return();
});
app.server.header(app.render);
app.server.header(m.diet_cookies);
app.server.header(auth);
app.server.footer(app.static);


function stop($) {
	$.status('500', 'Server Error');
	$.end(`${$.statusCode} ${$.statusMessage}\n${$.stop_message}`);
}
function auth($) {
	if (m.path.extname($.url.pathname)) {
		$.return();
	}
	else {
		$.auth = {
			user_map: {
				id: 'ID',
				avatar: 'AVATAR',
				name: 'NAME',
				spec: 'POSITION',
				department: 'DEPARTMENT_NAME'
			},
			user: null,
			is_authed: () => {
				if ($.auth.user) {
					$.worker = $.auth.user;
					if ($.auth.user.spec == 'Начальник отдела' && $.auth.user.id != 3) $.worker.is_order_edit_allowed = 'Y';
					else $.worker.is_order_edit_allowed = 'N';
				}
				return !!$.auth.user;
			},
			store_user_cookie: (obj) => {
				let user = {};
				for (let local_key in $.auth.user_map) {
					if (!$.auth.user_map.hasOwnProperty(local_key)) continue;
					let remote_key = $.auth.user_map[local_key];

					user[local_key] = obj[remote_key];
					if (remote_key == 'POSITION' && user[local_key] === undefined) user[local_key] = 'Начальник отдела';
					user[local_key] = encodeURIComponent(user[local_key]);
					$.cookies.set(`user-${local_key}`, user[local_key], {httpOnly: true});
				}
				$.auth.user = user;
			},
			obtain_user_cookie: () => {
				let user = {};

				for (let local_key in $.auth.user_map) {
					if (!$.auth.user_map.hasOwnProperty(local_key)) continue;
					user[local_key] = $.cookies[`user-${local_key}`];
					user[local_key] = decodeURIComponent(user[local_key]);
				}
				if (!(user['id'] > 0)) return null;
				return user;
			},
			eat_user_cookie: () => {
				$.auth.user = null;
				for (let local_key in $.auth.user_map) {
					if (!$.auth.user_map.hasOwnProperty(local_key)) continue;
					$.cookies.delete(`user-${local_key}`);
				}
			},
			log_in: () => {
				return new Promise((resolve, reject) => {
					let login = $.body['USERNAME'],
						password = $.body['PASSWORD'];

					if (!login || !password) {
						resolve(false);
					}
					app.q(false, {
							query: `USE JUPITER;\nSELECT ID, NAME, POSITION, (SELECT NAME FROM DEPARTMENTS WHERE ID = WORKERS.DEPARTMENT_ID) AS 'DEPARTMENT_NAME', AVATAR FROM WORKERS WHERE login = '${login}' AND PASSWORD = '${password}';`
						})
					.catch((errors) => {
						reject(errors);
					})
					.then((result) => {
						if (!result) {
							app.q(false, {
									query: `USE JUPITER;\nSELECT ID, NAME, (SELECT NAME FROM DEPARTMENTS WHERE ID = SUPERVISORS.DEPARTMENT_ID) AS 'DEPARTMENT_NAME', AVATAR FROM SUPERVISORS WHERE login = '${login}' AND PASSWORD = '${password}';`
								})
								.catch((errors) => {
									reject(errors);
								})
								.then((result) => {
									if (!result) {
										resolve(false);
										return;
									}
									$.auth.store_user_cookie(result[0]);
									resolve(true);
								});
							return;
						}
						$.auth.store_user_cookie(result[0]);
						resolve(true);
					});
				});
			},
			log_out: () => {
				$.auth.eat_user_cookie();
			}
		};
		$.auth.user = $.auth.obtain_user_cookie();
		$.return();
	}
}


app.server.missing(function($){
	console.info('index.js', 'universal hook missing url', $.url.href);
	if ($.method.toLowerCase() == 'get' && !m.path.extname($.url.pathname)) {
		$.redirect('/404/');
	}
	else {
		$.status('404', 'Page Not Found');
		$.end($.statusCode + ' ' + $.statusMessage);
	}
});
app.server.get('/', function($) {
	console.info('index.js', 'get hook for /');
	if (!$.auth.is_authed()) $.redirect('/login/');
	$.redirect('/dashboard/');
});
app.server.get('/dashboard/', function($) {
	console.info('index.js', 'get hook for /dashboard/');
	if (!$.auth.is_authed()) $.redirect('/login/');
	$.data['html-head'] = {
		title: 'Главная страница'
	};
	$.data['header'] = {
		logout_url: '/logout/'
	};
	$.data['menu'] = {
		links: [
			{
				is_active: false,
				href: '/orders/',
				cap: 'Список заказов'
			},
			{
				is_active: false,
				href: '/workers/',
				cap: 'Сотрудники'
			},
			{
				is_active: false,
				href: '/merchandise/',
				cap: 'Продукция завода'
			}
		]
	};
	$.data['dashboard'] = {
		current_orders: false
	};
	$.html('dashboard/index.html');
});
app.server.get('/orders/', function($) {
	console.info('index.js', 'get hook for /orders/');
	if (!$.auth.is_authed()) $.redirect('/login/');
	$.data['html-head'] = {
		title: 'Список заказов'
	};
	$.data['header'] = {
		logout_url: '/logout/'
	};
	$.data['menu'] = {
		links: [
			{
				is_active: true,
				href: '/orders/',
				cap: 'Список заказов'
			},
			{
				is_active: false,
				href: '/workers/',
				cap: 'Сотрудники'
			},
			{
				is_active: false,
				href: '/merchandise/',
				cap: 'Продукция завода'
			}
		]
	};
	$.data['orders'] = {
		orders: {}
	};
	let get_purchased_items = (order_id) => {
		return new Promise((resolve, reject) => {
			app.q(true, {
				query: `
				USE JUPITER;\n
				SELECT ORDER_ID, (SELECT NAME FROM PRODUCTION WHERE ID = PURCHASED_ITEMS.PRODUCT_ID) AS 'PRODUCT_NAME', SUM(QUANTITY) AS 'SUM' 
				FROM PURCHASED_ITEMS 
				WHERE ORDER_ID = ${order_id} 
				GROUP BY PRODUCT_ID, ORDER_ID 
				ORDER BY ORDER_ID
				`
			})
			.catch((errors) => {
				reject(errors);
			})
			.then((result) => {
				if (!result) {
					reject('Bad T-SQL query: DB response is empty');
					return;
				}
				console.log(result);
				resolve(result);
			});
		});
	};

	app.q(true, {
		query: `
		USE JUPITER;\n
		SELECT 
		(SELECT TYPE FROM CLIENTS WHERE ID = ORDERS.CLIENT_ID) AS 'TYPE', 
		ID, 
		(SELECT NAME FROM CLIENTS WHERE ID = ORDERS.CLIENT_ID) AS 'CLIENT_NAME', 
		(SELECT REPUTATION FROM CLIENTS WHERE ID = ORDERS.CLIENT_ID) AS 'CLIENT_REPUTATION', 
		DATE, 
		STATUS 
		FROM ORDERS WHERE STATUS = 'working on' OR STATUS = 'awaiting' 
		ORDER BY DATE DESC, STATUS DESC
		`
	})
	.catch((errors) => {
		$.stop_message = errors;
		$.stop();
	})
	.then((result) => {
		if (!result) {
			$.stop_message = 'Bad T-SQL query: DB response is empty';
			$.stop();
			return;
		}
		let orders = $.data['orders'].orders;

		result.map((value) => {
			let status = value['STATUS'],
				date = value['DATE'].string_date(),
				obj = {};

			if (orders[value['TYPE']] === undefined) orders[value['TYPE']] = [];
			if (status == 'awaiting') status = 'Ожидает';
			if (status == 'working on') status = 'Выполняется';
			obj.id = value['ID'];
			obj.vid = value['ID'];
			if (obj.vid < 10) obj.vid = '000' + obj.vid; else
			if (obj.vid < 100) obj.vid = '00' + obj.vid; else
			if (obj.vid < 1000) obj.vid = '0' + obj.vid;
			obj.client_name = value['CLIENT_NAME'];
			obj.is_acceptable = (value['CLIENT_REPUTATION']=='bad')?'N':'Y';
			obj.date = date;
			obj.status = status;
			obj.obj = JSON.stringify(obj, (key, val) => {
				if (key == 'obj') return '[circular]';
				return val;
			});
			if (orders[value['TYPE']].length > 15) return;
			orders[value['TYPE']].push(obj);
		});

		$.data['order-modal'] = {
			workers: []
		};
		app.q(false, {
			query: `
			USE JUPITER;\n
			SELECT ID, NAME, (SELECT NAME FROM DEPARTMENTS WHERE ID = WORKERS.DEPARTMENT_ID) AS 'DEPARTMENT_NAME', AVATAR FROM WORKERS WHERE POSITION = 'Инженер'
			`
		})
		.catch((errors) => {
			$.stop_message = errors;
			$.stop();
		})
		.then((result) => {
			if (!result) {
				$.stop_message = 'Bad T-SQL query: DB response is empty';
				$.stop();
				return;
			}

			result.map((value) => {
				let obj = {};
				
				obj.id = value['ID'];
				obj.name = value['NAME'];
				obj.department = value['DEPARTMENT_NAME'];
				obj.avatar = value['AVATAR'];
				$.data['order-modal'].workers.push(obj);
			});
			$.html('orders/index.html');
		});
	});
});
app.server.get('/workers/', function($) {
	console.info('index.js', 'get hook for /workers/');
	if (!$.auth.is_authed()) $.redirect('/login/');
	$.data['html-head'] = {
		title: 'Сотрудники'
	};
	$.data['header'] = {
		logout_url: '/logout/'
	};
	$.data['menu'] = {
		links: [
			{
				is_active: false,
				href: '/orders/',
				cap: 'Список заказов'
			},
			{
				is_active: true,
				href: '/workers/',
				cap: 'Сотрудники'
			},
			{
				is_active: false,
				href: '/merchandise/',
				cap: 'Продукция завода'
			}
		]
	};
	$.data['workers'] = {
		departments: {}
	};
	app.q(false, {
		query: `
		USE JUPITER;\n
		SELECT (SELECT NAME FROM DEPARTMENTS WHERE ID = SUPERVISORS.DEPARTMENT_ID) AS 'DEPARTMENT_NAME', NAME, 'Начальник отдела' AS POSITION, AVATAR FROM SUPERVISORS
		UNION
		SELECT (SELECT NAME FROM DEPARTMENTS WHERE ID = WORKERS.DEPARTMENT_ID) AS 'DEPARTMENT_NAME', NAME, POSITION, AVATAR FROM WORKERS ORDER BY DEPARTMENT_NAME, AVATAR
		`
	})
	.catch((errors) => {
		$.stop_message = errors;
		$.stop();
	})
	.then((result) => {
		if (!result) {
			$.stop_message = 'Bad T-SQL query: DB response is empty';
			$.stop();
			return;
		}
		let departments = $.data['workers'].departments;

		result.map((value) => {
			let department_name = value['DEPARTMENT_NAME'];

			if (departments[department_name] === undefined) departments[department_name] = [];
			departments[department_name].push({
				avatar: value['AVATAR'],
				name: value['NAME'],
				spec: value['POSITION']
			});
		});
		$.html('workers/index.html');
	});
});
app.server.get('/merchandise/', function($) {
	console.info('index.js', 'get hook for /merchandise/');
	if (!$.auth.is_authed()) $.redirect('/login/');
	$.data['html-head'] = {
		title: 'Продукция завода'
	};
	$.data['header'] = {
		logout_url: '/logout/'
	};
	$.data['menu'] = {
		links: [
			{
				is_active: false,
				href: '/orders/',
				cap: 'Список заказов'
			},
			{
				is_active: false,
				href: '/workers/',
				cap: 'Сотрудники'
			},
			{
				is_active: true,
				href: '/merchandise/',
				cap: 'Продукция завода'
			}
		]
	};
	$.data['merchandise'] = {
		production: []
	};
	app.q(false, {
		query: `
		USE JUPITER;\n
		SELECT NAME, NICKNAME, DONE, ON_DEMAND FROM PRODUCTION
		`
	})
		.catch((errors) => {
			$.stop_message = errors;
			$.stop();
		})
		.then((result) => {
			if (!result) {
				$.stop_message = 'Bad T-SQL query: DB response is empty';
				$.stop();
				return;
			}
			result.map((value, index) => {
				let production = $.data['merchandise'].production,
					icon = index+1;

				if (icon < 10) icon = '0' + icon;
				production.push({
					icon: icon,
					name: value['NAME'],
					nickname: value['NICKNAME'],
					done: value['DONE'],
					on_demand: value['ON_DEMAND']
				});
			});
			$.html('merchandise/index.html');
		});
});
app.server.get('/login/', function($) {
	console.info('index.js', 'get hook for /login/');
	if ($.auth.is_authed()) $.redirect('/dashboard/');
	$.data['html-head'] = {
		title: 'Радиозавод24'
	};
	$.data['login'] = {
		wrong_credentials: 'N'
	};
	$.html('login/index.html');
});
app.server.get('/404/', function($) {
	console.info('index.js', 'get hook for /404/');
	$.data['html-head'] = {
		title: 'Страница не найдена'
	};
	$.html('404/index.html');
});

app.server.post('/orders/', function($) {
	console.info('index.js', 'post hook for /orders/');
	console.log($.body);
	let response_error = {
			result: 'error',
			response: [
				{
					code: 500,
					name: 'default',
					description: 'Не передан ORDER_ID'
				}
			]
		},
		response_bad_reputation = {
			result: 'error',
			response: [
				{
					code: 413,
					name: 'bad_reputation',
					description: 'Компания с плохой репутацией'
				}
			]
		},
		response_least_one_worker = {
			result: 'error',
			response: [
				{
					code: 420,
					name: 'least_one_worker',
					description: 'Должен быть назначен хотя бы один инженер'
				}
			]
		},
		response_success = {
			result: 'success',
			response: []
		};
	
	if (!$.body['ORDER_ID']) {
		$.json(response_error);
		return;
	}

	if ($.body['STATUS'] && $.body['WORKERS'] === undefined) {
		app.q(false, {
			query: `
			USE JUPITER;\n
			UPDATE ORDERS SET STATUS = '${$.body['STATUS']}' WHERE ID = ${$.body['ORDER_ID']};\n
			`
		})
		.catch((errors) => {
			$.stop_message = errors;
			$.stop();
		})
		.then(() => {
			$.json(response_success);
		});
	}
	else {
		if ($.body['WORKERS'].length == 0) {
			$.json(response_least_one_worker);
			return;
		}

		let WORKERS_INSERTS = ``;
		$.body['WORKERS'].map((worker_id) => {
			WORKERS_INSERTS += `INSERT APPOINTMENTS VALUES (${$.body['ORDER_ID']}, ${worker_id});\n`;
		});
		app.q(false, {
			query: `
			USE JUPITER;\n
			UPDATE ORDERS SET STATUS = 'working on' WHERE ID = ${$.body['ORDER_ID']};\n
			UPDATE SCHEDULE SET LEAD_ENGINEER_ID = ${$.body['LEAD_ENGINEER']} WHERE ORDER_ID = ${$.body['ORDER_ID']};\n
			DELETE FROM APPOINTMENTS WHERE ORDER_ID = ${$.body['ORDER_ID']};\n
			${WORKERS_INSERTS}
			`
		})
		.catch((errors) => {
			$.stop_message = errors;
			$.stop();
		})
		.then(() => {
			$.json(response_success);
		});
	}
});
app.server.post('/login/', function($){
	console.info('index.js', 'post hook for /login/');
	$.auth.log_in()
		.then((is_authed) => {
			if (!is_authed) {
				$.data['login'] = {
					wrong_credentials: 'Y'
				};
				$.data['html-head'] = {
					title: 'Радиозавод24'
				};
				$.html('login/index.html');
				return;
			}
			$.redirect('/dashboard/');
		})
		.catch((errors) => {
			$.stop_message = errors;
			$.stop();
		});
});
app.server.post('/logout/', function($){
	console.info('index.js', 'post hook for /logout/');
	$.auth.log_out();
	$.redirect('/login/');
});

m.opn(app.const.app_config.host + '/');