App.c.blocks.OrderForm = function() {
	var self = this;

	self.opt = {};
	self.dom = {};
	self.on_form_success = function() {
		if (self.dom.$messages.js) self.dom.$messages.js.show({type: 'success', id: 'default'});
	};
	self.on_submit = function(e) {
		var form = this;

		/*if (form.is_acceptable == 'N') {
			if (self.dom.$messages.js) self.dom.$messages.js.show({type: 'error', id: 'bad_reputation'});
			self.dom.$submit_btn.disabled = true;
			return;
		}*/
		/*if ($checked_radio) form.params['LEAD_ENGINEER'] = $checked_radio.value;
		else form.params['LEAD_ENGINEER'] = null;
		form.params['WORKERS'] = [];
		*/
		//self.opt.FH.dom.$submit_btn = self.dom.$submit_btn;
		App.h.i(self.dom.$block.querySelectorAll('.field:not([disabled])'), function($node){
			let args = $node.name.match(new RegExp('PURCHASED_ITEMS\\[([\\d]+)\\]'));
			if (args) {
				if (!self.opt.form.params['PURCHASED_ITEMS']) self.opt.form.params['PURCHASED_ITEMS'] = {};
				self.opt.form.params['PURCHASED_ITEMS'][args[1]] = $node.value;
			}
			else self.opt.form.params[$node.name] = $node.value;
		});
		App.h.trigger(self.dom.$form, 'fh:submit', self.opt.form);
		e.preventDefault();
	};
	/*self.on_done = function(e) {
		var form = this;

		if (form.order['status'] != 'Выполняется') {
			return;
		}
		form.params['STATUS'] = 'completed';
		self.opt.FH.dom.$submit_btn = self.dom.$done_btn;
		App.h.trigger(self.dom.$form, 'fh:submit', form);
		e.preventDefault();
	};*/
	self.render = function() {
		self.dom.$form = self.dom.$block;
		self.dom.$type = self.dom.$block.querySelector('.order-type');
		self.dom.$fio = self.dom.$block.querySelector('.order-name-fio');
		self.dom.$corp = self.dom.$block.querySelector('.order-name-corp');
		self.dom.$submit_btn = self.dom.$block.querySelector('.order-form__submit-btn');
		//self.dom.$done_btn = self.dom.$block.querySelector('.order-form__done-btn');
		self.dom.$messages = self.dom.$block.querySelector('.order-form__messages');
		self.dom.$form.addEventListener('fh:success', self.on_form_success, false);
		self.opt.FH = new App.c.tools.FormHandler({
			$form: self.dom.$form,
			$submit_btn: self.dom.$submit_btn,
			$messages: self.dom.$messages
		});
		self.opt.form = {};
		self.opt.form.url = self.dom.$submit_btn.dataset.url;
		self.opt.form.params = App.h.get_json(self.dom.$submit_btn.dataset.params);
		self.dom.$submit_btn.onclick = self.on_submit;
		//self.dom.$type.js.
	};
	self.initiate = function() {
		self.dom.$block = App.d.d.querySelector('.order-form');
		if (!self.dom.$block) {
			self = null;
			return false;
		}
		self.render();
	};
	self.initiate();
	return {self: self};
};