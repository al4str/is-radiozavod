Ссылка на незаконченный мануал по развёртыванию ИС Радиозваод "Юпитер":
https://docs.google.com/document/d/1sdJiS9ET2pvZvb0JifhhqToLpBGM_irU98K5gK-f71Q/edit?usp=sharing

Шаги:
1. Установка MS SQL Server 2014
2. Установка Node.js под Windows
3. Установка Git Bash под Windows
4. Вытягивание репозитория
5. Установка зависимостей
6. Запуск проекта
