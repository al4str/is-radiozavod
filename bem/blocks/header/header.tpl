<header class="header">
	<div class="header__top">
		<div class="fixed-block header__top-wrp">
			<div class="header__logo-col">
				#logo-link#
			</div>
			<div class="header__slogan-col">
				#slogan#
			</div>
			<!--suppress HtmlUnknownTarget -->
			<form class="header__logout-col" action="{{- this['header'].logout_url}}" method="post">
				<button class="btn btn_link btn_link_muted header__logout-btn" type="submit">
					<span class="btn__cap header__logout-cap">Выйти из системы</span>
				</button>
			</form>
		</div>
	</div>
	<div class="header__bottom">
		<div class="fixed-block header__bottom-wrp">
			<div class="header__worker-col">
				<!--suppress HtmlUnknownTarget -->
				<img class="header__worker-image" src="graphics-worker-{{-this.worker.avatar}}.svg" alt="{{-this.worker.name}}" />
				<div class="header__worker">
					<p class="header__worker-name">{{-this.worker.name}}</p>
					<p class="header__worker-spec">{{-this.worker.spec}}</p>
					<p class="header__worker-department">{{-this.worker.department}}</p>
				</div>
			</div>
			<div class="header__menu-col">
				#menu#
			</div>
		</div>
	</div>
</header>