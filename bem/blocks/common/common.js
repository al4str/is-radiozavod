/**
 * Глобальная переменная приложения
 *
 * @class: App
 * @property {object} d — (DOM) Ссылки на часто используемые узлы
 * @property {object} h — (help) Вспомогательные функции
 * @property {object} c — (class) Классы инструментов и блоков
 * @property {object} window — Экземпляр класса, отвечающий за модальное окно
 * @property {object} xhr — Экземпляр класса, отвечающий за асинхронные запросы
 * @property {object} i — (instance) Коллекция экземпляров блоков
 *
 * @author Al4STR
 */
var App = {
	d: {},
	h: {},
	c: {
		tools: {},
		blocks: {}
	},
	i: {}
};
window.onload = function() {
	console.info('window.onload event');
	/**
	 * Window
	 * @type {object}
	 */
	App.d.w = window;
	/**
	 * Документ
	 * @type {object}
	 */
	App.d.d = document;
	/**
	 * Тег body
	 * @type {object}
	 */
	App.d.b = App.d.d.body;
	App.h.start_coordinates = [55.755833, 37.617778];
	App.h.primary_color = '#009688';
	/**
	 * Вызывает событие на указанном узле
	 *
	 * @namespace App.h
	 * @class trigger
	 *
	 * @param {object} $element — DOM узел
	 * @param {string} event_name — Название события
	 */
	App.h.trigger = function($element, event_name, params) {
		if (!$element || !event_name) return false;
		params = params || null;
		if (App.d.w.CustomEvent) {
			$element.dispatchEvent(new CustomEvent(event_name, {detail: params}));
		} else
		if (App.d.d.createEvent) {
			var ev = App.d.d.createEvent('HTMLEvents');

			ev.initEvent(event_name, true, false, params);
			$element.dispatchEvent(ev);
		} else {
			$element.fireEvent('on' + event_name);
		}
	};
	/**
	 * Возвращает отформатированную строку даты
	 * Формат ДД.ММ.ГГГГ
	 *
	 * @namespace App.h
	 * @class get_date_string
	 *
	 * @param {(date|string)} date — Экземпляр даты или валидная строка в международном формате
	 * @returns {string}
	 */
	App.h.get_date_string = function(date) {
		if (!date || (typeof date == 'string' && date.trim() == '')) return null;
		date = new Date(date);
		var day = date.getDate(),
			month = date.getMonth()+1,
			year = date.getFullYear();

		if (day < 10) day = '0' + day;
		if (month < 10) month = '0' + month;
		return day + '.' + month + '.' + year;
	};
	/**
	 * Возвращает отформатированную строку даты для поля типа DATE
	 * Формат ГГГГ-ММ-ДД
	 *
	 * @namespace App.h
	 * @class get_date_input_string
	 *
	 * @param {(date|string)} date — Экземпляр даты или валидная строка в международном формате
	 * @returns {string}
	 */
	App.h.get_date_input_string = function(date) {
		if (!date || (typeof date == 'string' && date.trim() == '')) return null;
		date = new Date(date);
		var day = date.getDate(),
			month = date.getMonth()+1,
			year = date.getFullYear();

		if (day < 10) day = '0' + day;
		if (month < 10) month = '0' + month;
		return year + '-' + month + '-' + day;
	};
	/**
	 * Возвращает отформатированную строку в процентах
	 * Формат 42%
	 *
	 * @namespace App.h
	 * @class get_percentage_string
	 *
	 * @param {(number|string)} value — Число или строка
	 * @returns {string}
	 */
	App.h.get_percentage_string = function(value) {
		value = Math.abs(parseInt(value));
		if (isNaN(value)) value = 0;
		if (value < 0) value = 0;
		if (value > 100) value = 100;
		return value + '%';
	};
	/**
	 * Имеет ли указанный узел указанного предка
	 *
	 * @namespace App.h
	 * @class has_parent
	 *
	 * @param {object} $node — Узел
	 * @param {(object|string)} $parent — Узел или класс узла родителя
	 * @returns {boolean}
	 */
	App.h.has_parent = function($node, $parent) {
		var parent_list = [], condition;

		if (typeof $parent == 'string') condition = 'class';
		else condition = 'node';
		while ($node) {
			if ($node == App.d.b) break;
			parent_list.push($node);
			parent_list.reverse();
			$node = $node.parentNode;
			if (condition == 'class') {
				if ($node.classList.contains($parent)) return true;
			} else
			if (condition == 'node') {
				if ($node == $parent) return true;
			}
		}
		return false;
	};
	/**
	 * Возвращает новый узел
	 *
	 * @namespace App.h
	 * @class get_node
	 *
	 * @param {string} class_name — Класс или строка из классов, разделённых пробелом
	 * @param {*} value — Значение узла (строка, число, узел)
	 * @param {string} tag — Тег узла
	 * @returns {object}
	 */
	App.h.get_node = function(class_name, value, tag) {
		class_name = class_name || null;
		value = value || null;
		tag = tag || 'div';
		var $node = App.d.d.createElement(tag);

		if (class_name) {
			var class_list = class_name.split(' ');

			for (var i = 0; i < class_list.length; i++) {
				$node.classList.add(class_list[i]);
			}
		}
		if (value) {
			if (value instanceof DocumentFragment) $node.appendChild(value);
			else $node.innerHTML = value;
		}
		return $node;
	};
	/**
	 * Возвращает предка указанного узла по указанному классу
	 *
	 * @namespace App.h
	 * @class get_parent
	 *
	 * @param {object} $node — Узел
	 * @param {string} $parent — Класс узла родителя
	 * @returns {object}
	 */
	App.h.get_parent = function($node, class_name) {
		var parent_list = [];

		if (!$node || !class_name) return false;
		while ($node) {
			if ($node == App.d.b) break;
			parent_list.push($node);
			parent_list.reverse();
			$node = $node.parentNode;
			if ($node.classList.contains(class_name)) return $node;
		}
		return false;
	};
	/**
	 * Возвращает объект календаря
	 *
	 * @namespace App.h
	 * @class make_calendar
	 *
	 * @param {object} $element — Поле типа DATE
	 * @returns {object}
	 */
	App.h.make_calendar = function($element) {
		var self = {};

		self.dom = {};
		self.opt = {};
		self.dom.$native = $element;
		if (!self.dom.$native) return false;
		if (isMobile.any) {
			self.dom.$field = self.dom.$native.cloneNode(false);
			self.dom.$field.setAttribute('type', 'hidden');
			self.dom.$native.removeAttribute('name');
			self.dom.$native.parentNode.appendChild(self.dom.$field);
		} else {
			self.dom.$field = self.dom.$native.cloneNode(false);
			self.dom.$field.setAttribute('type', 'text');
			self.dom.$native.style.display = 'none';
			self.dom.$native.disabled = true;
			self.dom.$native.parentNode.appendChild(self.dom.$field);
			self.opt.pikaday = new Pikaday({
				field: self.dom.$field,
				onSelect: function() {
					self.dom.$field.value = App.h.get_date_string(this.getDate());
					self.dom.$native.value = App.h.get_date_input_string(this.getDate());
					App.h.trigger(self.dom.$native, 'input');
				},
				firstDay: 1,
				i18n: {
					previousMonth : 'Предыдущий месяц',
					nextMonth : 'Следующий месяц',
					months : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					weekdays : ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
					weekdaysShort : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
				}
			});
			if (!self.dom.$native.value || self.dom.$native.value.trim().length == '') self.opt.pikaday.setDate(new Date());
			else self.opt.pikaday.setDate(new Date(self.dom.$native.value));
		}
		return self;
	};
	/**
	 * Возвращает обработанный объект ошибок запроса или false
	 *
	 * @namespace App.h
	 * @class parse_errors
	 *
	 * @param {object} error_object — Объект ответа запроса
	 * @returns {(object|bollean)}
	 */
	App.h.parse_errors = function(error_object) {
		var self = {};

		self.types = {};
		self.required_fields = [];
		if (error_object['result'] == 'error') {
			self.responses = error_object['response'];

			for (var response_i = 0; response_i < self.responses.length; response_i++) {
				var response = self.responses[response_i];

				if (response['name'] == 'required_fields') {
					var fields = response['description'];

					for (var field_i = 0; field_i < fields.length; field_i++) {
						self.required_fields.push(fields[field_i]);
					}
				}
				self.types[response['name']] = response['code'];
			}
			return self;
		}
		else return false;
	};
	/**
	 * Показывает нотификацию с ошибкой (нужен рефакторинг)
	 *
	 * @namespace App.h
	 * @class make_notification
	 *
	 * @param {object} $element — Узел нотификации
	 * @param {string} type — Тип ошибки
	 * @param {string} message — Название ошибки
	 */
	App.h.make_notification = function($element, type, message) {
		var $text, text;

		if (!$element || !type) return false;
		$text = $element.querySelector('.text');
		if ($element.hasAttribute('data-' + type + '-' + message)) text = $element.getAttribute('data-' + type + '-' + message);
		else {
			if (type == 'danger') text = $element.getAttribute('data-' + type + '-default') + message;
			else text = $element.getAttribute('data-' + type + '-default');
		}
		$text.textContent = text;
		$element.setAttribute('data-type', type);
		$element.style.display = 'block';
	};
	/**
	 * Показывает нотификацию с ошибкой (нужен рефакторинг)
	 *
	 * @namespace App.h
	 * @class show_notification
	 *
	 * @param {object} $element — Узел нотификации
	 * @param {string} type — Тип ошибки
	 * @param {string} message — Название ошибки
	 * @param {string} code — Код ошибки
	 */
	App.h.show_notification = function($element, type, message, code) {
		var $text, text;

		if (!$element || !type) return false;
		$text = $element.querySelector('.text');
		if ($element.hasAttribute('data-' + type + '-' + message)) text = $element.getAttribute('data-' + type + '-' + message);
		else {
			if (type == 'danger') text = $element.getAttribute('data-' + type + '-default') + code;
			else text = $element.getAttribute('data-' + type + '-default');
		}
		$text.textContent = text;
		$element.setAttribute('data-type', type);
		$element.style.display = 'block';
	};
	/**
	 * Возвращает новый массив, без указанного элемента
	 *
	 * @namespace App.h
	 * @class remove_from_array
	 *
	 * @param {array} array — Массив
	 * @param {*} value — Элемент массива
	 * @returns {array}
	 */
	App.h.remove_from_array = function(array, value) {
		var value_i = array.indexOf(value);

		if (value_i != -1) array.splice(value_i, 1);
		return array;
	};
	/**
	 * Возвращает массив, состоящий из следующих за указанным узлов
	 *
	 * @namespace App.h
	 * @class get_next_siblings
	 *
	 * @param {object} $node — Узел
	 * @param {object} $node_to_skip — Узел, который не должен попасть в возращаемый массив
	 * @returns {array}
	 */
	App.h.get_next_siblings = function($node, $node_to_skip){
		var $children = [];

		for (; $node; $node = $node.nextSibling)
			if ($node.nodeType == 1 && $node != $node_to_skip)
				$children.push($node);
		return $children;
	};
	/**
	 * Возвращает массив из всех соседних узлов указанного, без него самого
	 *
	 * @namespace App.h
	 * @class get_siblings
	 *
	 * @param {object} $node — Узел
	 * @returns {array}
	 */
	App.h.get_siblings = function($node) {
		return App.h.get_next_siblings($node.parentNode.firstChild, $node);
	};
	/**
	 * Возвращает отформатированную строку цены
	 * Формат 1 000 000
	 *
	 * @namespace App.h
	 * @class format_cost
	 *
	 * @param {(number|string)} value — Значение
	 * @param {string} separator — Разделитель тысячных
	 * @returns {string}
	 */
	App.h.format_cost = function(value, separator) {
		var value_length, value_formatted, count = 0;

		separator = separator || ' ';
		value = '' + (value || 0);
		value_length = value.length;
		value_formatted = value.split('');
		if (value_length > 3) {
			value = value_formatted;
			App.h.i(value, function(c, i){
				if ((value_length-i)%3 == 0) {
					value_formatted.splice(i+count, 0, separator);
					count++;
				}
			});
		}
		return value_formatted.join('').trim();
	};
	/**
	 * Удаляет узел
	 *
	 * @namespace App.h
	 * @class remove_node
	 *
	 * @param {object} $node — Узел
	 */
	App.h.remove_node = function($node) {
		if (!$node) return false;
		if ($node.parentNode) $node.parentNode.removeChild($node);
	};
	/**
	 * Вызов нотификации с ошибкой. Попахивает костылём (нужен рефакторинг)
	 *
	 * @namespace App.h
	 * @class show_notification_error
	 *
	 * @param {object} response_errors — Узел нотификации
	 * @param {string} $notification — Тип ошибки
	 */
	App.h.show_notification_error = function(response_errors, $notification) {
		for (var name_i in response_errors) {
			if (response_errors.hasOwnProperty(name_i)) {
				App.h.show_notification($notification, 'danger', name_i, response_errors[name_i]);
				break;
			}
		}
	};
	/**
	 * Итерация по элементам массива или свойствам объекта
	 * Возвращает false, если array не валиден
	 *
	 * @namespace App.h
	 * @class i
	 *
	 * @param {(array|object)} array — Массив или объект
	 * @param {function} callback — Функция, срабатываемая на каждой итерации (передаются значение и индекс текущей итерации)
	 * @returns {boolean}
	 */
	App.h.i = function(array, callback) {
		if (!array || typeof callback !== 'function') return false;
		var i;

		if (array instanceof Array || array instanceof HTMLCollection) {
			for (i = 0; i < array.length; i++) {
				//console.debug('i: item ', i, array[i]);
				callback(array[i], i);
			}
		} else
		if (array instanceof Object) {
			for (i in array) {
				// STUPID IOS8 BUG
				if (array.hasOwnProperty(i) && i != 'length') {
					//console.debug('i: attribute ', i, array[i]);
					callback(array[i], i);
				}
			}
		} else return false;
	};
	/**
	 * Возвращает значения top, left узла от начала документа
	 *
	 * @namespace App.h
	 * @class get_offset
	 *
	 * @param {object} $node — Узел
	 * @returns {(object|boolean)}
	 */
	App.h.get_offset = function($node) {
		var self = {};

		self.opt = {};
		self.dom = {};
		self.dom.$node = $node;
		if (!self.dom.$node) return false;
		self.opt.node_bounds = self.dom.$node.getBoundingClientRect();
		self.dom.$document_element = App.d.d.documentElement;
		self.opt.scrollTop = App.d.w.pageYOffset || self.dom.$document_element.scrollTop || App.d.b.scrollTop;
		self.opt.scrollLeft = App.d.w.pageXOffset || self.dom.$document_element.scrollLeft || App.d.b.scrollLeft;
		self.opt.clientTop = self.dom.$document_element.clientTop || App.d.b.clientTop || 0;
		self.opt.clientLeft = self.dom.$document_element.clientLeft || App.d.b.clientLeft || 0;
		self.opt.top = self.opt.node_bounds.top + self.opt.scrollTop - self.opt.clientTop;
		self.opt.left = self.opt.node_bounds.left + self.opt.scrollLeft - self.opt.clientLeft;
		return {
			top: Math.round(self.opt.top),
			left: Math.round(self.opt.left)
		};
	};
	/**
	 * Возвращает значения top, left узла от прямого предка
	 *
	 * @namespace App.h
	 * @class get_position
	 *
	 * @param {object} $node — Узел
	 * @returns {(object|boolean)}
	 */
	App.h.get_position = function($node) {
		var self = {};

		self.opt = {};
		self.dom = {};
		self.dom.$node = $node;
		if (!self.dom.$node) return false;
		self.dom.$parent = self.dom.$node.parentNode;
		if (!self.dom.$parent) return false;
		return {
			top: Math.round(App.h.get_offset(self.dom.$node).top-App.h.get_offset(self.dom.$parent).top),
			left: Math.round(App.h.get_offset(self.dom.$node).left-App.h.get_offset(self.dom.$parent).left)
		};
	};
	/**
	 * Показывает индикатор загрузки данных поверх указанного узла
	 * Внимание: подходит не каждый узел
	 *
	 * @namespace App.h
	 * @class load_node
	 *
	 * @param {object} $node — Узел
	 * @param {boolean} turn_off — Если false, включает индикатор и при true отключает
	 * @returns {boolean}
	 */
	App.h.load_node = function($node, turn_off) {
		var self = {};

		self.opt = {};
		self.dom = {};
		self.change_top = function() {
			self.opt.scroll_amount = App.d.w.pageYOffset || App.d.d.documentElement.scrollTop;
			var window_near_point = self.opt.scroll_amount,
				window_far_point = self.opt.scroll_amount+self.opt.windows_height,
				node_near_point = self.opt.node_offsets.top,
				node_far_point = self.opt.node_offsets.top+self.opt.node_height,
				hidden_far = node_far_point-window_far_point,
				hidden_near = window_near_point-node_near_point;

			if (hidden_far <= 0) hidden_far = 0;
			if (hidden_near <= 0) hidden_near = 0;
			self.opt.top = (self.opt.node_height-hidden_far+hidden_near)/2-self.opt.loader_height/2;
			if (self.opt.top <= self.opt.min_offset) self.opt.top = self.opt.min_offset;
			if (self.opt.top >= self.opt.max_offset) self.opt.top = self.opt.max_offset;
			self.dom.$load_node.style.top = self.opt.top + 'px';
		};
		self.turn_off = function() {
			setTimeout(function(){
				self.dom.$node.removeAttribute('data-load');
				if (!self.dom.$node.load_node) {
					self.dom.$load_node = self.dom.$node.querySelector('.load-node');
				} else {
					self.dom.$load_node = self.dom.$node.load_node.dom.$load_node;
				}
				if (self.dom.$load_node) App.h.remove_node(self.dom.$load_node); //self.dom.$node.removeChild(self.dom.$load_node);
				self.dom.$node.load_node = null;
			}, 300);
		};
		self.turn_on = function() {
			if (self.dom.$node.load_node) return false;
			self.dom.$node.setAttribute('data-load', '');
			self.dom.$load_node = App.h.get_node('load-icon');
			if (self.dom.$node.children.length > 0) self.dom.$node.insertBefore(self.dom.$load_node, self.dom.$node.children[0]);
			else self.dom.$node.appendChild(self.dom.$load_node);
			self.dom.$node.load_node = self;
			if (self.opt.node_height > self.opt.windows_height) {
				self.opt.loader_height = self.dom.$load_node.getBoundingClientRect().height;
				self.opt.node_offsets = App.h.get_offset(self.dom.$node);
				self.opt.min_offset = 50;
				self.opt.max_offset = self.opt.node_height-self.opt.min_offset-self.opt.loader_height;
				self.change_top();
				App.d.w.addEventListener('scroll', self.change_top, false);
			}
		};
		self.initiate = function() {
			self.dom.$node = $node;
			self.opt.turn_off = turn_off;
			if (!self.dom.$node) return false;
			self.opt.windows_height = App.d.w.innerHeight;
			self.opt.node_height = (self.dom.$node.getBoundingClientRect)?self.dom.$node.getBoundingClientRect().height:null;
			if (self.opt.turn_off) {
				self.turn_off();
			} else {
				self.turn_on();
			}
		};
		self.initiate();
	};
	/**
	 * Прокрутка документа до указанного узла или на конкретную позицию
	 *
	 * @namespace App.h
	 * @class scroll_to
	 *
	 * @param {(number|object)} input — Узел или число
	 * @param {number} Предыдущее значение прокрутки (необязательный параметр)
	 * @returns {boolean}
	 */
	App.h.scroll_to = function(input, previos_scroll_top) {
		var self = {};

		self.opt = {};
		self.dom = {};
		self.opt.previos_scroll_top = parseInt(previos_scroll_top) || null
		if (input instanceof Node) self.opt.to = App.h.get_offset(input).top;
		else if (typeof parseInt(input) === 'number') self.opt.to = parseInt(input);
		else return false;
		self.opt.difference = self.opt.to - App.d.b.scrollTop;
		self.opt.per_tick = self.opt.difference/150*1000/60;

		self.opt.clock = setTimeout(function() {
			App.d.b.scrollTop = App.d.b.scrollTop + self.opt.per_tick;
			if ((self.opt.difference >= 0 && App.d.b.scrollTop > self.opt.to || self.opt.difference < 0 && App.d.b.scrollTop < self.opt.to) || self.opt.previos_scroll_top == App.d.b.scrollTop) {
				clearTimeout(self.opt.clock);
				return false;
			}
			self.opt.previos_scroll_top = App.d.b.scrollTop;
			App.h.scroll_to(self.opt.to, self.opt.previos_scroll_top);
		}, 1000/60);
	};
	/**
	 * Добавляет после указанного узла другой
	 *
	 * @namespace App.h
	 * @class insert_after
	 *
	 * @param {object} $node — Узел
	 * @param {object} $after — Узел, который нужно вставить после указанного
	 */
	App.h.insert_after = function($node, $after) {
		if (!$node || !$after) return false;
		$after.parentNode.insertBefore($node, $after.nextSibling);
	};
	/**
	 * Возвращает узел, выдернутый из DOM
	 *
	 * @namespace App.h
	 * @class detach
	 *
	 * @param {object} $node — Узел
	 * @returns {(object|boolean)}
	 */
	App.h.detach = function($node) {
		var $parent;

		if (!$node) return false;
		$parent = $node.parentNode;
		if (!$parent) return false;
		return $parent.removeChild($node);
	};
	/**
	 * Возвращает случайное число из указанного диапазона
	 *
	 * @namespace App.h
	 * @class get_random_int
	 *
	 * @param {number} min — Минимальное значение
	 * @param {number} max — Максимальное
	 * @returns {number}
	 */
	App.h.get_random_int = function(min, max) {
		min = min || 0;
		max = max || 1;
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};
	/**
	 * Возвращает объект из json строки
	 *
	 * @namespace App.h
	 * @class get_json
	 *
	 * @param {string} string — Минимальное значение
	 * @returns {boolean}
	 */
	App.h.get_json = function(string) {
		try {
			return JSON.parse(string);
		} catch (e) {
			return false;
		}
	};
	/**
	 * Возвращает размер объект
	 *
	 * @namespace App.h
	 * @class get_object_length
	 *
	 * @param {object} object — входящий объект
	 * @returns {number|boolean}
	 */
	App.h.get_object_length = function(object) {
		if (!object instanceof Object) return false;
		var length = 0;

		App.h.i(object, function(item){
			length++;
		});
		return length;
	};
	/**
	 * Возвращает искомый(ые) объекты
	 *
	 * @namespace App.h
	 * @class find
	 *
	 * @param {object} list — входящий лист объектов
	 * @param {object} search — входящий объект параметров поиска
	 * @param {object} all — вернуть один или все найденные
	 * @returns {object|boolean}
	 */
	App.h.find = function(list, search, all) {
		/* list: object or array */
		/* search: {name1: value1, name2: value2, ... } */
		if (!list || !search) return false;
		all = (all)?true:false;
		var result = (all)?null:[],
			matches = App.h.get_object_length(search),
			found = false;

		App.h.i(list, function(item){
			if (!found || all) {
				var item_matches = 0;

				App.h.i(search, function(search_value, search_name){
					if (item.hasOwnProperty(search_name) && item[search_name] == search_value) item_matches++;
				});
				if (item_matches == matches) {
					if (!all) result = item;
					else result.push(item);
					found = true;
				}
			}
		});
		return result;
	};
	/**
	 * Подмена url парметра на другое значение
	 *
	 * @namespace App.h
	 * @class replace_param
	 *
	 * @param {string} url — входящий url
	 * @param {string} param — название параметра
	 * @param {string} value — новое значение
	 * @returns {string}
	 */
	App.h.replace_param = function(url, param, value) {
		if (value == null) value = '';
		var pattern = new RegExp('\\b('+param+'=).*?(&|$)');

		if (url.search(pattern) >= 0) return url.replace(pattern,'$1' + value + '$2');
		else return url + (url.indexOf('?')>0 ? '&' : '?') + param + '=' + value
	};
	/**
	 * Возвращает уникальную строку
	 *
	 * @namespace App.h
	 * @class get_uniqe_id
	 *
	 * @param {string} string — префикс
	 * @returns {string}
	 */
	App.h.get_uniqe_id = function(string) {
		var phrases = ['Alpha', 'Bravo', 'Charlie', 'Delta', 'Echo', 'Foxtrot', 'Golf', 'Hotel', 'India', 'Juliet', 'Kilo', 'Lima', 'Mike', 'November', 'Oscar', 'Papa', 'Quebec', 'Romeo', 'Sierra', 'Tango', 'Uniform', 'Victor', 'Whiskey', 'X-ray', 'Yankee', 'Zulu'];

		string = string || phrases[App.h.get_random_int(0, phrases.length-1)];
		return string.trim().toLowerCase() + '-' + new Date().getTime() + '-' + App.h.get_random_int(10, 100);
	};
	/**
	 * Возвращает массив координат
	 *
	 * @namespace App.h
	 * @class get_coordinates
	 *
	 * @param {object|string} input — входные данные
	 * @returns {object|boolean}
	 */
	App.h.get_coordinates = function(input) {
		var coordinates, latitude, longitude;

		if (!input) return false;
		if (typeof input === 'string') {
			coordinates = input.split(',');
			if (coordinates[1] == undefined) coordinates = input.split(' ');
			latitude = coordinates[0];
			longitude = coordinates[1];
		} else if (typeof input === 'object' && input[0] && input[1]) {
			latitude = input[0];
			longitude = input[1];
		} else return false;
		latitude = parseFloat(latitude);
		longitude = parseFloat(longitude);
		if (!isNaN(latitude) && !isNaN(longitude)) return [latitude, longitude];
		else return false;
	};
	/**
	 * Возвращает строку html node
	 *
	 * @namespace App.h
	 * @class get_html_string
	 *
	 * @param {object} $node — html node
	 * @param {boolean} clear_whitespace — удалять ли вайтспейсы
	 * @returns {boolean|string}
	 */
	App.h.get_html_string = function($node, clear_whitespace) {
		var $temp_node = App.h.get_node(), result;

		if (!$node) return false;
		$temp_node.appendChild($node.cloneNode(true));
		result = $temp_node.innerHTML;
		if (clear_whitespace) result.replace(/\n/g, '').replace(/\t/g, '');
		return result
	};
	/**
	 * Выполняет действие компонента после готовности последнего
	 *
	 * @namespace App.h
	 * @class postponed_action
	 *
	 * @param {object} $node — html node
	 * @param {string} action — действие
	 * @param {string} argument — аргументы к действию
	 */
	App.h.postponed_action = function($node, action, argument) {
		if (!$node || !action) return false;
		if ($node.js) $node.js[action](argument);
		else $node.addEventListener('is-ready', function(){
			$node.js[action](argument);
		}, false);
	};
	App.h.remove_location_hash = function() {
		App.d.w.history.pushState("", App.d.d.title, App.d.w.location.pathname + App.d.w.location.search);
	};

	/**
	 * XMLRequest транспорт
	 * Отвечает за все асинхронные запросы
	 *
	 * @namespace App.c.tools
	 * @class XHR
	 * @returns {object}
	 */
	App.c.tools.XHR = function() {
		var self = this;

		self.x = function () {
			if (typeof XMLHttpRequest !== 'undefined') {
				return new XMLHttpRequest();
			}
			var versions = [
				"MSXML2.XmlHttp.6.0",
				"MSXML2.XmlHttp.5.0",
				"MSXML2.XmlHttp.4.0",
				"MSXML2.XmlHttp.3.0",
				"MSXML2.XmlHttp.2.0",
				"Microsoft.XmlHttp"
			];

			var xhr;
			for (var i = 0; i < versions.length; i++) {
				try {
					xhr = new ActiveXObject(versions[i]);
					break;
				} catch (e) {}
			}
			return xhr;
		};
		self.send = function (url, callback, method, data, async) {
			if (async === undefined) {
				async = true;
			}
			var x = self.x();

			x.open(method, url, async);
			x.onreadystatechange = function () {
				if (x.readyState == 4 && typeof callback === 'function') {
					callback(x.responseText)
				}
			};
			if (!(data instanceof FormData)) {
				x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			}
			x.send(data)
		};
		self.upload = function (url, data, method, on_end, on_progress) {
			var async = true;
			var x = self.x();

			x.open(method, url, async);
			x.onreadystatechange = function () {
				if (x.readyState == 4 && typeof on_end === 'function') {
					on_end(x.responseText)
				}
			};
			x.upload.onprogress = function(e) {
				if (typeof on_progress === 'function') {
					on_progress(e);
				}
			}
			x.send(data)
		};
		self.get = function (url, data, callback, async) {
			var query = [];
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
				}
			}
			self.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async)
		};
		self.post = function (url, data, callback, async) {
			var query = [];
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
				}
			}
			self.send(url, callback, 'POST', query.join('&'), async)
		};
		self.json = function(url, callback, data) {
			var x = self.x();

			x.open('POST', url, true);
			x.onreadystatechange = function () {
				if (x.readyState == 4 && typeof callback === 'function') {
					callback(x.responseText);
				}
			};
			x.setRequestHeader("Content-Type", "application/json");
			x.send(JSON.stringify(data));
		}
	};
	/**
	 * Элемент единичного выбора из выпадающего списка
	 *
	 * @namespace App.c.tools
	 * @class SelectBlock
	 * @returns {object}
	 */
	App.c.tools.SelectBlock = function($block) {
		var self = this;

		self.opt = {};
		self.dom = {};
		self.enable = function() {
			self.dom.$field.disabled = false;
			self.dom.$btn.disabled = false;
			App.h.trigger(self.dom.$block, 'is-enabled');
		};
		self.disable = function() {
			self.opt.is_shown = false;
			self.dom.$block.removeAttribute('data-is-shown');
			self.dom.$field.disabled = true;
			self.dom.$btn.disabled = true;
			App.h.trigger(self.dom.$block, 'is-disabled');
		};
		self.show = function() {
			self.opt.is_shown = true;
			self.dom.$block.setAttribute('data-is-shown', '');
			self.dom.$block.setAttribute('data-interacted', '');
			App.h.trigger(self.dom.$block, 'is-shown');
			if (self.opt.selected) {
				self.dom.$list.scrollTop = self.opt.selected.$node.offsetTop;
				self.opt.previous_value = self.opt.selected['data']['value'];
			}
		};
		self.hide = function() {
			self.opt.is_shown = false;
			self.dom.$block.removeAttribute('data-is-shown');
			App.h.trigger(self.dom.$block, 'is-hidden');
			if (self.opt.selected && self.opt.previous_value != self.opt.selected['data']['value']) {
				App.h.trigger(self.dom.$block, 'is-changed');
			}
		};
		self.toggle = function() {
			if (self.opt.is_shown) self.hide();
			else self.show();
		};
		self.on_native_select = function() {
			var value = self.dom.$field.value,
				selected_option = App.h.find(self.opt.options_data, {value: value});

			App.h.i(self.opt.options_data, function(option_data){
				option_data['selected'] = false;
			});
			selected_option['selected'] = true;
			self.opt.selected = {
				data: {
					value: selected_option.value,
					title: selected_option.title
				}
			};
			self.dom.$btn_cap.textContent = selected_option.title;
			App.h.trigger(self.dom.$block, 'is-changed');
		};
		self.on_select = function(e) {
			var option = this;

			if (!option) return false;
			App.h.i(self.opt.options, function(option_object){
				option_object.$node.classList.remove('select-block__option-btn_selected');
			});
			option.$node.classList.add('select-block__option-btn_selected');
			self.opt.selected = option;
			self.dom.$btn_cap.textContent = option.data.title;
			self.dom.$field.value = option.data.value;
			if (e && e.type == 'click') self.hide();
		};
		self.on_arrow_select = function(direction) {
			var index = self.opt.options.indexOf(self.opt.selected),
				new_index = index+((direction=='up')?-1:1);

			if (!self.opt.selected || index == -1 || !self.opt.options[new_index]) {
				index = (direction=='up')?self.opt.options.length-1:0;
				self.opt.selected = self.opt.options[index];
			} else {
				self.opt.selected = self.opt.options[new_index];
			}
			self.on_select.call(self.opt.selected);
		};
		self.on_key_down = function(e) {
			if (self.opt.is_shown) {
				if (e.which == 32) {
					self.opt.search += ' ';
					e.stopPropagation();
					e.preventDefault();
				} else
				if ([9, 27].indexOf(parseInt(e.which)) != -1) {
					self.hide();
					self.dom.$btn.focus();
					e.stopPropagation();
					e.preventDefault();
				} else
				if (e.which == 38) {
					self.on_arrow_select('up');
					e.stopPropagation();
					e.preventDefault();
				} else
				if (e.which == 40) {
					self.on_arrow_select('down');
					e.stopPropagation();
					e.preventDefault();
				}
			}
		};
		self.on_key_press = function(e) {
			if (self.opt.is_shown) {
				var char_code = (typeof e.which == "number")?e.which:e.keyCode,
					typed_char = String.fromCharCode(char_code);

				self.opt.search += typed_char;
				clearTimeout(self.opt.interval);
				self.opt.interval = setTimeout(function(){
					self.search_option(self.opt.search);
					self.opt.search = '';
				}, 300);
			}
		};
		self.on_outside_click = function(e) {
			if (!self.opt.is_shown) return false;
			var is_target_block = (e.target == self.dom.$block),
				has_target_block = App.h.has_parent(e.target, self.dom.$block);

			if (!is_target_block && !has_target_block) self.hide();
		};
		self.on_outside_key_down = function(e) {
			if (self.opt.is_shown) {
				if (e.which == 27) {
					self.hide();
					self.dom.$btn.focus();
					e.stopPropagation();
					e.preventDefault();
				}
			}
		};
		self.search_option = function(search_phrase) {
			search_phrase = ('' + search_phrase).trim().toLocaleLowerCase();
			if (search_phrase.length == 0) return false;
			App.h.i(self.opt.options, function(option){
				var title = option['data']['title'].trim().toLocaleLowerCase();

				if (title.indexOf(search_phrase) == 0) {
					self.dom.$list.scrollTop = option.$node.offsetTop;
				}
			});
		};
		self.get_options = function(input) {
			if (!input) {
				input = [];
				App.h.i(self.dom.$field.children, function($node){
					input.push({
						title: $node.textContent,
						value: $node.value,
						selected: $node.selected
					});
				});
			} else input = App.h.get_json(input);
			if (input.length == 0) input = null;
			self.opt.options_data = input;
		};
		self.if_mobile = function() {
			if (!self.opt.options_data) self.disable();
			var selected_option = App.h.find(self.opt.options_data, {selected: true});

			if (!selected_option || !self.opt.selected) {
				self.opt.selected = {
					data: {
						value: self.opt.options_data[0].value,
						title: self.opt.options_data[0].title
					}
				};
				self.dom.$btn_cap.textContent = self.opt.options_data[0].title;
			}
		};
		self.render_options = function() {
			if (!self.opt.options_data) self.disable();
			App.h.i(self.opt.options, function(option_object){
				App.h.remove_node(option_object.$node);
				option_object = null;
			});
			self.opt.options = [];
			self.opt.selected = null;
			App.h.i(self.opt.options_data, function(option_data){
				var option = {};

				option.data = option_data;
				option.$node = App.h.get_node('btn select-block__option-btn', '', 'button');
				option.$node.type = 'button';
				option.$node.tabIndex = '-1';
				option.$node.title = option.data.title || '';
				option.$node_cap = App.h.get_node('btn__cap', option.data.title, 'span');
				//option.$node.js = option;
				option.$node.appendChild(option.$node_cap);
				self.dom.$list.appendChild(option.$node);
				if (option.data.selected) {
					option.$node.classList.add('select-block__option-btn_selected');
					self.opt.selected = option;
					self.dom.$btn_cap.textContent = option.data.title;
					self.dom.$field.value = option.data.value;
				}
				self.opt.options.push(option);
				option.$node.addEventListener('click', self.on_select.bind(option), false);
			});
			if (!self.opt.selected) {
				self.opt.options[0].$node.classList.add('select-block__option-btn_selected');
				self.opt.selected = self.opt.options[0];
				self.dom.$btn_cap.textContent = self.opt.options[0].data.title;
				self.dom.$field.value = self.opt.options[0].data.value;
			}
		};
		self.render_mobile = function() {
			self.dom.$btn = self.dom.$block.querySelector('.select-block__btn');
			self.dom.$btn_cap = self.dom.$block.querySelector('.select-block__btn-cap');
			self.dom.$field = self.dom.$block.querySelector('.select-block__field');
			self.dom.$block.setAttribute('data-is-mobile', '');
			self.dom.$btn.tabIndex = '-1';
			self.dom.$field.addEventListener('change', self.on_native_select, false);
		};
		self.render = function() {
			self.dom.$btn = self.dom.$block.querySelector('.select-block__btn');
			self.dom.$btn_cap = self.dom.$block.querySelector('.select-block__btn-cap');
			self.dom.$field = self.dom.$block.querySelector('.select-block__field');
			self.dom.$list = App.h.get_node('select-block__list', '', '');
			self.dom.$block.appendChild(self.dom.$list);
			if (self.dom.$btn.disabled) self.disable();
			self.dom.$btn.onclick = self.toggle;
			self.dom.$block.addEventListener("keydown", self.on_key_down, false);
			App.d.d.addEventListener('click', self.on_outside_click, false);
			App.d.d.addEventListener("keydown", self.on_outside_key_down, false);
			App.d.d.addEventListener("keypress", self.on_key_press, false);
		};
		self.initiate = function() {
			self.dom.$block = $block;
			if (!self.dom.$block) {
				self = null;
				return false;
			}
			self.opt.previous_value = null;
			self.opt.interval = null;
			self.opt.search = '';
			self.opt.is_shown = false;
			self.opt.selected = null;
			self.opt.options = [];
			if (isMobile.any) {
				self.render_mobile();
				self.get_options();
				self.if_mobile();
			} else {
				self.render();
				self.get_options();
				self.render_options();
				self.dom.$block.classList.add('select-block_desktop');
			}
			self.dom.$block.js = self;
			App.h.trigger(self.dom.$block, 'is-ready');
		};
		self.initiate();
		return self;
	};
	/**
	 * Элемент выбора адреса
	 *
	 * @namespace App.c.tools
	 * @class AddressBlock
	 * @returns {object}
	 */
	App.c.tools.AddressBlock = function($block) {
		var self = this;

		self.opt = {};
		self.dom = {};
		self.enable = function() {
			self.dom.$coordinates_field.disabled = false;
			self.dom.$field.disabled = false;
			if (self.dom.$btn) self.dom.$btn.disabled = false;
			App.h.trigger(self.dom.$block, 'is-enabled');
		};
		self.disable = function() {
			self.opt.is_shown = false;
			self.dom.$block.removeAttribute('data-is-shown');
			self.dom.$coordinates_field.disabled = true;
			self.dom.$field.disabled = true;
			if (self.dom.$btn) self.dom.$btn.disabled = true;
			//self.dom.$block.setAttribute('data-disabled', '');
			App.h.trigger(self.dom.$block, 'is-disabled');
		};
		self.show = function() {
			self.opt.is_shown = true;
			self.dom.$block.setAttribute('data-is-shown', '');
			App.h.trigger(self.dom.$block, 'is-shown');
		};
		self.hide = function() {
			self.opt.is_shown = false;
			self.dom.$block.removeAttribute('data-is-shown');
			App.h.trigger(self.dom.$block, 'is-hidden');
			self.on_blur();
		};
		self.on_blur = function() {
			if (!self.opt.selected) {
				self.dom.$coordinates_field.value = '';
				self.dom.$field.value = '';
				App.h.trigger(self.dom.$block, 'is-cleared');
			}
		};
		self.on_done_click = function() {
			if (!self.opt.temp_place) return false;
			self.on_select.call(self.opt.temp_place);
			App.modal.hide();
		};
		self.on_btn_click = function() {
			App.modal.show(self.dom.$modal);
			if (App.d.w.innerWidth < 480) {
				var modal_height = self.dom.$modal.clientHeight,
					heading_height = self.dom.$modal.querySelector('.address-modal__line').clientHeight,
					map_height = modal_height-heading_height;

				//console.debug('2', modal_height, heading_height, map_height);
				self.dom.$modal.querySelector('.address-modal__map').style.height = map_height + 'px';
			}
		};
		self.on_map_hide = function() {
			self.dom.$field.focus();
			//self.opt.temp_place = null;
		};
		self.on_map_select = function(e) {
			//console.info('on_map_select');
			var coordinates = e.get('coords');

			if (!self.opt.map_pin) self.set_pin(coordinates);
			else self.set_pin_position(coordinates);
			//console.log('AddressBlock:on_map_select', coordinates);
			ymaps.geocode(coordinates, {results: 1}).then(function(res){
				var place = res['geoObjects'].get(0);

				//console.log('AddressBlock:on_map_select', place);
				self.dom.$done_btn.disabled = false;
				self.set_pin_properties(place['properties']);
				self.opt.temp_place = {
					data: {
						name: place['properties'].get('name'),
						description: place['properties'].get('description')
					},
					title: place['properties'].get('name') + ', ' + place['properties'].get('description'),
					raw_coordinates: coordinates,
					string_coordinates: coordinates[0] + ',' + coordinates[1]
				};
				if (self.opt.immidiate_select) {
					//console.info('immidiate_select');
					self.opt.selected = self.opt.temp_place;
					self.dom.$coordinates_field.value = self.opt.temp_place.string_coordinates;
					self.dom.$field.value = self.opt.temp_place.title;
				}
			});
		};
		self.on_key_down = function(e) {
			if (self.opt.is_shown) {
				if (e.which == 27) {
					e.stopPropagation();
					e.preventDefault();
					self.hide();
					self.dom.$field.focus();
				} else
				if (e.which == 9) {
					e.stopPropagation();
					e.preventDefault();
					self.hide();
					if (self.dom.$btn) self.dom.$btn.focus();
				} else
				if (e.which == 38) {
					e.stopPropagation();
					e.preventDefault();
					self.on_arrow_select('up');
				} else
				if (e.which == 40) {
					e.stopPropagation();
					e.preventDefault();
					self.on_arrow_select('down');
				} else
				if (e.which == 13) {
					e.stopPropagation();
					e.preventDefault();
					self.hide();
				}
			}
		};
		self.on_outside_click = function(e) {
			if (!self.opt.is_shown) return false;
			var is_target_block = (e.target == self.dom.$block),
				has_target_block = App.h.has_parent(e.target, self.dom.$block);

			if (!is_target_block && !has_target_block) self.hide();
		};
		self.on_outside_key_down = function(e) {
			if (self.opt.is_shown) {
				if (e.which == 27) {
					self.hide();
					self.dom.$field.focus();
					e.stopPropagation();
					e.preventDefault();
				}
			}
		};
		self.on_input = function(e) {
			//console.info('on_input');
			clearTimeout(self.opt.interval);
			if (self.dom.$field.value.trim() == '') {
				self.hide();
				self.clear_list();
				return false;
			}
			self.opt.interval = setTimeout(function(){
				self.get_address(self.dom.$field.value.trim());
			}, 300);
		};
		self.on_select = function(e) {
			//console.info('on_select');
			var option = this;

			if (!option) return false;
			if (e && e.type == 'click') self.hide();
			App.h.i(self.opt.options, function(option_object) {
				option_object.$node.classList.remove('address-block__option-btn_selected');
			});
			if (option.$node) option.$node.classList.add('address-block__option-btn_selected');
			self.opt.selected = option;
			self.dom.$coordinates_field.value = option.string_coordinates;
			self.dom.$field.value = option.title;
		};
		self.on_arrow_select = function(direction) {
			//console.info('on_arrow_select');
			var index = self.opt.options.indexOf(self.opt.selected),
				new_index = index+((direction=='up')?-1:1);

			if (!self.opt.selected || index == -1 || !self.opt.options[new_index]) {
				index = (direction=='up')?self.opt.options.length-1:0;
				self.opt.selected = self.opt.options[index];
			} else {
				self.opt.selected = self.opt.options[new_index];
			}
			self.on_select.call(self.opt.selected);
			if (!self.opt.map_pin) self.set_pin(self.opt.selected.raw_coordinates);
			else self.set_pin_position(self.opt.selected.raw_coordinates);
			self.set_pin_properties({
				get: function(key) {
					if (key == 'name') return self.opt.selected.data['name']; else
					if (key == 'text') return self.opt.selected.title;
				}
			});
		};
		self.get_address = function(address) {
			if (!self.opt.location) return false;
			ymaps.geocode(self.opt.location + ', ' + address, {
				results: 5,
				json: true
			}).then(function(response){
				self.render_options(response['GeoObjectCollection']['featureMember'], address);
			});
		};
		self.get_location = function(is_update) {
			var coordinates,
				location = ('' + self.dom.$block.getAttribute('data-location')).trim(),
				start_coordinates = App.h.get_coordinates(self.dom.$block.getAttribute('data-center'));

			//console.log('AddressBlock:get_location', location, start_coordinates);
			if (self.opt.location != '' && self.opt.location == location) {
				console.warn('AddressBlock:get_location', 'location is the same');
				return false;
			}
			if (is_update) {
				self.dom.$field.value = '';
				self.dom.$coordinates_field.value = '';
			}
			self.opt.location = location;
			if (!self.opt.location || self.opt.location.trim() == '') {
				self.disable();
				self.opt.location = null;
				console.warn('AddressBlock:get_location', 'location undefined');
				return false;
			} else {
				self.enable();
			}
			if (self.opt.map) self.opt.map.destroy();
			self.opt.selected = null;
			self.opt.options_data = null;
			self.opt.options = [];
			self.opt.interval = null;
			self.opt.map_pin = null;
			self.opt.temp_place = null;
			self.opt.map_id = App.h.get_uniqe_id('map');
			ymaps.ready(function(){
				//console.log('AddressBlock:get_location', 'map initialization');
				if (start_coordinates) {
					self.initiate_map(start_coordinates);
				}
				else {
					ymaps.geocode(self.opt.location, {results: 1}).then(function(response){
						self.initiate_map(response.geoObjects.get(0).geometry.getCoordinates());
					});
				}
			});
		};
		self.set_pin = function(coordinates) {
			self.opt.map_pin = new ymaps.Placemark(coordinates, {
				iconContent: 'Поиск...'
			}, {
				preset: 'islands#grayStretchyIcon',
				draggable: false
			});
			self.opt.map['geoObjects'].add(self.opt.map_pin);
		};
		self.set_pin_position = function(coordinates) {
			self.opt.map_pin['geometry'].setCoordinates(coordinates);
			self.opt.map_pin['properties'].set('iconContent', 'Поиск...');
		};
		self.set_pin_properties = function(properties) {
			self.opt.map_pin['properties'].set({
				iconContent: properties.get('name'),
				balloonContent: properties.get('text')
			});
		};
		self.update_location = function() {
			self.get_location(true);
		};
		self.initiate_map = function(coordinates) {
			if (self.dom.$btn && !self.dom.$modal) {
				console.error('AddressBlock: $modal не найден');
				return false;
			}
			if (self.dom.$btn && self.dom.$modal) {
				self.dom.$map = self.dom.$modal.querySelector('.address-modal__map');
				self.dom.$done_btn = self.dom.$modal.querySelector('.address-modal__done-btn');
				self.dom.$modal.addEventListener('is-hidden', self.on_map_hide, false);
				self.dom.$done_btn.addEventListener('click', self.on_done_click, false);
			}
			else {
				self.dom.$map = self.dom.$block.querySelector('.address-modal__map');
			}
			self.dom.$map.id = self.opt.map_id;
			self.opt.map = new ymaps.Map(self.opt.map_id, {
				center: coordinates,
				zoom: 11,
				controls: ['fullscreenControl', 'zoomControl', 'searchControl', 'typeSelector']
			}, {
				searchControlProvider: 'yandex#search'
			});
			self.opt.map['events'].add('click', function (e) {
				self.on_map_select(e);
			});
			if (self.dom.$coordinates_field.value.trim() != '') {
				var string_coordinates = self.dom.$coordinates_field.value.trim(),
					split_coordinates = App.h.get_coordinates(string_coordinates);

				if (split_coordinates != false) self.on_map_select({get:function(){return split_coordinates;}});
			}
			//console.log('AddressBlock:initiate_map', 'map initialized');
			App.h.trigger(self.dom.$block, 'map-initiated');
		};
		self.clear_selected = function() {
			self.opt.selected = null;
			self.dom.$coordinates_field.value = '';
		};
		self.clear_list = function() {
			App.h.i(self.opt.options, function(option_object){
				App.h.remove_node(option_object.$node);
				option_object = null;
			});
			self.opt.options = [];
			self.clear_selected();
		};
		self.render_options = function(input, address) {
			self.clear_list();
			if (input) self.opt.options_data = input;
			App.h.i(self.opt.options_data, function(option_data){
				var option = {};

				option.data = option_data['GeoObject'];
				option.title = option.data['name'] + ', ' + option.data['description'];
				option.coordinates = App.h.get_coordinates(option.data['Point']['pos']);
				option.raw_coordinates = [option.coordinates[1], option.coordinates[0]];
				option.string_coordinates = option.raw_coordinates.join(',');
				option.$title = option.title.replace(new RegExp(address, 'gi'), '<b>' + address + '</b>');
				option.$node = App.h.get_node('btn address-block__option-btn', '', 'button');
				option.$node.type = 'button';
				option.$node.tabIndex = '-1';
				option.$node_cap = App.h.get_node('btn__cap', option.$title, 'span');
				option.$node.appendChild(option.$node_cap);
				self.dom.$list.appendChild(option.$node);
				option.$node.onclick = function(e){
					self.on_select.call(option, e);
					if (!self.opt.map_pin) self.set_pin(option.raw_coordinates);
					else self.set_pin_position(option.raw_coordinates);
					self.set_pin_properties({
						get: function(key) {
							if (key == 'name') return option.data['name']; else
							if (key == 'text') return option.title;
						}
					});
				}
				self.opt.options.push(option);
			});
			self.show();
		};
		self.render = function() {
			self.opt.is_shown = false;
			self.opt.location = null;
			self.opt.immidiate_select = false;
			self.dom.$modal = self.dom.$block.querySelector('.address-block__modal');
			self.dom.$field = self.dom.$block.querySelector('.address-block__field');
			self.dom.$coordinates_field = self.dom.$block.querySelector('.address-block__coordinates');
			self.dom.$btn = self.dom.$block.querySelector('.address-block__btn');
			self.dom.$list = App.h.get_node('address-block__list', '', '');
			self.dom.$block.appendChild(self.dom.$list);
			//self.dom.$btn.addEventListener('click', self.on_btn_click, false);
			if (self.dom.$btn) self.dom.$btn.onclick = self.on_btn_click;
			self.dom.$field.addEventListener("input", self.on_input, false);
			self.dom.$field.addEventListener("keydown", self.on_key_down, false);
			App.d.d.addEventListener('click', self.on_outside_click, false);
			App.d.d.addEventListener("keydown", self.on_outside_key_down, false);
			self.get_location();
			self.dom.$block.js = self;
			App.h.trigger(self.dom.$block, 'is-ready');
		};
		self.initiate = function() {
			self.dom.$block = $block;
			if (!self.dom.$block) {
				self = null;
				return false;
			}
			self.render();
		};
		self.initiate();
		return self;
		/*self.set_value = function(option) {
		 var coord = option.object['Point']['pos'].split(' ');

		 self.dom.$container.setAttribute('data-coord', coord[1] + ',' + coord[0]);
		 self.dom.$container.setAttribute('data-value', option.object['name'] + ', ' + option.object['description']);
		 self.opt.address = {
		 coord: coord[1] + ',' + coord[0],
		 name: option.object['name'],
		 description: option.object['description']
		 };
		 self.on_map_select({
		 get: function() {
		 return [coord[1], coord[0]];
		 }
		 });
		 self.opt.map.setCenter([coord[1], coord[0]]);
		 };*/
	};
	/**
	 * Элемент с выпадающим списком действий
	 *
	 * @namespace App.c.tools
	 * @class ActionsBlock
	 * @returns {object}
	 */
	App.c.tools.ActionsBlock = function($block) {
		var self = this;

		self.opt = {};
		self.dom = {};
		self.enable = function() {
			self.dom.$btn.disabled = false;
			//self.dom.$block.removeAttribute('data-disabled');
			App.h.trigger(self.dom.$block, 'is-enabled');
		};
		self.disable = function() {
			self.opt.is_shown = false;
			self.dom.$block.removeAttribute('data-is-shown');
			self.dom.$btn.disabled = true;
			//self.dom.$block.setAttribute('data-disabled', '');
			App.h.trigger(self.dom.$block, 'is-disabled');
		};
		self.show = function() {
			self.opt.is_shown = true;
			self.dom.$block.setAttribute('data-is-shown', '');
			self.dom.$block.setAttribute('data-interacted', '');
			App.h.trigger(self.dom.$block, 'is-shown');
		};
		self.hide = function() {
			self.opt.is_shown = false;
			self.dom.$block.removeAttribute('data-is-shown');
			App.h.trigger(self.dom.$block, 'is-hidden');
		};
		self.toggle = function() {
			if (self.opt.is_shown) self.hide();
			else self.show();
		};
		self.on_key_down = function(e) {
			if (self.opt.is_shown && e.which == 27) {
				self.hide();
				self.dom.$btn.focus();
				e.stopPropagation();
				e.preventDefault();
			}
		};
		self.on_outside_click = function(e) {
			if (!self.opt.is_shown) return false;
			var is_target_block = (e.target == self.dom.$block),
				has_target_block = App.h.has_parent(e.target, self.dom.$block);

			if (!is_target_block && !has_target_block) self.hide();
		};
		self.render = function() {
			self.opt.is_shown = false;
			self.dom.$btn = self.dom.$block.querySelector('.actions-block__btn');
			self.dom.$btn.addEventListener('click', self.toggle, false);
			self.dom.$block.addEventListener("keydown", self.on_key_down, false);
			App.d.d.addEventListener('click', self.on_outside_click, false);
			App.d.d.addEventListener("keydown", self.on_key_down, false);
			if (self.dom.$btn.disabled) self.disable();
			self.dom.$block.js = self;
			App.h.trigger(self.dom.$block, 'is-ready');
		};
		self.initiate = function() {
			self.dom.$block = $block;
			if (!self.dom.$block) {
				self = null;
				return false;
			}
			self.render();
		};
		self.initiate();
		return self;
	};
	/**
	 * Элемент ввода и выбора значения из предложенных
	 *
	 * @namespace App.c.tools
	 * @class CompleteBlock
	 * @returns {object}
	 */
	App.c.tools.CompleteBlock = function($block) {
		var self = this;

		self.opt = {};
		self.dom = {};
		self.enable = function() {
			self.dom.$field.disabled = false;
			self.dom.$value_field.disabled = false;
			//self.dom.$block.removeAttribute('data-disabled');
			App.h.trigger(self.dom.$block, 'is-enabled');
		};
		self.disable = function() {
			self.opt.is_shown = false;
			self.dom.$block.removeAttribute('data-is-shown');
			self.dom.$field.disabled = true;
			self.dom.$value_field.disabled = true;
			//self.dom.$block.setAttribute('data-disabled', '');
			App.h.trigger(self.dom.$block, 'is-disabled');
		};
		self.show = function() {
			self.opt.is_shown = true;
			self.dom.$block.setAttribute('data-is-shown', '');
			App.h.trigger(self.dom.$block, 'is-shown');
		};
		self.hide = function() {
			self.opt.is_shown = false;
			self.dom.$block.removeAttribute('data-is-shown');
			App.h.trigger(self.dom.$block, 'is-hidden');
			self.on_blur();
		};
		self.on_blur = function() {
			if (!self.opt.selected) {
				self.dom.$value_field.value = '';
				self.dom.$field.value = '';
				App.h.trigger(self.dom.$block, 'is-cleared');
			}
		};
		self.on_key_down = function(e) {
			if (self.opt.is_shown) {
				if (e.which == 27) {
					self.hide();
					//self.clear_selected();
					self.dom.$field.focus();
					e.stopPropagation();
					e.preventDefault();
				} else
				/*if (e.which == 9) {
				 self.hide();
				 self.dom.$btn.focus();
				 e.stopPropagation();
				 e.preventDefault();
				 } else*/
				if (e.which == 38) {
					self.on_arrow_select('up');
					e.stopPropagation();
					e.preventDefault();
				} else
				if (e.which == 40) {
					self.on_arrow_select('down');
					e.stopPropagation();
					e.preventDefault();
				} else
				if (e.which == 13) {
					self.hide();
					App.h.trigger(self.dom.$block, 'is-entered');
					e.stopPropagation();
					e.preventDefault();
				}
			}
		};
		self.on_outside_click = function(e) {
			if (!self.opt.is_shown) return false;
			var is_target_block = (e.target == self.dom.$block),
				has_target_block = App.h.has_parent(e.target, self.dom.$block);

			if (!is_target_block && !has_target_block) {
				self.hide();
				//self.clear_selected();
			}
		};
		self.on_outside_key_down = function(e) {
			if (self.opt.is_shown) {
				if (e.which == 27) {
					self.hide();
					//self.clear_selected();
					self.dom.$field.focus();
					e.stopPropagation();
					e.preventDefault();
				}
			}
		};
		self.on_response = function(response) {
			//setTimeout(function(){
			self.dom.$block.removeAttribute('data-is-loading');
			var value = this;

			response = JSON.parse(response);
			if (response['result'] == 'success') self.render_options(response['response'], value);
			else self.hide();
			//}, 300);
		}
		self.on_input = function() {
			self.opt.selected = null;
			App.h.trigger(self.dom.$block, 'is-typing');
			var value = self.dom.$field.value.trim();

			clearTimeout(self.opt.interval);
			if (value == '') {
				App.h.trigger(self.dom.$block, 'is-cleared');
				self.clear_list();
				self.hide();
				return false;
			}
			self.opt.interval = setTimeout(function(){
				self.clear_list();
				self.dom.$block.setAttribute('data-is-loading', '');
				self.opt.url = App.h.replace_param(self.opt.url, self.opt.input_field_name, value);
				App.xhr.get(self.opt.url, null, self.on_response.bind(value));
			}, 300);
		};
		self.on_select = function(e) {
			var option = this;

			if (!option) return false;
			App.h.i(self.opt.options, function(option_object){
				option_object.$node.classList.remove('complete-block__option-btn_selected');
			});
			option.$node.classList.add('complete-block__option-btn_selected');
			self.opt.selected = option;
			self.dom.$value_field.value = option.value;
			self.dom.$field.value = option.title;
			if (e && e.type == 'click') {
				/*e.preventDefault();
				 e.stopPropagation();*/
				self.dom.$field.focus();
				self.hide();
				App.h.trigger(self.dom.$block, 'is-selected');
			}
			App.h.trigger(self.dom.$block, 'is-changed');
		};
		self.on_arrow_select = function(direction) {
			var index = self.opt.options.indexOf(self.opt.selected),
				new_index = index+((direction=='up')?-1:1);

			if (!self.opt.selected || index == -1 || !self.opt.options[new_index]) {
				index = (direction=='up')?self.opt.options.length-1:0;
				self.opt.selected = self.opt.options[index];
			} else {
				self.opt.selected = self.opt.options[new_index];
			}
			self.on_select.call(self.opt.selected);
		};
		self.clear_selected = function() {
			self.opt.selected = null;
			self.dom.$value_field.value = '';
		};
		self.clear_list = function() {
			self.dom.$block.removeAttribute('data-is-loading');
			App.h.i(self.opt.options, function(option_object){
				App.h.remove_node(option_object.$node);
				option_object = null;
			});
			self.opt.options = [];
			self.clear_selected();
		};
		self.render_options = function(input, value) {
			self.clear_list();
			if (input) self.opt.options_data = input;
			//console.info('CompleteBlock', self.opt.options_data);
			App.h.i(self.opt.options_data, function(option_data, option_index){
				if (option_index < self.opt.max_options) {
					var option = {};

					option.data = option_data;
					option.title = option.data['NAME'];
					option.value = option.data['ID'];
					option.$title = option.title.replace(new RegExp('^' + value, 'gi'), '<b>' + value + '</b>');
					option.$node = App.h.get_node('btn complete-block__option-btn', '', 'button');
					option.$node.type = 'button';
					option.$node.tabIndex = '-1';
					option.$node_cap = App.h.get_node('btn__cap', option.$title, 'span');
					option.$node.appendChild(option.$node_cap);
					self.dom.$list.appendChild(option.$node);
					option.$node.addEventListener('click', self.on_select.bind(option), false);
					self.opt.options.push(option);
				}
			});
			if (!(self.opt.options_data.length > 0)) self.hide();
			else self.show();
		};
		self.render = function() {
			self.opt.url = self.dom.$block.getAttribute('data-url') || false;
			self.opt.options = App.h.get_json(self.dom.$block.getAttribute('data-options'));
			self.opt.input_field_name = (self.opt.options && self.opt.options['input_field_name'])?self.opt.options['input_field_name']:'NAME';
			self.opt.options_data = [];
			self.opt.is_shown = false;
			self.opt.selected = null;
			self.opt.options = [];
			self.opt.interval = null;
			self.opt.max_options = 6;
			self.dom.$value_field = self.dom.$block.querySelector('.complete-block__value-field');
			self.dom.$field = self.dom.$block.querySelector('.complete-block__field');
			self.dom.$list = App.h.get_node('complete-block__list', '', '');
			self.dom.$block.appendChild(self.dom.$list);
			self.dom.$field.addEventListener("keydown", self.on_key_down, false);
			//self.dom.$block.addEventListener("blur", self.on_blur, false);
			App.d.d.addEventListener('click', self.on_outside_click, false);
			App.d.d.addEventListener("keydown", self.on_outside_key_down, false);
			if (!self.opt.url) {
				self.disable();
			} else
			if (self.opt.url == 'javascript:void(0);') {

			} else {
				self.dom.$field.addEventListener("input", self.on_input, false);
			}
			self.dom.$block.js = self;
			App.h.trigger(self.dom.$block, 'is-ready');
		};
		self.initiate = function() {
			self.dom.$block = $block;
			if (!self.dom.$block) {
				self = null;
				return false;
			}
			self.render();
		};
		self.initiate();
		return self;
	};
	/**
	 * Элемент кнопки для сравнения компаний
	 *
	 * @namespace App.c.tools
	 * @class CompanyCompareBtn
	 * @returns {object}
	 */
	App.c.tools.CompanyCompareBtn = function($block) {
		var self = this;

		self.opt = {};
		self.dom = {};
		self.disable = function() {
			self.dom.$block.disabled = true;
			App.h.load_node(self.dom.$block, true);
		};
		self.add = function() {
			self.opt.is_added = true;
			self.dom.$block.setAttribute('data-is-added', '');
			App.h.trigger(self.dom.$block, 'is-added');
		};
		self.remove = function() {
			self.opt.is_added = false;
			self.dom.$block.removeAttribute('data-is-added');
			App.h.trigger(self.dom.$block, 'is-removed');
		};
		self.toggle = function() {
			if (!self.opt.url) {
				console.error('CompanyCompareBtn: кто-то забыл указать url');
				self.disable();
				return false;
			}
			App.h.load_node(self.dom.$block);
			var form_data = new FormData();

			App.h.i(self.opt.params, function(value, param){
				form_data.append(param, value);
			});
			App.xhr.send(self.opt.url, function(string){
				var response = App.h.get_json(string);

				if (!response) {
					console.error('CompanyCompareBtn: сервер ответил невалидной json строкой');
					self.disable();
					return false;
				}
				if (response['result'] == 'success') {
					App.h.load_node(self.dom.$block, true);
					if (self.opt.is_added) self.remove();
					else self.add();
				} else {
					console.error('CompanyCompareBtn: сервер ответил ошибкой');
					self.disable();
				}
			}, 'POST', form_data);
		};
		self.render = function() {
			self.opt.is_added = !!self.dom.$block.hasAttribute('data-is-added');
			self.opt.url = self.dom.$block.getAttribute('data-url');
			self.opt.params = App.h.get_json(self.dom.$block.getAttribute('data-params'));
			self.dom.$block.onclick = self.toggle;
			self.dom.$block.js = self;
		};
		self.initiate = function() {
			self.dom.$block = $block;
			if (!self.dom.$block) {
				self = null;
				return false;
			}
			self.render();
		};
		self.initiate();
		return self;
	};
	/**
	 * Элемент табов
	 *
	 * @namespace App.c.tools
	 * @class Tabs
	 * @returns {object}
	 */
	App.c.tools.Tabs = function($block) {
		var self = this;

		self.opt = {};
		self.dom = {};
		self.reset_links = function() {
			App.h.i(self.dom.$links, function($node){
				$node.classList.remove('tabs__link_active');
			});
		};
		self.reset_tabs = function() {
			App.h.i(self.dom.$tabs, function($node){
				$node.classList.remove('tabs__tab_is-shown');
			});
		};
		self.on_click = function() {
			var $link = this;

			self.reset_links();
			self.reset_tabs();
			self.opt.active_tab_id = $link.getAttribute('data-tab');
			self.dom.$active_tab = self.dom.$block.querySelector('[data-tab-id=' + self.opt.active_tab_id + ']');
			App.h.i(self.dom.$block.querySelectorAll('[data-tab=' + self.opt.active_tab_id + ']'), function($node){
				$node.classList.add('tabs__link_active');
			});
			if (!self.dom.$active_tab) {
				console.error('Таб с таким data-tab-id не найден');
				return false;
			}
			self.dom.$active_tab.classList.add('tabs__tab_is-shown');
		};
		self.show_all = function() {
			App.h.i(self.dom.$tabs, function($node){
				$node.classList.add('tabs__tab_is-shown');
			});
			self.dom.$toggle_btn.style.display = 'none';
		};
		self.render = function() {
			self.opt.active_tab_id = App.d.w.location.hash.replace(/[\s#]/, '') || 'unkown-fucking-tab-selector';
			self.dom.$links = self.dom.$block.querySelectorAll('.tabs__link');
			self.dom.$tabs = self.dom.$block.querySelectorAll('.tabs__tab');
			self.dom.$toggle_btn = self.dom.$block.querySelector('.tabs__toggle-btn');
			self.dom.$active_link = self.dom.$block.querySelector('[data-tab=' + self.opt.active_tab_id + ']');
			if (self.opt.active_tab_id && self.dom.$active_link) {
				App.h.remove_location_hash();
				self.reset_links();
				self.dom.$active_link.classList.add('tabs__link_active');
			} else {
				self.dom.$active_link = self.dom.$block.querySelector('.tabs__link_active');
				self.opt.active_tab_id = self.dom.$active_link.getAttribute('data-tab');
			}
			if (!self.dom.$active_link) {
				self.dom.$active_link = self.dom.$links[0];
				if (!self.dom.$active_link) {
					console.error('Tabs', 'куда ты дел табы, Карл??');
					return false;
				}
				self.dom.$active_link.classList.add('tabs__link_active');
				self.opt.active_tab_id = self.dom.$active_link.getAttribute('data-tab');
			}
			self.dom.$active_tab = self.dom.$block.querySelector('[data-tab-id=' + self.opt.active_tab_id + ']');
			self.reset_tabs();
			self.dom.$active_tab.classList.add('tabs__tab_is-shown');
			App.h.i(self.dom.$links, function($node){
				$node.addEventListener('click', self.on_click, false);
			});
			if (self.dom.$toggle_btn) self.dom.$toggle_btn.addEventListener('click', self.show_all, false);
		};
		self.initiate = function() {
			self.dom.$block = $block;
			if (!self.dom.$block) {
				self = null;
				return false;
			}
			self.render();
			self.dom.$block.js = self;
		};
		self.initiate();
		return self;
	};
	/**
	 * Элемент для работы с модальными окнами
	 *
	 * @namespace App.c.tools
	 * @class ModalContainer
	 * @returns {object}
	 */
	App.c.tools.ModalContainer = function() {
		var self = this;

		self.opt = {};
		self.dom = {};
		self.append_modal = function() {
			var $modal = this;

			self.dom.$cell.appendChild($modal);
			$modal.is_moved = true;
			App.h.i($modal.querySelectorAll('.modal__close-action'), function($node){
				$node.addEventListener('click', self.hide, false);
			});
		};
		self.read_options = function() {
			var $modal = this;

			if ($modal.options.background_color) self.dom.$back.style.backgroundColor = $modal.options.background_color;
			else self.dom.$back.removeAttribute('style');
			if ($modal.options.close_on_outer_click == false) self.opt.close_on_outer_click = false;
			else self.opt.close_on_outer_click = true;
		};
		self.freeze_page = function() {
			self.opt.scroll_amount = App.d.w.pageYOffset;
			self.dom.$page.style.position = 'fixed';
			self.dom.$page.style.left = '0px';
			self.dom.$page.style.top = -1 * self.opt.scroll_amount + 'px';
			self.dom.$container.style.display = 'block';
			self.dom.$page.style.width = '100%';
			self.dom.$page.style.height = (self.opt.scroll_amount + self.opt.viewport_height) + 'px';
			self.dom.$page.style.overflow = 'hidden';
			self.dom.$page.style.zIndex = 1;
			//if (!no_calculating && !self.opt.reopen) {}
		};
		self.hide_modals = function() {
			App.h.i(self.dom.$cell.children, function($node){
				$node.style.display = 'none';
			});
		};
		self.show = function($modal) {
			if (self.opt.is_blocked) return false;
			if (!$modal || !($modal instanceof HTMLElement)) {
				console.error('Modal: $modal не передан или не является node элементом');
				return false;
			}
			self.opt.is_blocked = true;
			if (!$modal.is_moved) self.append_modal.call($modal);
			if ($modal.options) self.read_options.call($modal);
			else self.read_options.call({options: {}});
			if (self.opt.is_shown && self.opt.$active_modal) {
				self.opt.modals_pull.push(self.opt.$active_modal);
				self.hide_modals();
			} else {
				self.freeze_page();
			}
			$modal.style.display = 'block';
			self.opt.$active_modal = $modal;
			App.d.w.scrollTo(0, 0);
			setTimeout(function () {
				self.dom.$container.setAttribute('data-is-shown', '');
				self.opt.is_shown = true;
				App.h.trigger($modal, 'is-shown');
				self.opt.is_blocked = false;
			}, 50);
		};
		self.hide = function() {
			if (self.opt.is_blocked) return false;
			if (self.opt.modals_pull.length > 0) {
				var last_index = self.opt.modals_pull.length-1,
					$modal = self.opt.modals_pull[last_index];

				self.hide_modals();
				App.h.trigger(self.opt.$active_modal, 'is-hidden');
				self.opt.$active_modal = null;
				self.opt.is_blocked = false;
				App.h.remove_from_array(self.opt.modals_pull, $modal);
				self.show($modal);
				return false;
			}
			self.opt.is_blocked = true;
			self.dom.$container.removeAttribute('data-is-shown');
			setTimeout(function(){
				self.hide_modals();
				self.dom.$container.style.display = 'none';
				self.dom.$page.removeAttribute('style');
				App.d.w.scrollTo(0, self.opt.scroll_amount);
				self.opt.is_shown = false;
				App.h.trigger(self.opt.$active_modal, 'is-hidden');
				self.opt.$active_modal = null;
				self.opt.is_blocked = false;
			}, 200);
		};
		self.on_out_click_start = function(e) {
			self.opt.click_out = !!(self.opt.is_shown && (e.target == self.dom.$wrp || e.target == self.dom.$cell));
		};
		self.on_out_click_end = function(e) {
			//console.debug(self.opt.close_on_outer_click, self.opt.click_out, self.opt.is_shown, e.target);
			if (e.which === 3 || e.button === 2) {
				self.opt.click_out = false;
			}
			if (self.opt.close_on_outer_click &&
				self.opt.click_out &&
				self.opt.is_shown &&
				(e.target == self.dom.$wrp || e.target == self.dom.$cell)
			) self.hide();
			self.opt.click_out = false;
		};
		self.on_key_down = function(e) {
			if (self.opt.is_shown && e.which == 27) self.hide();
		};
		self.on_resize = function() {
			self.opt.viewport_height = App.d.w.innerHeight;
			if (self.opt.is_shown) self.dom.$container.style.minHeight = self.opt.viewport_height + 'px';
		};
		self.render = function() {
			self.dom.$page = App.d.d.getElementById('page');
			self.dom.$container = App.h.get_node('modal-container');
			self.dom.$back = App.h.get_node('modal-container__back');
			self.dom.$wrp = App.h.get_node('modal-container__wrp');
			self.dom.$cell = App.h.get_node('modal-container__cell');
			self.dom.$wrp.appendChild(self.dom.$cell);
			self.dom.$container.appendChild(self.dom.$back);
			self.dom.$container.appendChild(self.dom.$wrp);
			self.dom.$container.style.display = 'none';
			App.d.b.appendChild(self.dom.$container);
			if (isMobile.any) {
				App.d.b.addEventListener("touchstart", self.on_out_click_start, false);
				App.d.b.addEventListener("touchend", self.on_out_click_end, false);
				App.d.b.addEventListener("touchcancel", self.on_out_click_end, false);
			} else {
				App.d.b.addEventListener("mousedown", self.on_out_click_start, false);
				App.d.b.addEventListener("mouseup", self.on_out_click_end, false);
				App.d.d.addEventListener("keydown", self.on_key_down, false);
			}
			App.d.w.addEventListener('resize', self.on_resize, false);
		};
		self.initiate = function() {
			self.render();
			self.opt.is_shown = false;
			self.opt.click_out = false;
			self.opt.close_on_outer_click = true;
			self.opt.active_modal = null;
			self.opt.viewport_height = App.d.w.innerHeight;
			self.opt.scroll_amount = null;
			self.opt.is_blocked = false;
			self.opt.modals_pull = [];
		};
		self.initiate();
		return self;
	};
	/**
	 * Рейтинг
	 * Состоит из 5 звёзд. Баллы от 0 до 10. Заполненность звёзд зависит от кол-ва баллов
	 *
	 * @namespace App.c.tools
	 * @class Rating
	 * @returns {object}
	 */
	App.c.tools.Rating = function($block) {
		var self = this;

		self.opt = {};
		self.dom = {};
		/**
		 * Возвращает SVG тег с указанным классом
		 *
		 * @namespace App.c.tools.Rating
		 * @class get_svg_node
		 *
		 * @param {string} tag — Тег
		 * @param {string} class_name — Класс
		 * @returns {object}
		 */
		self.get_svg_node = function(tag, class_name) {
			class_name = class_name || null;
			tag = tag || 'svg';
			var $node = App.d.d.createElementNS('http://www.w3.org/2000/svg', tag);

			if (class_name) $node.classList.add(class_name);
			return $node;
		};
		/**
		 * Возвращает объект одной звезды, содержащий SVG узлы
		 *
		 * @namespace App.c.tools.Rating
		 * @class get_star
		 *
		 * @param {number} value — Заполненность звезды
		 * @param {number} index — Индекс звезды
		 * @returns {object}
		 */
		self.prepare_star = function(value) {
			var star = this;

			star.id = 'star-gradient-' + new Date().getTime() + '-' + App.h.get_random_int(10,100) + star.index;
			star.$svg = star.$icon.querySelector('svg');
			star.$polygon = star.$svg.querySelector('polygon');
			star.$polygon.setAttribute('fill', 'url(#' + star.id + ')');
			star.$polygon.setAttribute('style', self.opt.$stroke);
			star.$gradient = self.get_svg_node('linearGradient');
			star.$gradient.setAttribute('id', star.id);
			star.$defs = self.get_svg_node('defs');
			if (star.star_percentage == 0) {
				star.$stop_1 = self.get_svg_node('stop');
				star.$stop_1.setAttribute('offset', '0%');
				star.$stop_1.setAttribute('style', 'stop-color:' + self.opt.bg_color + ';');
				star.$gradient.appendChild(star.$stop_1);
				star.$stop_2 = self.get_svg_node('stop');
				star.$stop_2.setAttribute('offset', '100%');
				star.$stop_2.setAttribute('style', 'stop-color:' + self.opt.bg_color + ';');
				star.$gradient.appendChild(star.$stop_2);
			} else
			if (star.star_percentage == 100) {
				star.$stop_1 = self.get_svg_node('stop');
				star.$stop_1.setAttribute('offset', '0%');
				star.$stop_1.setAttribute('style', 'stop-color:' + self.opt.fg_color + ';');
				star.$gradient.appendChild(star.$stop_1);
				star.$stop_2 = self.get_svg_node('stop');
				star.$stop_2.setAttribute('offset', '100%');
				star.$stop_2.setAttribute('style', 'stop-color:' + self.opt.fg_color + ';');
				star.$gradient.appendChild(star.$stop_2);
			}
			else {
				star.$stop_1 = self.get_svg_node('stop');
				star.$stop_1.setAttribute('offset', '0%');
				star.$stop_1.setAttribute('style', 'stop-color:' + self.opt.fg_color + ';');
				star.$gradient.appendChild(star.$stop_1);
				star.$stop_2 = self.get_svg_node('stop');
				star.$stop_2.setAttribute('offset', star.star_percentage + '%');
				star.$stop_2.setAttribute('style', 'stop-color:' + self.opt.fg_color + ';');
				star.$gradient.appendChild(star.$stop_2);
				star.$stop_3 = self.get_svg_node('stop');
				star.$stop_3.setAttribute('offset',  star.star_percentage + '%');
				star.$stop_3.setAttribute('style', 'stop-color:' + self.opt.bg_color + ';');
				star.$gradient.appendChild(star.$stop_3);
				star.$stop_4 = self.get_svg_node('stop');
				star.$stop_4.setAttribute('offset', '100%');
				star.$stop_4.setAttribute('style', 'stop-color:' + self.opt.bg_color + ';');
				star.$gradient.appendChild(star.$stop_4);
			}
			star.$defs.appendChild(star.$gradient);
			star.$svg.appendChild(star.$defs);
		};
		/**
		 * Очищает список звёзд
		 *
		 * @namespace App.c.tools.Rating
		 * @class clear
		 */
		self.clear = function() {
			self.opt.list = [];
		};
		/**
		 * Создание и добавление звёзд в DOM древо
		 *
		 * @namespace App.c.tools.Rating
		 * @class render_list
		 */
		self.render_list = function() {
			self.clear();
			if (self.dom.$block.hasAttribute('data-monochrome')) {
				self.opt.bg_color = 'rgba(0,0,0,0)';
				self.opt.fg_color = '#333';
				self.opt.$stroke = 'stroke:#333;stroke-width:-1;'
			} else {
				self.opt.bg_color = '#b0bec5';
				self.opt.fg_color = '#ffc107';
				self.opt.$stroke = '';
			}
			self.opt.stars_amount = self.dom.$list.children.length;
			self.opt.max_rating = 10;
			self.opt.rating_percentage = 100/self.opt.max_rating*self.opt.rating_value;
			self.opt.max_value_per_star = Math.round(100/self.opt.stars_amount);
			App.h.i(self.dom.$list.children, function($star, star_i){
				var star = {};

				star.$icon = $star;
				star.index = star_i;
				if (self.opt.max_value_per_star <= self.opt.rating_percentage) {
					star.star_value = self.opt.max_value_per_star;
					self.opt.rating_percentage -= self.opt.max_value_per_star;
				} else {
					star.star_value = self.opt.rating_percentage;
					self.opt.rating_percentage = 0;
				}
				star.star_percentage = 100/self.opt.max_value_per_star*star.star_value;
				self.prepare_star.call(star);
				self.opt.list.push(star);
			});
		};
		self.render = function() {
			if (isNaN(self.opt.rating_value)) return false;
			self.dom.$list = self.dom.$block.querySelector('.rating__stars');
			self.render_list();
		};
		self.initiate = function() {
			self.dom.$block = $block;
			if (!self.dom.$block) {
				self = null;
				return false;
			}
			self.opt.list = [];
			self.opt.rating_value = parseFloat(self.dom.$block.getAttribute('data-rating'));
			self.render();
			self.dom.$block.js = self;
		};
		self.initiate();
		return self;
	};
	/**
	 * Экземпляр класса, отвечающий за модальное окно
	 *
	 * @namespace App
	 * @type {object} window
	 */
	App.modal = new App.c.tools.ModalContainer();
	/**
	 * Экземпляр класса, отвечающий за асинхронные запросы
	 *
	 * @namespace App
	 * @type {object} xhr
	 */
	App.xhr = new App.c.tools.XHR();
	/**
	 * Набор функций, выполняющийся при загрузке каждой страницы
	 *
	 * @namespace App.c.blocks
	 * @class PageStart
	 * @returns {object}
	 */
	App.c.blocks.PageStart = function() {
		var self = this;

		self.opt = {};
		self.dom = {};
		/**
		 * Отменяет переадресацию по ссылке, если она отключена
		 *
		 * @namespace App.c.blocks.Preparing
		 * @class fix_links
		 *
		 * @returns {boolean}
		 */
		self.fix_links = function() {
			App.d.b.addEventListener('click', function(e){
				if (e.target.tagName == 'A' && e.target.hasAttribute('data-disabled')) {
					e.stopPropagation();
					e.preventDefault();
					return false;
				}
			}, false);
		};
		self.initiate = function() {
			try {
				self.fix_links();
			} catch(e) {
				console.error('PageStart: ' + e.message + ' / ', e);
			}
			try {
				App.h.i(App.d.d.querySelectorAll('.select-block'), function($block){
					new App.c.tools.SelectBlock($block);
				});
			} catch(e) {
				console.error('PageStart: ' + e.message + ' / ', e);
			}
			try {
				App.h.i(App.d.d.querySelectorAll('.calendar-block'), function($block){
					new App.c.tools.CalendarBlock($block);
				});
			} catch(e) {
				console.error('PageStart: ' + e.message + ' / ', e);
			}
		};
		self.initiate();
		return {self: self};
	};
	/* Инициализация. Получение экземпляров блоков. Блоки обособлены друг от друга */
	/* Несинтаксические ошибки будут глушиться и выводиться в консоль, не перекрывая работу других блоков */
	/* Блок с общими для всех страниц субинициализациями - App.c.blocks.Preparing */
	/* Инициализируются все блоки. Те, которые не удовлетворяют условиям будут равны null */
	for (var class_index in App.c.blocks) {
		if (App.c.blocks.hasOwnProperty(class_index)) {
			try {
				var instance = new App.c.blocks[class_index]();

				if (instance.self) {
					//App.i[class_index] = instance;
					console.info('class "' + class_index + '" is initiated');
				}
			} catch(e) {
				console.error('error in class "' + class_index + '" ', + e.message, e);
			}
		}
	}
	console.info('initiations end');
};

/*App.c.blocks. = function($block) {
 var self = this;

 self.opt = {};
 self.dom = {};
 self.initiate = function() {
 self.dom.$block = $block;
 if (!self.dom.$block) {
 self = null;
 return false;
 }
 self.dom.$block.js = self;
 };
 self.initiate();
 return {self: self};
 };*/






