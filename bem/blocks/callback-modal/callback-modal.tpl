<div class="modal modal_size_middle form-modal" id="callback-modal">
	<button class="btn modal__close-btn modal__close-action" type="button">
		<i class="btn__icon">
			<svg class="btn__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
				<g fill="none" stroke="inherit" stroke-width="inherit">
					<path d="M0,0 L16,16 M0,16 L16,0"></path>
				</g>
			</svg>
		</i>
	</button>
	<div class="modal__content form-modal__content">
		<div class="form-modal__wrp">
			<div class="form-modal__information-side">
				<div class="form-modal__information-wrp">
					<i class="form-modal__icon">
						<svg class="form-modal__svg" width="82" height="82" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
							<g fill="inherit" transform="translate(2.6 2.3) scale(0.7)">
								<path d="M3.45.93C2.79,1.4.9,4,4.24,10.06c3.51,6.38,7,6.18,7.67,5.76L9.53,11.64c-.58.33-1.06-.24-1.94-1.47a17.2,17.2,0,0,1-1.7-3c-.35-.79-.62-1.6,0-1.95Z"></path>
								<path d="M13.41,15a.53.53,0,0,0,.2-.73h0L11.76,11a.53.53,0,0,0-.73-.2l-.93.53,2.38,4.19.93-.52Z"></path>
								<path d="M7.39,4.36a.53.53,0,0,0,.2-.73h0L5.69.27A.53.53,0,0,0,5,.07L4,.6,6.46,4.88l.93-.52Z"></path>
							</g>
						</svg>
					</i>
					<p class="form-modal__title">Заявка на обратный звонок</p>
					<p class="form-modal__text"></p>
				</div>
			</div>
			<div class="form-modal__separator"></div>
			<div class="form-modal__form-side">
				<form class="form-modal__form" action="javascript:void(0);" method="post">
					<div class="field-block form-modal__item">
						<p class="field-block__cap">Имя</p>
						<div class="field-block__grp">
							<input class="field" type="text" name="PROPERTY_CLIENT_NAME" placeholder="Как вас зовут?" />
						</div>
					</div>
					<div class="field-block form-modal__item">
						<p class="field-block__cap">Номер телефона</p>
						<div class="field-block__grp">
							<div class="field-grp field-grp_phone">
								<input class="field field-grp__field" name="PROPERTY_CLIENT_PHONE" type="tel" maxlength="20" placeholder="" />
								<p class="field-grp__prefix">+7</p>
							</div>
						</div>
					</div>
					<div class="form-modal__submit-wrp">
						<button class="btn btn_color_primary btn_size_lg form-modal__submit-btn" type="button">
							<span class="btn__cap">Перезвоните мне</span>
						</button>
					</div>
					#form-messages#
				</form>
			</div>
		</div>
		<div class="form-modal__success">
			<div class="form-modal__success-wrp">
				<i class="form-modal__success-icon form-modal__success-icon">
					<svg class="form-modal__success-icon-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.79 11.5">
						<g fill="inherit">
							<polygon points="16.79 1 15.79 0 6.21 9.59 0.91 4.29 0 5.21 6.29 11.5 6.29 11.5 6.29 11.5 16.79 1"></polygon>
						</g>
					</svg>
				</i>
				<p class="form-modal__success-title">Благодарим за вашу заявку</p>
				<p class="form-modal__success-text">Услуга оказывается в рабочее время. Если вы оставляете заявку в нерабочее время, сотрудники компании<span class="form-modal__company-name" hidden></span> свяжутся с вами на следующий день</p>
			</div>
		</div>
	</div>
</div>