/**
 * Элемент ввода пароля
 *
 * @namespace App.c.tools
 * @class PasswordBlock
 * @returns {object}
 */
App.c.blocks.PasswordBlock = function() {
	var self = this;

	self.opt = {};
	self.dom = {};
	self.prepare = function($node) {
		var o = {};
		
		o.opt = {};
		o.dom = {};
		o.dom.$block = $node;
		o.opt.is_shown = !!o.dom.$block.hasAttribute('data-is-shown');
		o.dom.$field = o.dom.$block.querySelector('.password-block__field');
		o.dom.$btn = o.dom.$block.querySelector('.password-block__btn');
		o.show = function() {
			o.opt.is_shown = true;
			o.dom.$block.setAttribute('data-is-shown', '');
			o.dom.$field.type = 'text';
			App.h.trigger(o.dom.$block, 'is-shown');
		};
		o.hide = function() {
			o.opt.is_shown = false;
			o.dom.$block.removeAttribute('data-is-shown');
			o.dom.$field.type = 'password';
			App.h.trigger(o.dom.$block, 'is-hidden');
		};
		o.toggle = function() {
			if (o.opt.is_shown) o.hide();
			else o.show();
		};
		o.dom.$btn.addEventListener('click', o.toggle, false);
		o.dom.$block.js = o;
	};
	self.render = function() {
		App.h.i(self.dom.$blocks, function($node){
			self.prepare($node);
		});
	};
	self.initiate = function() {
		self.dom.$blocks = App.d.d.querySelectorAll('.password-block');
		if (!self.dom.$blocks && self.dom.$blocks.length !== 0) {
			self = null;
			return false;
		}
		self.render();
	};
	self.initiate();
	return {self: self};
};