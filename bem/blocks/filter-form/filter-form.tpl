<form class="filter-form" action="" method="post">
	<div class="filter-form__wrp">
		<div class="filter-form__primary">
			<p class="filter-form__title" data-sim="if(NOT $data.distance ? ignore()); content($data.distance.title);">Показать в пределах (км):</p>
			#distance-block#
			<p class="filter-form__title" data-sim="content($data.ratings.title)">Рейтинг завода:</p>
			<div class="radio-block" data-sim="foreach($data.ratings.fields as $radio)">
				<label data-sim="ignore()" class="radio-block__wrp">
					<input class="radio-block__field" type="radio" name="RADIOS" value="" />
					<i class="radio-block__icon">
						<span class="radio-block__dot"></span>
					</i>
					<span class="radio-block__cap">Любой</span>
				</label>
				<label class="radio-block__wrp">
					<input class="radio-block__field" type="radio" name="" value="" data-sim="attributes(name,$radio.item.name); attributes(value,$radio.item.value); if($radio.item.checked ? attributes(checked,''));" />
					<i class="radio-block__icon">
						<span class="radio-block__dot"></span>
					</i>
					<span data-sim="if(NOT $radio.item.stars ? content($radio.item.title)); if(NOT $radio.item.stars ? class('radio-block__cap'));"></span>
					<span class="radio-block__cap filter-form__rating-radio-cap" data-sim="if(NOT $radio.item.stars ? ignore()); foreach($radio.item.stars as $star);">
						<span data-sim="if($star.key GT 0 ? ignore())">От</span>
						<i class="filter-form__rating-radio-star">
							<svg class="filter-form__rating-radio-svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
						<i class="filter-form__rating-radio-star" data-sim="ignore()">
							<svg class="filter-form__rating-radio-svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
						<i class="filter-form__rating-radio-star" data-sim="ignore()">
							<svg class="filter-form__rating-radio-svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
						<i class="filter-form__rating-radio-star" data-sim="ignore()">
							<svg class="filter-form__rating-radio-svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
					</span>
				</label>
				<label data-sim="ignore()" class="radio-block__wrp">
					<input class="radio-block__field" type="radio" name="RADIOS" checked value="" />
					<i class="radio-block__icon">
						<span class="radio-block__dot"></span>
					</i>
					<span class="radio-block__cap filter-form__rating-radio-cap">
						<span>От</span>
						<i class="filter-form__rating-radio-star">
							<svg class="rating__svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
						<i class="filter-form__rating-radio-star">
							<svg class="rating__svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
						<i class="filter-form__rating-radio-star">
							<svg class="rating__svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
					</span>
				</label>
				<label data-sim="ignore()" class="radio-block__wrp">
					<input class="radio-block__field" type="radio" name="RADIOS" value="" />
					<i class="radio-block__icon">
						<span class="radio-block__dot"></span>
					</i>
					<span class="radio-block__cap filter-form__rating-radio-cap">
						<span>От</span>
						<i class="filter-form__rating-radio-star">
							<svg class="rating__svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
						<i class="filter-form__rating-radio-star">
							<svg class="rating__svg" viewBox="0 0 11.8 11.2" xmlns="http://www.w3.org/2000/svg"><polygon fill="#ffc107" points="11.8 4.3 7.4 4.1 5.9 0 4.4 4.1 0 4.3 3.5 7 2.3 11.2 5.9 8.8 9.6 11.2 8.4 7 11.8 4.3" style=""></polygon></svg>
						</i>
					</span>
				</label>
			</div>
			<p class="filter-form__title" data-sim="content($data.fabrication.title)">Производство:</p>
			<div class="checkbox-block" data-sim="foreach($data.fabrication.fields as $check)">
				<label class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="" value="" data-sim="attributes(name,$check.item.name); attributes(value,$check.item.value); if($check.item.checked ? attributes(checked,''));" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap" data-sim="content($check.item.title)">Наличие автопарка</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Аттестованная лаборатория</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Паспорт качества</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Проверен в Бетон24</span>
				</label>
			</div>
		</div>
		<div class="filter-form__secondary">
			<p class="filter-form__title" data-sim="content($data.payment.title)">Оплата и доставка:</p>
			<div class="checkbox-block" data-sim="foreach($data.payment.fields as $check)">
				<label class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="" value="" data-sim="attributes(name,$check.item.name); attributes(value,$check.item.value); if($check.item.checked ? attributes(checked,''));" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap" data-sim="content($check.item.title)">Рассрочка</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Оплата на месте</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Оплата картой</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Работает с НДС</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Предоставляет отчетность</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Предоставляет накладные</span>
				</label>
			</div>
			<p class="filter-form__title" data-sim="content($data.services.title)">Услуги:</p>
			<div class="checkbox-block" data-sim="foreach($data.services.fields as $check)">
				<label class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="" value="" data-sim="attributes(name,$check.item.name); attributes(value,$check.item.value); if($check.item.checked ? attributes(checked,''));" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap" data-sim="content($check.item.title)">Прокачка</span>
				</label>
				<label data-sim="ignore()" class="checkbox-block__wrp">
					<input class="checkbox-block__field" type="checkbox" name="NAME" value="" />
					<i class="checkbox-block__icon">
						<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg"><g fill="inherit"><polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon></g></svg>
					</i>
					<span class="checkbox-block__cap">Выезд специалиста</span>
				</label>
			</div>
		</div>
	</div>
	<button class="btn btn_fluid btn_color_primary filter-form__submit-btn" type="submit" data-sim="attributes(name,$data.actions.filter.name); attributes(value,$data.actions.filter.value);">
		<span class="btn__cap">Отфильтровать результаты</span>
	</button>
	<button class="btn btn_fluid filter-form__reset-btn" type="submit" data-sim="attributes(name,$data.actions.clear.name); attributes(value,$data.actions.clear.value);">
		<span class="btn__cap">Сбросить фильтр</span>
	</button>
</form>