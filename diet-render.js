const m = {};
m.fs = require('fs');
m.ect = require('ect');
m.path = require('path');
m.merge = require('merge');
m.clone = require('clone');
module.exports = function(options){
	options = options || {};
	var ECT = m.ect(
		m.merge(
			{
				root : options.path,
				ext: '.html',
				open: '{{',
				close: '}}',
				cache: true,
				watch: true,
				gzip: true
			},
			options
		)
	);

	return function($) {
		$.htmlModule = function(path) {
			path = path || 'index.html';
			if (['\\','/'].indexOf(path[0]) !== -1) path = path.substr(1);
			if (m.fs.existsSync(m.path.join(ECT.options.path, path))) {
				$.response.end(ECT.render(path, m.merge(m.clone($, false, 1), $.data)));
			}
			else {
				console.warn('diet-render.js', '"path" is a string');
				$.response.end(path);
			}
			$.nextRoute();
		};
		$.return();
	}
};