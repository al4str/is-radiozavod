const m = {};
m.crypto = require('crypto');
m.fs = require('fs');
m.path = require('path');

const g = {};
g.db_name = "JUPITER";
g.tables = {};

g.tables.CLIENTS = require('./json/CLIENTS.json') || {};
g.tables.DEPARTMENTS = require('./json/DEPARTMENTS.json') || {};
g.tables.WORKERS = require('./json/WORKERS.json') || {};
g.tables.SUPERVISORS = require('./json/SUPERVISORS.json') || {};
g.tables.PRODUCTION = require('./json/PRODUCTION.json') || {};
g.tables.ORDERS = require('./json/ORDERS.json') || {};
g.tables.PURCHASED_ITEMS = require('./json/PURCHASED_ITEMS.json') || {};
g.tables.SCHEDULE = require('./json/SCHEDULE.json') || {};
g.tables.APPOINTMENTS = require('./json/APPOINTMENTS.json') || {};
g.tables.WORKER_STATS_ITEMS = require('./json/WORKER_STATS_ITEMS.json') || {};
g.tables.WORKER_STATS_ORDERS = require('./json/WORKER_STATS_ORDERS.json') || {};
g.tables.WORKER_SALARY = require('./json/WORKER_SALARY.json') || {};
g.tables.SUPERVISOR_SALARY = require('./json/SUPERVISOR_SALARY.json') || {};

String.prototype.save = function(file_path) {
	if (!m.path.parse(file_path).ext) {
		console.warn('String.save:', 'file_path is not an actual path');
		return;
	}
	m.fs.writeFile(
		file_path,
		this,
		'utf8',
		(errors) => {
			if (errors) console.error('String.save:', errors);
		}
	);
};
String.prototype.sql_date = function() {
	let day, month, year, split = this.split('-');

	if (split.length !== 3) {
		console.warn('String.sql_date:', 'not a sql date string');
		return false;
	}
	day = parseInt(split[0]);
	month = Date.prototype.sql_months.indexOf(split[1]);
	month += 1;
	year = parseInt(split[2]);
	if (isNaN(day) || month === -1 || isNaN(year)) {
		console.warn('String.sql_date:', 'not a valid sql date string');
		return false;
	}
	return new Date(year + '/' + month + '/' + day + ' 12:00:00');
};
String.prototype.hash = function() {
	let secret = 'Does this unit have a soul?';

	return m.crypto.createHmac('sha256', secret).update(this.toString()).digest('hex');
};
Object.prototype.size = function() {
	let size = 0;

	for (let param_i in this) {
		if (!this.hasOwnProperty(param_i)) continue;
		size++;
	}
	return size;
};
Object.prototype.random_int_in_range = function() {
	if (!(this instanceof Array)) {
		console.warn('Object.random_int_in_range:', 'is not an array');
		return false;
	}
	if (this['length'] !== 2) {
		console.warn('Object.random_int_in_range:', 'not a range');
		return false;
	}
	let min = parseInt(this[0]), max = parseInt(this[1]);

	if (isNaN(min)) {
		console.warn('Object.random_int_in_range:', 'min is not a number');
		return false;
	}
	if (isNaN(max)) {
		console.warn('Object.random_int_in_range:', 'max is not a number');
		return false;
	}
	return Math.floor(Math.random() * (max - min + 1)) + min;
};
Object.prototype.populate = function(params, condition) {
	if (params === null || typeof params !== 'object') {
		console.warn('Object.populate:', 'params is undefined');
		return;
	}
	for (let param_i in params) {
		if (!params.hasOwnProperty(param_i)) continue;
		if (typeof condition === 'function') {
			if (condition(param_i, params[param_i]))
				this[param_i] = params[param_i];
		}
		else
			this[param_i] = params[param_i];
	}
};
Object.prototype.save = function(file_path) {
	if (!m.path.parse(file_path).ext) {
		console.warn('Object.save:', 'file_path is not an actual path');
		return;
	}
	let string = JSON.stringify(this, null, 2);

	if (this.size() === 0) string = '{}';
	string.save(file_path);
};
Object.prototype.log = function() {
	console.log(JSON.stringify(this, null, 2));
};
Date.prototype.sql_months = [
	'JAN', 'FEB', 'MAR', 'APR',
	'MAY', 'JUN', 'JUL', 'AUG',
	'SEP', 'OCT', 'NOV', 'DEC'
];
Date.prototype.sql_date = function() {
	var day, month, year;

	day = this.getDate();
	if (day < 10) day = '0' + day;
	month = Date.prototype.sql_months[this.getMonth()];
	year = this.getFullYear();
	return day + '-' + month + '-' + year;
};

function CLIENT(client) {
	this.table = 'CLIENTS';
	this.self = {};
	this.orders = {};
	this.self.populate(client);
	/*this.orders.populate(
		g.tables.ORDERS,
		(key, value) => {
			return this.self['ORDER_ID'] == value['ID'];
		}
	);*/
}
CLIENT.prototype.generate_orders = function() {
	let id = this.self['ID'],
		completed_orders = this.self['COMPLETED_ORDERS'],
		refused_orders = this.self['REFUSED_ORDERS'],
		awaiting_orders = [4, 7].random_int_in_range(),
		working_on_orders = [4, 7].random_int_in_range(),
		date_range = [
			new Date('2016/1/1').getTime(),
			new Date('2017/2/28').getTime()
		],
		statuses = ['awaiting', 'refused', 'completed', 'working on'],
		generate = (amount, status) => {
			for (let order_i = 0; order_i < amount; order_i++) {
				let order,
					date = new Date(date_range.random_int_in_range());

				order = ORDER.prototype.create(id, date, status);
				if (!order) {
					console.warn('CLIENT.generate_orders:', 'ORDER wan not generated', id, date, status);
					return;
				}
				this.orders[order['ID']] = order;
			}
		};

	generate(awaiting_orders, statuses[0]);
	generate(working_on_orders, statuses[3]);
	generate(refused_orders, statuses[1]);
	generate(completed_orders, statuses[2]);
	return this;
};
CLIENT.prototype.insert = function() {
	return `INSERT ${this.table} VALUES ('${this.self['TYPE']}', '${this.self['NAME']}', '${this.self['ADDRESS']}', '${this.self['CONTACTS']}', ${this.self['COMPLETED_ORDERS']}, ${this.self['REFUSED_ORDERS']}, '${this.self['REPUTATION']}');`;
};

function PRODUCTION(production) {
	this.table = 'PRODUCTION';
	this.self = {};
	this.self.populate(production);
}
PRODUCTION.prototype.insert = function() {
	return `INSERT ${this.table} VALUES ('${this.self['NAME']}', '${this.self['NICKNAME']}', ${this.self['DONE']}, ${this.self['ON_DEMAND']}, ${this.self['VALUE']});`;
};

function DEPARTMENT(department) {
	this.table = 'DEPARTMENTS';
	this.self = {};
	this.self.populate(department);
}
DEPARTMENT.prototype.insert = function() {
	return `INSERT ${this.table} VALUES ('${this.self['NAME']}');`;
};

function SUPERVISOR(supervisor) {
	this.table = 'SUPERVISORS';
	this.self = {};
	this.supervisor_salaries = [];
	this.self.populate(supervisor);
}
SUPERVISOR.prototype.generate_salaries = function() {
	let id = this.self['ID'],
		salaries = {
			'Отдел разработки 01': 28000,
			'Отдел разработки 02': 28000,
			'Отдел снабжения': 20000
		},
		department_id = this.self['DEPARTMENT_ID'],
		fixed_rate,
		effort_total = 0,
		bonus_total = 0,
		effort_from_workers,
		bonus_from_workers,
		payday,
		amplifier = .2,
		salary;

	for (let department_i in g.tables.DEPARTMENTS) {
		if (!g.tables.DEPARTMENTS.hasOwnProperty(department_i)) continue;
		let department = g.tables.DEPARTMENTS[department_i];

		if (department_id == department['ID']) fixed_rate = salaries[department['NAME']];
	}
	for (let worker_i in g.tables.WORKERS) {
		if (!g.tables.WORKERS.hasOwnProperty(worker_i)) continue;
		let worker = g.tables.WORKERS[worker_i];

		if (department_id == worker['DEPARTMENT_ID']) {
			let worker_id = worker['ID'];

			for (let salary_i in g.tables.WORKER_SALARY) {
				if (!g.tables.WORKER_SALARY.hasOwnProperty(salary_i)) continue;
				let salary = g.tables.WORKER_SALARY[salary_i];

				if (worker_id == salary['WORKER_ID']) {
					effort_total += salary['CURRENT_EFFORT'];
					bonus_total += salary['CURRENT_BONUS'];
				}
			}
		}
	}
	effort_from_workers = Math.floor(effort_total*amplifier);
	bonus_from_workers = Math.floor(bonus_total*amplifier);
	payday = new Date('2017/4/11');
	salary = SUPERVISOR_SALARY.prototype.create(id, payday, effort_from_workers, bonus_from_workers, fixed_rate);
	if (!salary) {
		console.warn('SUPERVISOR.generate_salaries:', 'SUPERVISOR_SALARY wan not generated', id, payday, effort_from_workers, bonus_from_workers, fixed_rate);
		return;
	}
	this.supervisor_salaries[salary['ID']] = salary;
};
SUPERVISOR.prototype.insert = function() {
	return `INSERT ${this.table} VALUES ('${this.self['NAME']}', ${this.self['DEPARTMENT_ID']}, '${this.self['LOGIN']}', '${this.self['PASSWORD']}', '${this.self['AVATAR']}');`;
};

function WORKER(worker) {
	this.table = 'WORKERS';
	this.self = {};
	this.worker_stats_items = {};
	this.worker_stats_orders = {};
	this.worker_salaries = [];
	this.self.populate(worker);
}
WORKER.prototype.generate_stats_items = function() {
	let id = this.self['ID'],
		workers_amount = 6,
		last_month_range = [2, 30],
		effort_amplifier = .1;

	for (let product_i in g.tables.PRODUCTION) {
		if (!g.tables.PRODUCTION.hasOwnProperty(product_i)) continue;
		let product = g.tables.PRODUCTION[product_i],
			product_id = product['ID'],
			max_done_total = Math.floor(product['DONE']/workers_amount),
			done_total_range = [max_done_total-15, max_done_total+50],
			last_month_amplifier = last_month_range.random_int_in_range()/100,
			worker_stats_item, done_total, effort_total, done_last_month, effort_last_month;

		done_total = done_total_range.random_int_in_range();
		done_last_month = Math.floor(done_total*last_month_amplifier);
		effort_total = Math.floor(done_total*product['VALUE']*effort_amplifier);
		effort_last_month = Math.floor(done_last_month*product['VALUE']*effort_amplifier);
		worker_stats_item = WORKER_STATS_ITEM.prototype.create(id, product_id, done_total, effort_total, done_last_month, effort_last_month);
		if (!worker_stats_item) {
			console.warn('WORKER.generate_appointment:', 'WORKER_STATS_ITEM wan not generated', id, product_id, done_total, effort_total, done_last_month, effort_last_month);
			return;
		}
		this.worker_stats_items[worker_stats_item['ID']] = worker_stats_item;
	}
};
WORKER.prototype.generate_stats_orders = function() {
	let id = this.self['ID'],
		last_month_range = [5, 15],
		last_month_chance = last_month_range.random_int_in_range()/100,
		bonus_amplifier = 250,
		bonus_total, bonus_last_month,
		lead_engineer_total = 0,
		lead_engineer_last_month,
		worker_stats_order;

	for (let schedule_i in g.tables.SCHEDULE) {
		if (!g.tables.SCHEDULE.hasOwnProperty(schedule_i)) continue;
		let schedule = g.tables.SCHEDULE[schedule_i];

		if (id == schedule['LEAD_ENGINEER_ID']) lead_engineer_total++;
	}
	bonus_total = lead_engineer_total*bonus_amplifier;
	lead_engineer_last_month = Math.floor(lead_engineer_total*last_month_chance);
	bonus_last_month = lead_engineer_last_month*bonus_amplifier;
	worker_stats_order = WORKER_STATS_ORDER.prototype.create(id, lead_engineer_total, bonus_total, lead_engineer_last_month, bonus_last_month);
	if (!worker_stats_order) {
		console.warn('WORKER.generate_stats_orders:', 'WORKER_STATS_ORDER wan not generated', id, lead_engineer_total, bonus_total, lead_engineer_last_month, bonus_last_month);
		return;
	}
	this.worker_stats_orders[worker_stats_order['ID']] = worker_stats_order;
};
WORKER.prototype.generate_salaries = function() {
	let id = this.self['ID'],
		salaries = {
			'Инженер': 21000,
			'Кадровик': 15000,
			'Бухгалтер': 16500,
			'Системный администратор': 18000
		},
		fixed_rate = salaries[this.self['POSITION']],
		current_effort = 0, current_bonus, payday, salary;

	for (let item_i in g.tables.WORKER_STATS_ITEMS) {
		if (!g.tables.WORKER_STATS_ITEMS.hasOwnProperty(item_i)) continue;
		let item = g.tables.WORKER_STATS_ITEMS[item_i];

		if (id != item['WORKER_ID']) continue;
		current_effort += item['EFFORT_VALUE_LAST_MONTH'];
	}
	for (let order_i in g.tables.WORKER_STATS_ORDERS) {
		if (!g.tables.WORKER_STATS_ORDERS.hasOwnProperty(order_i)) continue;
		let order = g.tables.WORKER_STATS_ORDERS[order_i];

		if (id != order['WORKER_ID']) continue;
		current_bonus = order['BONUS_LAST_MONTH'];
		break;
	}
	if (!(current_bonus > 0)) current_bonus = 0;
	payday = new Date('2017/4/10');
	salary = WORKER_SALARY.prototype.create(id, payday, current_effort, current_bonus, fixed_rate);
	if (!salary) {
		console.warn('WORKER.generate_salaries:', 'WORKER_SALARY wan not generated', id, payday, current_effort, current_bonus, fixed_rate);
		return;
	}
	this.worker_salaries[salary['ID']] = salary;
};
WORKER.prototype.insert = function() {
	return `INSERT ${this.table} VALUES ('${this.self['NAME']}', '${this.self['POSITION']}', ${this.self['DEPARTMENT_ID']}, '${this.self['LOGIN']}', '${this.self['PASSWORD']}', '${this.self['AVATAR']}');`;
};

function ORDER(order) {
	this.table = 'ORDERS';
	this.self = {};
	this.purchased_items = {};
	this.schedule = {};
	this.appointments = {};
	this.self.populate(order);
}
ORDER.prototype.get_new_id = function() {
	//noinspection JSUnresolvedFunction
	return g.tables.ORDERS.size()+1;
};
ORDER.prototype.create = function(client_id, date, status) {
	if (!(client_id > 0)) {
		console.warn('ORDER.create:', 'client_id is undefined');
		return false;
	}
	date = date || new Date();
	status = status || 'awaiting';
	let id = ORDER.prototype.get_new_id(),
		order = {
		ID: id,
		CLIENT_ID: client_id,
		DATE: date.sql_date(),
		STATUS: status
	};

	g.tables.ORDERS[id] = order;
	return order;
};
ORDER.prototype.generate_purchased_items = function() {
	let id = this.self['ID'],
		company_type = g.tables.ORDERS[this.self['CLIENT_ID']]['TYPE'],
		products_used = [],
		product_type_amount = [1, g.tables.PRODUCTION.size()].random_int_in_range(),
		product_type_range = [1, g.tables.PRODUCTION.size()],
		quantity_range,
		get_product_id = () => {
			let product_id = product_type_range.random_int_in_range();
			while (products_used.indexOf(product_id) !== -1) {
				product_id = product_type_range.random_int_in_range();
			}
			return product_id;
		};

	if (company_type == 'company')
		quantity_range = [1, Math.floor(400/product_type_amount)];
	else if (company_type == 'individual')
		quantity_range = [1, Math.floor(200/product_type_amount)];
	else
		quantity_range = [1, Math.floor(20/product_type_amount)];

	for (let item_i = 0; item_i < product_type_amount; item_i++) {
		let purchased_item,
			product_id = get_product_id(),
			product_quantity = quantity_range.random_int_in_range();

		products_used.push(product_id);
		purchased_item = PURCHASED_ITEM.prototype.create(id, product_id, product_quantity);
		if (!purchased_item) {
			console.warn('ORDER.generate_purchased_items:', 'PURCHASED_ITEM wan not generated', id, product_id, product_quantity);
			return;
		}
		this.purchased_items[purchased_item['ID']] = purchased_item;
	}
	return this;
};
ORDER.prototype.generate_schedule = function() {
	let id = this.self['ID'],
		lead_engineer_range = [1, 6],
		date_range = [
			new Date('2016/4/1').getTime(),
			new Date('2017/6/30').getTime()
		],
		amplifier = .2,
		total = 0,
		value,
		products = {},
		lead_engineer,
		date,
		schedule;

	for (let item_i in g.tables.PURCHASED_ITEMS) {
		if (!g.tables.PURCHASED_ITEMS.hasOwnProperty(item_i)) continue;
		let item = g.tables.PURCHASED_ITEMS[item_i];

		if (id != item['ORDER_ID']) continue;
		if (products[item['PRODUCT_ID']] === undefined) products[item['PRODUCT_ID']] = 0;
		products[item['PRODUCT_ID']] += item['QUANTITY'];
	}
	for (let product_id in products) {
		if (!products.hasOwnProperty(product_id)) continue;
		total += g.tables.PRODUCTION[product_id]['VALUE']*products[product_id];
	}
	value = total + total*amplifier;
	lead_engineer = lead_engineer_range.random_int_in_range();
	date = new Date(date_range.random_int_in_range());

	schedule = SCHEDULE.prototype.create(id, lead_engineer, date, value);
	if (!schedule) {
		console.warn('ORDER.generate_schedule:', 'SCHEDULE wan not generated', id, lead_engineer, date, value);
		return;
	}
	this.schedule[schedule['ID']] = schedule;
};
ORDER.prototype.generate_appointment = function() {
	let id = this.self['ID'],
		lead_engineer,
		workers_used = [],
		workers_amount = [1, 4].random_int_in_range(),
		workers_range = [1, 6],
		get_worker_id = ()=> {
			let worker_id = workers_range.random_int_in_range();

			while (lead_engineer == worker_id || workers_used.indexOf(worker_id) !== -1) {
				worker_id = workers_range.random_int_in_range();
			}
			return worker_id;
		};

	for (let schedule_i in g.tables.SCHEDULE) {
		if (!g.tables.SCHEDULE.hasOwnProperty(schedule_i)) continue;
		let schedule = g.tables.SCHEDULE[schedule_i];

		if (id != schedule['ORDER_ID']) continue;
		lead_engineer = schedule['LEAD_ENGINEER_ID'];
	}
	for (let worker_i = 1; worker_i <= workers_amount; worker_i++) {
		let worker_id = get_worker_id(), appointment;

		workers_used.push(worker_id);
		appointment = APPOINTMENT.prototype.create(id, worker_id);
		if (!appointment) {
			console.warn('ORDER.generate_appointment:', 'APPOINTMENT wan not generated', id, worker_id);
			return;
		}
		this.appointments[appointment['ID']] = appointment;
	}
};
ORDER.prototype.insert = function() {
	return `INSERT ${this.table} VALUES (${this.self['CLIENT_ID']}, '${this.self['DATE']}', '${this.self['STATUS']}');`;
};

function PURCHASED_ITEM(purchased_item) {
	this.table = 'PURCHASED_ITEMS';
	this.self = {};
	this.self.populate(purchased_item);
}
PURCHASED_ITEM.prototype.get_new_id = function() {
	return g.tables.PURCHASED_ITEMS.size()+1;
};
PURCHASED_ITEM.prototype.create = function(order_id, product_id, quantity) {
	if (!(order_id > 0)) {
		console.warn('PURCHASED_ITEM.create:', 'order_id is undefined');
		return false;
	}
	if (!(product_id > 0)) {
		console.warn('PURCHASED_ITEM.create:', 'product_id is undefined');
		return false;
	}
	quantity = quantity || 1;
	let id = PURCHASED_ITEM.prototype.get_new_id(),
		purchased_item = {
			ID: id,
			ORDER_ID: order_id,
			PRODUCT_ID: product_id,
			QUANTITY: quantity
		};

	g.tables.PURCHASED_ITEMS[id] = purchased_item;
	return purchased_item;
};
PURCHASED_ITEM.prototype.insert = function() {
	return `INSERT ${this.table} VALUES (${this.self['ORDER_ID']}, ${this.self['PRODUCT_ID']}, ${this.self['QUANTITY']});`;
};

function SCHEDULE(schedule) {
	this.table = 'SCHEDULE';
	this.self = {};
	this.self.populate(schedule);
}
SCHEDULE.prototype.get_new_id = function() {
	return g.tables.SCHEDULE.size()+1;
};
SCHEDULE.prototype.create = function(order_id, lead_engineer, date, value) {
	if (!(order_id > 0)) {
		console.warn('SCHEDULE.create:', 'order_id is undefined');
		return false;
	}
	if (!date) {
		console.warn('SCHEDULE.create:', 'date is undefined');
		return false;
	}
	lead_engineer = lead_engineer || null;
	value = value || 0;
	let id = SCHEDULE.prototype.get_new_id(),
		schedule = {
			ID: id,
			ORDER_ID: order_id,
			LEAD_ENGINEER_ID: lead_engineer,
			EST_COMPLETION_DATE: date.sql_date(),
			VALUE: value
		};

	g.tables.SCHEDULE[id] = schedule;
	return schedule;
};
SCHEDULE.prototype.insert = function() {
	return `INSERT ${this.table} VALUES (${this.self['ORDER_ID']}, ${this.self['LEAD_ENGINEER_ID']}, '${this.self['EST_COMPLETION_DATE']}', ${this.self['VALUE']});`;
};

function APPOINTMENT(appointment) {
	this.table = 'APPOINTMENTS';
	this.self = {};
	this.self.populate(appointment);
}
APPOINTMENT.prototype.get_new_id = function() {
	return g.tables.APPOINTMENTS.size()+1;
};
APPOINTMENT.prototype.create = function(order_id, worker_id) {
	if (!(order_id > 0)) {
		console.warn('APPOINTMENT.create:', 'order_id is undefined');
		return false;
	}
	if (!(worker_id > 0)) {
		console.warn('APPOINTMENT.create:', 'worker_id is undefined');
		return false;
	}
	let id = APPOINTMENT.prototype.get_new_id(),
		appointment = {
			ID: id,
			ORDER_ID: order_id,
			WORKER_ID: worker_id
		};

	g.tables.APPOINTMENTS[id] = appointment;
	return appointment;
};
APPOINTMENT.prototype.insert = function() {
	return `INSERT ${this.table} VALUES (${this.self['ORDER_ID']}, ${this.self['WORKER_ID']});`;
};

function WORKER_STATS_ITEM(worker_stats_item) {
	this.table = 'WORKER_STATS_ITEMS';
	this.self = {};
	this.self.populate(worker_stats_item);
}
WORKER_STATS_ITEM.prototype.get_new_id = function() {
	return g.tables.WORKER_STATS_ITEMS.size()+1;
};
WORKER_STATS_ITEM.prototype.create = function(worker_id, product_id, done_total, effort_total, done_last_month, effort_last_month) {
	if (!(worker_id > 0)) {
		console.warn('WORKER_STATS_ITEM.create:', 'worker_id is undefined');
		return false;
	}
	if (!(product_id > 0)) {
		console.warn('WORKER_STATS_ITEM.create:', 'product_id is undefined');
		return false;
	}
	done_total = done_total || 0;
	effort_total = effort_total || 0;
	done_last_month = done_last_month || 0;
	effort_last_month = effort_last_month || 0;
	let id = WORKER_STATS_ITEM.prototype.get_new_id(),
		worker_stats_item = {
			ID: id,
			WORKER_ID: worker_id,
			PRODUCT_ID: product_id,
			DONE_TOTAL: done_total,
			EFFORT_VALUE_TOTAL: effort_total,
			DONE_LAST_MONTH: done_last_month,
			EFFORT_VALUE_LAST_MONTH: effort_last_month
		};

	g.tables.WORKER_STATS_ITEMS[id] = worker_stats_item;
	return worker_stats_item;
};
WORKER_STATS_ITEM.prototype.insert = function() {
	return `INSERT ${this.table} VALUES (${this.self['WORKER_ID']}, ${this.self['PRODUCT_ID']}, ${this.self['DONE_TOTAL']}, ${this.self['EFFORT_VALUE_TOTAL']}, ${this.self['DONE_LAST_MONTH']}, ${this.self['EFFORT_VALUE_LAST_MONTH']});`;
};

function WORKER_STATS_ORDER(worker_stats_order) {
	this.table = 'WORKER_STATS_ORDERS';
	this.self = {};
	this.self.populate(worker_stats_order);
}
WORKER_STATS_ORDER.prototype.get_new_id = function() {
	return g.tables.WORKER_STATS_ORDERS.size()+1;
};
WORKER_STATS_ORDER.prototype.create = function(worker_id, was_lead_engineer_total, bonus_total, was_lead_engineer_last_month, bonus_last_month) {
	if (!(worker_id > 0)) {
		console.warn('WORKER_STATS_ORDER.create:', 'worker_id is undefined');
		return false;
	}
	was_lead_engineer_total = was_lead_engineer_total || 0;
	bonus_total = bonus_total || 0;
	was_lead_engineer_last_month = was_lead_engineer_last_month || 0;
	bonus_last_month = bonus_last_month || 0;
	let id = WORKER_STATS_ORDER.prototype.get_new_id(),
		worker_stats_order = {
			ID: id,
			WORKER_ID: worker_id,
			WAS_LEAD_ENGI_TOTAL: was_lead_engineer_total,
			BONUS_TOTAL: bonus_total,
			WAS_LEAD_ENGI_LAST_MONTH: was_lead_engineer_last_month,
			BONUS_LAST_MONTH: bonus_last_month
		};

	g.tables.WORKER_STATS_ORDERS[id] = worker_stats_order;
	return worker_stats_order;
};
WORKER_STATS_ORDER.prototype.insert = function() {
	return `INSERT ${this.table} VALUES (${this.self['WORKER_ID']}, ${this.self['WAS_LEAD_ENGI_TOTAL']}, ${this.self['BONUS_TOTAL']}, ${this.self['WAS_LEAD_ENGI_LAST_MONTH']}, ${this.self['BONUS_LAST_MONTH']});`;
};

function WORKER_SALARY(worker_salary) {
	this.table = 'WORKER_SALARY';
	this.self = {};
	this.self.populate(worker_salary);
}
WORKER_SALARY.prototype.get_new_id = function() {
	return g.tables.WORKER_SALARY.size()+1;
};
WORKER_SALARY.prototype.create = function(worker_id, date, current_effort, current_bonus, fixed_rate) {
	if (!(worker_id > 0)) {
		console.warn('WORKER_SALARY.create:', 'worker_id is undefined');
		return false;
	}
	if (!date) {
		console.warn('WORKER_SALARY.create:', 'date is undefined');
		return false;
	}
	current_effort = current_effort || 0;
	current_bonus = current_bonus || 0;
	fixed_rate = fixed_rate || 0;
	let id = WORKER_SALARY.prototype.get_new_id(),
		worker_salary = {
			ID: id,
			WORKER_ID: worker_id,
			PAYDAY: date.sql_date(),
			CURRENT_EFFORT: current_effort,
			CURRENT_BONUS: current_bonus,
			FIXED_RATE: fixed_rate
		};

	g.tables.WORKER_SALARY[id] = worker_salary;
	return worker_salary;
};
WORKER_SALARY.prototype.insert = function() {
	return `INSERT ${this.table} VALUES (${this.self['WORKER_ID']}, '${this.self['PAYDAY']}', ${this.self['CURRENT_EFFORT']}, ${this.self['CURRENT_BONUS']}, ${this.self['FIXED_RATE']});`;
};

function SUPERVISOR_SALARY(supervisor_salary) {
	this.table = 'SUPERVISOR_SALARY';
	this.self = {};
	this.self.populate(supervisor_salary);
}
SUPERVISOR_SALARY.prototype.get_new_id = function() {
	return g.tables.SUPERVISOR_SALARY.size()+1;
};
SUPERVISOR_SALARY.prototype.create = function(supervisor_id, date, effort_from_workers, bonus_form_workers, fixed_rate) {
	if (!(supervisor_id > 0)) {
		console.warn('SUPERVISOR_SALARY.create:', 'supervisor_id is undefined');
		return false;
	}
	if (!date) {
		console.warn('SUPERVISOR_SALARY.create:', 'date is undefined');
		return false;
	}
	effort_from_workers = effort_from_workers || 0;
	bonus_form_workers = bonus_form_workers || 0;
	fixed_rate = fixed_rate || 0;
	let id = SUPERVISOR_SALARY.prototype.get_new_id(),
		supervisor_salary = {
			ID: id,
			SUPERVISOR_ID: supervisor_id,
			PAYDAY: date.sql_date(),
			EFFORT_FROM_WORKERS: effort_from_workers,
			BONUS_FROM_WORKERS: bonus_form_workers,
			FIXED_RATE: fixed_rate
		};

	g.tables.SUPERVISOR_SALARY[id] = supervisor_salary;
	return supervisor_salary;
};
SUPERVISOR_SALARY.prototype.insert = function() {
	return `INSERT ${this.table} VALUES (${this.self['SUPERVISOR_ID']}, '${this.self['PAYDAY']}', ${this.self['EFFORT_FROM_WORKERS']}, ${this.self['BONUS_FROM_WORKERS']}, ${this.self['FIXED_RATE']});`;
};

function save_orders() {
	for (let client_i in g.tables.CLIENTS) {
		if (!g.tables.CLIENTS.hasOwnProperty(client_i)) continue;
		new CLIENT(g.tables.CLIENTS[client_i]).generate_orders();
	}
	//g.tables.ORDERS.log();
	g.tables.ORDERS.save(m.path.join(__dirname, 'json/ORDERS.json'));
}
function save_purchased_items() {
	for (let order_i in g.tables.ORDERS) {
		if (!g.tables.ORDERS.hasOwnProperty(order_i)) continue;
		new ORDER(g.tables.ORDERS[order_i]).generate_purchased_items();
	}
	//g.tables.PURCHASED_ITEMS.log();
	g.tables.PURCHASED_ITEMS.save(m.path.join(__dirname, 'json/PURCHASED_ITEMS.json'));
}
function save_production() {
	let done_products = {},
		on_demand_products = {};

	for (let order_i in g.tables.ORDERS) {
		if (!g.tables.ORDERS.hasOwnProperty(order_i)) continue;
		let order = g.tables.ORDERS[order_i];
		
		if (order['STATUS'] == 'completed') {
			for (let item_i in g.tables.PURCHASED_ITEMS) {
				if (!g.tables.PURCHASED_ITEMS.hasOwnProperty(item_i)) continue;
				let item = g.tables.PURCHASED_ITEMS[item_i];

				if (order['ID'] != item['ORDER_ID']) continue;
				if (done_products[item['PRODUCT_ID']] === undefined) done_products[item['PRODUCT_ID']] = 0;
				done_products[item['PRODUCT_ID']] += item['QUANTITY'];
			}
		} else if (order['STATUS'] == 'awaiting' || order['STATUS'] == 'working on') {
			for (let item_i in g.tables.PURCHASED_ITEMS) {
				if (!g.tables.PURCHASED_ITEMS.hasOwnProperty(item_i)) continue;
				let item = g.tables.PURCHASED_ITEMS[item_i];

				if (order['ID'] != item['ORDER_ID']) continue;
				if (on_demand_products[item['PRODUCT_ID']] === undefined) on_demand_products[item['PRODUCT_ID']] = 0;
				on_demand_products[item['PRODUCT_ID']] += item['QUANTITY'];
			}
		}
	}
	//done_products.log();
	//on_demand_products.log();
	for (let product_id in done_products) {
		if (!done_products.hasOwnProperty(product_id)) continue;
		g.tables.PRODUCTION[product_id]['DONE'] = done_products[product_id];
	}
	for (let product_id in on_demand_products) {
		if (!on_demand_products.hasOwnProperty(product_id)) continue;
		g.tables.PRODUCTION[product_id]['ON_DEMAND'] = on_demand_products[product_id];
	}
	g.tables.PRODUCTION.save(m.path.join(__dirname, 'json/PRODUCTION.json'));
}
function save_schedule() {
	for (let order_i in g.tables.ORDERS) {
		if (!g.tables.ORDERS.hasOwnProperty(order_i)) continue;
		new ORDER(g.tables.ORDERS[order_i]).generate_schedule();
	}
	//g.tables.SCHEDULE.log();
	g.tables.SCHEDULE.save(m.path.join(__dirname, 'json/SCHEDULE.json'));
}
function save_appointments() {
	for (let order_i in g.tables.ORDERS) {
		if (!g.tables.ORDERS.hasOwnProperty(order_i)) continue;
		new ORDER(g.tables.ORDERS[order_i]).generate_appointment();
	}
	//g.tables.APPOINTMENTS.log();
	g.tables.APPOINTMENTS.save(m.path.join(__dirname, 'json/APPOINTMENTS.json'));
}
function save_worker_stats_items() {
	for (let worker_i in g.tables.WORKERS) {
		if (!g.tables.WORKERS.hasOwnProperty(worker_i)) continue;
		let worker = g.tables.WORKERS[worker_i];

		if (worker['POSITION'] != 'Инженер') continue;
		new WORKER(worker).generate_stats_items();
	}
	//g.tables.WORKER_STATS_ITEMS.log();
	g.tables.WORKER_STATS_ITEMS.save(m.path.join(__dirname, 'json/WORKER_STATS_ITEMS.json'));
}
function save_worker_stats_orders() {
	for (let worker_i in g.tables.WORKERS) {
		if (!g.tables.WORKERS.hasOwnProperty(worker_i)) continue;
		let worker = g.tables.WORKERS[worker_i];

		if (worker['POSITION'] != 'Инженер') continue;
		new WORKER(worker).generate_stats_orders();
	}
	//g.tables.WORKER_STATS_ORDERS.log();
	g.tables.WORKER_STATS_ORDERS.save(m.path.join(__dirname, 'json/WORKER_STATS_ORDERS.json'));
}
function save_worker_salaries() {
	for (let worker_i in g.tables.WORKERS) {
		if (!g.tables.WORKERS.hasOwnProperty(worker_i)) continue;
		let worker = g.tables.WORKERS[worker_i];

		new WORKER(worker).generate_salaries();
	}
	//g.tables.WORKER_SALARY.log();
	g.tables.WORKER_SALARY.save(m.path.join(__dirname, 'json/WORKER_SALARY.json'));
}
function save_supervisor_salaries() {
	for (let supervisor_i in g.tables.SUPERVISORS) {
		if (!g.tables.SUPERVISORS.hasOwnProperty(supervisor_i)) continue;
		let supervisor = g.tables.SUPERVISORS[supervisor_i];

		new SUPERVISOR(supervisor).generate_salaries();
	}
	//g.tables.SUPERVISOR_SALARY.log();
	g.tables.SUPERVISOR_SALARY.save(m.path.join(__dirname, 'json/SUPERVISOR_SALARY.json'));
}
function save_workers_credentials() {
	for (let worker_i in g.tables.WORKERS) {
		if (!g.tables.WORKERS.hasOwnProperty(worker_i)) continue;
		let worker = g.tables.WORKERS[worker_i],
			name = worker['NAME'],
			split = name.split(' '),
			password = split[0];

		//if (worker['PASSWORD_HASH']) delete worker['PASSWORD_HASH'];
		worker['LOGIN'] = split[1];
		worker['PASSWORD'] = password;
		if (password == 'Шмидт') worker['AVATAR'] = '02'; else
		if (password == 'Кузнецова') worker['AVATAR'] = '03'; else
		if (password == 'Яковлева') worker['AVATAR'] = '04'; else
		if (password == 'Сидорова') worker['AVATAR'] = '06'; else
		if (password == 'Ковальский') worker['AVATAR'] = '07'; else
		if (password == 'Бусыгин') worker['AVATAR'] = '08'; else
		if (password == 'Морозова') worker['AVATAR'] = '10'; else
		if (password == 'Зуева') worker['AVATAR'] = '11'; else
		if (password == 'Иванов') worker['AVATAR'] = '12';
	}
	//g.tables.WORKERS.log();
	g.tables.WORKERS.save(m.path.join(__dirname, 'json/WORKERS.json'));
}
function save_supervisors_credentials() {
	for (let supervisor_i in g.tables.SUPERVISORS) {
		if (!g.tables.SUPERVISORS.hasOwnProperty(supervisor_i)) continue;
		let supervisor = g.tables.SUPERVISORS[supervisor_i],
			name = supervisor['NAME'],
			split = name.split(' '),
			password = split[0];

		//if (supervisor['PASSWORD_HASH']) delete supervisor['PASSWORD_HASH'];
		supervisor['LOGIN'] = split[1];
		supervisor['PASSWORD'] = password;
		if (password == 'Орлов') supervisor['AVATAR'] = '01'; else
		if (password == 'Токарева') supervisor['AVATAR'] = '05'; else
		if (password == 'Тихомиров') supervisor['AVATAR'] = '09';
	}
	//g.tables.SUPERVISORS.log();
	g.tables.SUPERVISORS.save(m.path.join(__dirname, 'json/SUPERVISORS.json'));
}
function sql() {
	let tables = [
			{
				name: 'CLIENTS',
				table: g.tables.CLIENTS,
				proto: CLIENT
			},
			{
				name: 'PRODUCTION',
				table: g.tables.PRODUCTION,
				proto: PRODUCTION
			},
			{
				name: 'DEPARTMENTS',
				table: g.tables.DEPARTMENTS,
				proto: DEPARTMENT
			},
			{
				name: 'SUPERVISORS',
				table: g.tables.SUPERVISORS,
				proto: SUPERVISOR
			},
			{
				name: 'WORKERS',
				table: g.tables.WORKERS,
				proto: WORKER
			},
			{
				name: 'ORDERS',
				table: g.tables.ORDERS,
				proto: ORDER
			},
			{
				name: 'PURCHASED_ITEMS',
				table: g.tables.PURCHASED_ITEMS,
				proto: PURCHASED_ITEM
			},
			{
				name: 'SCHEDULE',
				table: g.tables.SCHEDULE,
				proto: SCHEDULE
			},
			{
				name: 'APPOINTMENTS',
				table: g.tables.APPOINTMENTS,
				proto: APPOINTMENT
			},
			{
				name: 'WORKER_STATS_ITEMS',
				table: g.tables.WORKER_STATS_ITEMS,
				proto: WORKER_STATS_ITEM
			},
			{
				name: 'WORKER_STATS_ORDERS',
				table: g.tables.WORKER_STATS_ORDERS,
				proto: WORKER_STATS_ORDER
			},
			{
				name: 'WORKER_SALARY',
				table: g.tables.WORKER_SALARY,
				proto: WORKER_SALARY
			},
			{
				name: 'SUPERVISOR_SALARY',
				table: g.tables.SUPERVISOR_SALARY,
				proto: SUPERVISOR_SALARY
			}
		],
		sql = `-- noinspection SqlDialectInspectionForFile\n`,
		create = (file_path)=> {
			let result = `USE ${g.db_name};\n`;

			result += m.fs.readFileSync(file_path, 'utf8');
			result += `\n`;
			result += `GO\n`;
			return result;
			/*return new Promise((resolve, reject)=> {
				let result = `USE ${g.db_name};\n`;

				m.fs.readFile(file_path, 'utf8', (errors, string)=> {
					if (errors) reject(errors);
					result += string;
					result += `\n`;
					result += `GO\n`;
					resolve(result);
				});

			 create(m.path.join(__dirname, `./sql/${item.table}.sql`))
			 .then((result)=> {
			 sql += result;
			 })
			 .catch((errors)=> {
			 console.error('sql:', errors);
			});*/
		};

	sql += `USE master;\n`;
	sql += `IF DB_ID('${g.db_name}') IS NULL\n`;
	sql += `	CREATE DATABASE ${g.db_name};\n`;
	sql += `GO\n`;
	for (let table_i = 0; table_i < tables.length; table_i++) {
		let name = tables[table_i].name,
			table = tables[table_i].table,
			proto = tables[table_i].proto;

		sql += create(m.path.join(__dirname, `./sql/${name}.sql`));
		sql += `USE ${g.db_name};\n`;
		for (let item_i in table) {
			if (!table.hasOwnProperty(item_i)) continue;
			sql += new proto(table[item_i]).insert();
			sql += '\n';
		}
		sql += `GO\n`;
	}
	//console.log(sql);
	sql.save(m.path.join(__dirname, 'JUPITER.sql'));
}
