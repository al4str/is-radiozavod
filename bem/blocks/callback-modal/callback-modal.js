/**
 * Модальное окно для заказа обратного звонка
 *
 * @namespace App.c.tools
 * @class CallbackModal
 * @returns {object}
 */
App.c.blocks.CallbackModal = function() {
	var self = this;

	self.opt = {};
	self.dom = {};
	self.on_form_success = function() {
		self.dom.$block.setAttribute('data-state', 'success');
		try {
			yaCounter38047745.reachGoal('callback', {currency: "RUB", order_price: 960.0});
		} catch (e) {
			console.error('CallbackModal: yandex метрика заблокирована');
		}
	};
	self.on_form_submit = function(e) {
		var form = this;

		App.h.trigger(self.dom.$form, 'fh:submit', form);
		e.preventDefault();
	};
	self.prepare_modal = function() {
		var $btn = this,
			form = {};
		
		form.url = $btn.getAttribute('data-url');
		form.params = App.h.get_json($btn.getAttribute('data-params'));
		form.options = App.h.get_json($btn.getAttribute('data-options'));
		self.dom.$submit_btn.onclick = self.on_form_submit.bind(form);
		if (form.options['name'] && form.options['name'] != '') {
			self.dom.$company_name.textContent = form.options['name'];
			self.dom.$company_name.hidden = false;
		}
		App.modal.show(self.dom.$block);
	};
	self.render = function() {
		self.dom.$company_name = self.dom.$block.querySelector('.form-modal__company-name');
		self.dom.$form = self.dom.$block.querySelector('.form-modal__form');
		self.dom.$submit_btn = self.dom.$block.querySelector('.form-modal__submit-btn');
		self.dom.$messages = self.dom.$block.querySelector('.form-modal__messages');
		self.dom.$btns = App.d.d.querySelectorAll('.callback-action');
		App.h.i(self.dom.$btns, function($node){
			$node.onclick = self.prepare_modal.bind($node);
		});
		self.dom.$form.addEventListener('fh:success', self.on_form_success, false);
		self.opt.FH = new App.c.tools.FormHandler({
			$form: self.dom.$form,
			$submit_btn: self.dom.$submit_btn,
			$messages: self.dom.$messages
		});
	};
	self.initiate = function() {
		self.dom.$block = App.d.d.getElementById('callback-modal');
		if (!self.dom.$block) {
			self = null;
			return false;
		}
		self.render();
	};
	self.initiate();
	return {self: self};
};