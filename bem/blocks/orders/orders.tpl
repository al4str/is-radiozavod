<section class="orders">
	<div class="fixed-block">
		<div class="island orders__island">
			<header class="island__header">
				<p class="island__title">Заказы больших компаний (ООО, ОАО, ЗАО)</p>
			</header>
			<div class="table">
				<div class="table__wrp orders__table">
					<div class="table__r table__head orders__table-head">
						<div class="table__c table__h orders__table-cell orders__vid-cell">ID</div>
						<div class="table__c table__h orders__table-cell orders__client-cell">Заказчик</div>
						<div class="table__c table__h orders__table-cell orders__date-cell">Дата заявки</div>
						<div class="table__c table__h orders__table-cell orders__state-cell">Статус</div>
					</div>
					{{for order in this['orders'].orders['company'] :}}
					<div class="table__r orders__table-row order-action" data-obj='{{-order.obj}}' data-is_acceptable="{{-order.is_acceptable}}">
						<div class="table__c orders__table-cell orders__vid-cell">{{-order.vid}}</div>
						<div class="table__c orders__table-cell orders__client-cell">{{-order.client_name}}</div>
						<div class="table__c orders__table-cell orders__date-cell">{{-order.date}}</div>
						<div class="table__c orders__table-cell orders__state-cell">{{-order.status}}</div>
					</div>
					{{end}}
				</div>
			</div>
		</div>
		<div class="island orders__island">
			<header class="island__header">
				<p class="island__title">Заказы индивидуальных предпринимателей</p>
			</header>
			<div class="table orders__table">
				<div class="table__wrp">
					<div class="table__r table__head orders__table-head">
						<div class="table__c table__h orders__table-cell orders__vid-cell">ID</div>
						<div class="table__c table__h orders__table-cell orders__client-cell">Заказчик</div>
						<div class="table__c table__h orders__table-cell orders__date-cell">Дата заявки</div>
						<div class="table__c table__h orders__table-cell orders__state-cell">Статус</div>
					</div>
					{{for order in this['orders'].orders['individual'] :}}
					<div class="table__r orders__table-row order-action" data-obj='{{-order.obj}}' data-is_acceptable="{{-order.is_acceptable}}">
						<div class="table__c orders__table-cell orders__vid-cell">{{-order.vid}}</div>
						<div class="table__c orders__table-cell orders__client-cell">{{-order.client_name}}</div>
						<div class="table__c orders__table-cell orders__date-cell">{{-order.date}}</div>
						<div class="table__c orders__table-cell orders__state-cell">{{-order.status}}</div>
					</div>
					{{end}}
				</div>
			</div>
		</div>
		<div class="island orders__island">
			<header class="island__header">
				<p class="island__title">Заказы ИНТЕРНЕТ-МАГАЗИН</p>
			</header>
			<div class="table orders__table">
				<div class="table__wrp">
					<div class="table__r table__head orders__table-head">
						<div class="table__c table__h orders__table-cell orders__vid-cell">ID</div>
						<div class="table__c table__h orders__table-cell orders__client-cell">Заказчик</div>
						<div class="table__c table__h orders__table-cell orders__date-cell">Дата заявки</div>
						<div class="table__c table__h orders__table-cell orders__state-cell">Статус</div>
					</div>
					{{for order in this['orders'].orders['e-shop'] :}}
					<div class="table__r orders__table-row order-action" data-obj='{{-order.obj}}' data-is_acceptable="{{-order.is_acceptable}}">
						<div class="table__c orders__table-cell orders__vid-cell">{{-order.vid}}</div>
						<div class="table__c orders__table-cell orders__client-cell">{{-order.client_name}}</div>
						<div class="table__c orders__table-cell orders__date-cell">{{-order.date}}</div>
						<div class="table__c orders__table-cell orders__state-cell">{{-order.status}}</div>
					</div>
					{{end}}
				</div>
			</div>
		</div>
	</div>
	{{ if this.worker.is_order_edit_allowed is 'Y' :}}
	<div class="hidden-wrp">
		#order-modal#
	</div>
	{{ end }}
</section>