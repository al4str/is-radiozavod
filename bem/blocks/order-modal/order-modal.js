App.c.blocks.OrderModal = function() {
	var self = this;

	self.opt = {};
	self.dom = {};
	self.on_form_success = function() {
		if (self.dom.$messages.js) self.dom.$messages.js.show({type: 'success', id: 'default'});
		setTimeout(function(){
			location.reload();
		}, 1500);
	};
	self.on_submit = function(e) {
		var form = this,
			$checked_radio = self.dom.$block.querySelector('.radio-block__field:checked'),
			$checked_checkboxes = self.dom.$block.querySelectorAll('.checkbox-block__field:checked');

		if (form.is_acceptable == 'N') {
			if (self.dom.$messages.js) self.dom.$messages.js.show({type: 'error', id: 'bad_reputation'});
			self.dom.$submit_btn.disabled = true;
			return;
		}
		if ($checked_radio) form.params['LEAD_ENGINEER'] = $checked_radio.value;
		else form.params['LEAD_ENGINEER'] = null;
		form.params['WORKERS'] = [];
		App.h.i($checked_checkboxes, function($node){
			form.params['WORKERS'].push($node.value);
		});
		self.opt.FH.dom.$submit_btn = self.dom.$submit_btn;
		App.h.trigger(self.dom.$form, 'fh:submit', form);
		e.preventDefault();
	};
	self.on_refuse = function(e) {
		var form = this;
		
		form.params['STATUS'] = 'refused';
		self.opt.FH.dom.$submit_btn = self.dom.$refuse_btn;
		App.h.trigger(self.dom.$form, 'fh:submit', form);
		e.preventDefault();
	};
	self.on_done = function(e) {
		var form = this;

		if (form.order['status'] != 'Выполняется') {
			return;
		}
		form.params['STATUS'] = 'completed';
		self.opt.FH.dom.$submit_btn = self.dom.$done_btn;
		App.h.trigger(self.dom.$form, 'fh:submit', form);
		e.preventDefault();
	};
	self.prepare_modal = function() {
		var $btn = this,
			form = {};

		if (self.dom.$messages.js) self.dom.$messages.js.hide();
		self.dom.$submit_btn.disabled = false;

		App.h.i(self.dom.$block.querySelectorAll('.radio-block__field'), function($node){
			$node.checked = false;
		});
		App.h.i(self.dom.$block.querySelectorAll('.checkbox-block__field'), function($node){
			$node.checked = false;
			$node.disabled = false;
			$node.parentNode.removeAttribute('data-disabled');
		});
		form.order = App.h.get_json($btn.dataset.obj);
		form.is_acceptable = $btn.dataset.is_acceptable;
		if (form.order['status'] == 'Выполняется') self.dom.$block.setAttribute('data-state', 'wip');
		else self.dom.$block.setAttribute('data-state', 'awaiting');
		form.url = self.dom.$submit_btn.dataset.url;
		form.params = App.h.get_json(self.dom.$submit_btn.dataset.params);
		form.params['ORDER_ID'] = form.order.id;
		self.dom.$submit_btn.onclick = self.on_submit.bind(form);
		self.dom.$done_btn.onclick = self.on_done.bind(form);
		self.dom.$refuse_btn.onclick = self.on_refuse.bind(form);
		App.h.i(form.order, function(value, key){
			var $node = self.dom.$block.querySelector('.order-' + key);

			if ($node) $node.textContent = value;
		});
		App.modal.show(self.dom.$block);
	};
	self.on_checkbox_change = function() {
		var $checkbox_field = this,
			max_amount = 4,
			checked_amount = self.dom.$block.querySelectorAll('.checkbox-block__field:checked').length,
			is_disabled = (checked_amount >= max_amount);

		App.h.i(self.dom.$block.querySelectorAll('.checkbox-block__field:not(:checked)'), function($node){
			if ($node != $checkbox_field) {
				$node.disabled = is_disabled;
				if (is_disabled) $node.parentNode.setAttribute('data-disabled', '');
				else $node.parentNode.removeAttribute('data-disabled');
			}
		});
	};
	self.on_radio_change = function() {
		var $radio_field = this,
			$worker = App.h.get_parent($radio_field, 'order-modal__worker'),
			$checkbox_field = $worker.querySelector('.checkbox-block__field'),
			$checkbox_wrp = $checkbox_field.parentNode;

		$checkbox_field.checked = false;
		$checkbox_field.disabled = true;
		$checkbox_wrp.setAttribute('data-disabled', '');
		self.on_checkbox_change.apply($checkbox_field);
	};
	self.render = function() {
		self.dom.$form = self.dom.$block;
		self.dom.$btns = App.d.d.querySelectorAll('.order-action');
		self.dom.$submit_btn = self.dom.$block.querySelector('.order-modal__submit-btn');
		self.dom.$refuse_btn = self.dom.$block.querySelector('.order-modal__refuse-btn');
		self.dom.$done_btn = self.dom.$block.querySelector('.order-modal__done-btn');
		self.dom.$messages = self.dom.$block.querySelector('.order-modal__messages');
		App.h.i(self.dom.$btns, function($node){
			$node.onclick = self.prepare_modal.bind($node);
		});
		App.h.i(self.dom.$block.querySelectorAll('.radio-block__field'), function($node){
			$node.onclick = self.on_radio_change;
		});
		App.h.i(self.dom.$block.querySelectorAll('.checkbox-block__field'), function($node){
			$node.onchange = self.on_checkbox_change;
		});
		self.dom.$form.addEventListener('fh:success', self.on_form_success, false);
		self.opt.FH = new App.c.tools.FormHandler({
			$form: self.dom.$form,
			$submit_btn: self.dom.$submit_btn,
			$messages: self.dom.$messages
		});
	};
	self.initiate = function() {
		self.dom.$block = App.d.d.getElementById('order-modal');
		if (!self.dom.$block) {
			self = null;
			return false;
		}
		self.render();
	};
	self.initiate();
	return {self: self};
};