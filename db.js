// CREATE TABLE
// INSERT, UPDATE, SELECT radiozavod

const m = {};
m.tedious = require('tedious');
const db = {};
db.const = {};
db.const.config = require('./db_config.json');
db.const.connect = m.tedious['Connection'];
db.const.request = m.tedious['Request'];
db.const.types = m.tedious['TYPES'];
db.connect = function() {
	return new Promise(function(resolve, reject){
		db.not_connected = false;
		db.is_connected = false;
		db.connection = new db.const.connect(db.const.config);
		db.connection.on('error', function(errors) {
			console.info("db.connect", "error occurred");
			reject(errors);
		});
		db.connection.on('errorMessage', function(errors) {
			console.info("db.connect", "error occurred");
			reject(errors);
		});
		db.connection.on('end', function() {
			db.not_connected = false;
			db.is_connected = false;
			db.connection = undefined;
			console.info("db.connect", "connection closed");
			resolve();
		});
		db.connection.on('connect', function(errors) {
			if (errors) {
				db.not_connected = true;
				console.error("db.connect", "was not connected", errors);
				reject(errors);
			}
			db.is_connected = true;
			console.info("db.connect", "connected successfully");
			resolve();
		});
	});
};
db.close = function() {
	console.info("db.close");
	db.connection.close();
};
db.q = function(hold, input_options) {
	if (hold !== undefined && !input_options || typeof input_options !== 'object') {
		console.warn("db.q", "options are not defined");
		input_options = {};
	}
	if (typeof hold === 'object') {
		console.warn("db.q", "hold and options are swapped");
		hold = false;
	}
	if (db.not_connected) {
		console.error("db.q", "connectivity issues");
		return Promise.reject();
	}
	if (!db.is_connected) {
		return db.connect()
			.catch(function(errors) {
				console.error("db.q", 'connection error occurred');
				return Promise.reject(errors);
			})
			.then(function(){
				return db.query(hold, input_options);
			});
	} else {
		return db.query(hold, input_options);
	}
};
db.query = function(hold, input_options) {
	return new Promise(function(resolve, reject) {
		var options = {
			query: `SELECT 'The owls are not what they seem' AS 'The Log Lady'`
		};
		if (typeof input_options === 'object') {
			for (var option_i in options) {
				if (!options.hasOwnProperty(option_i)) continue;
				if (input_options[option_i] !== undefined) options[option_i] = input_options[option_i];
			}
		}
		let regex = new RegExp('DROP|ALTER|CREATE|TRUNCATE|REVOKE|GRANT|LOCK/', 'i');

		if (options.query.match(regex)) console.warn('db.query', 'stop-word found');
		options.query.replace(regex, '');
		var query_request = new db.const.request(
			options.query,
			function(errors) {
				if (errors) {
					console.error('db.query', 'request error occurred');
					reject(errors);
					return;
				}
				console.info("db.query", "query request completed");
				if (!hold) db.close();
				let result = [];
				
				for (let row_i = 0; row_i < response_rows.length; row_i++) {
					let row = response_rows[row_i],
						obj = {};

					for (let col_i = 0; col_i < row.length; col_i++) {
						let col = row[col_i],
							col_name = col['metadata']['colName'];

						obj[col_name] = col['value'];
					}
					result.push(obj);
				}
				if (result.length == 0) resolve(null);
				resolve(result);
			}
		);
		var response_rows = [];
		query_request.on('row', function(columns) {
			response_rows.push(columns);
		});
		//noinspection JSUnresolvedFunction
		db.connection.execSql(query_request);
	});
};
module.exports = db.q;