<section class="welcome">
	<div class="fixed-block welcome__wrp">
		<img class="welcome__logo" src="graphics-jupiter.svg" alt="Радиозавод Юпитер" />
		<img class="welcome__tower" src="graphics-tower.svg" alt="Radio tower" />
		<div class="welcome__about">
			<p class="welcome__quote">"Мы поставляем лучшие радиодетали на рынок, используя современные технологии по самым низким ценам"</p>
			<p class="welcome__by">- Радиозавод "Юпитер"</p>
		</div>
	</div>
</section>