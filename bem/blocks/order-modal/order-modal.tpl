<!--suppress HtmlUnknownTarget -->
<form class="modal order-modal" id="order-modal" action="javascript:void(0);" method="post">
	<button class="btn modal__close-btn modal__close-action" type="button">
		<i class="btn__icon">
			<svg class="btn__svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
				<g fill="none" stroke="inherit" stroke-width="inherit">
					<path d="M0,0 L16,16 M0,16 L16,0"></path>
				</g>
			</svg>
		</i>
	</button>
	<div class="modal__content">
		<p class="order-modal__title">Заказ</p>
		<p class="order-modal__key">ID <span class="order-modal__value order-vid">0000</span></p>
		<p class="order-modal__key">Заказчик <span class="order-modal__value order-client_name">—</span></p>
		<p class="order-modal__key">Дата зявки <span class="order-modal__value order-date">00.00.0000</span></p>
		<p class="order-modal__key">Статус <span class="order-modal__value order-status">Ожидает</span></p>
		<ul class="order-modal__workers">
			<li class="order-modal__worker order-modal__header">
				<div class="order-modal__image">
					&nbsp;
				</div>
				<div class="order-modal__about">
					&nbsp;
				</div>
				<div class="order-modal__appointment">
					<p class="order-modal__name">Назначить</p>
				</div>
				<div class="order-modal__lead-engineer">
					<p class="order-modal__name">Ведущий специалист</p>
				</div>
			</li>
			{{for worker in this['order-modal'].workers :}}
			<li class="order-modal__worker">
				<!--suppress HtmlUnknownTarget -->
				<img class="order-modal__image" src="graphics-worker-{{-worker.avatar}}.svg" alt="{{-worker.name}}" />
				<div class="order-modal__about">
					<p class="order-modal__name">{{-worker.name}}</p>
					<p class="order-modal__spec">{{-worker.department}}</p>
				</div>
				<div class="order-modal__appointment">
					<div class="checkbox-block order-modal__select">
						<label class="checkbox-block__wrp">
							<input class="checkbox-block__field" type="checkbox" value="{{-worker.id}}" />
							<i class="checkbox-block__icon">
								<svg class="checkbox-block__svg" viewBox="0 0 13.96 10.45" xmlns="http://www.w3.org/2000/svg">
									<g fill="inherit">
										<polygon points="13.01 0 5.75 7.26 1.6 3.11 0 4.71 4.15 8.86 4.14 8.87 5.72 10.45 13.96 2.2 13.96 0.96 13.01 0"></polygon>
									</g>
								</svg>
							</i>
							<!--<span class="checkbox-block__cap">Булево поле</span>-->
						</label>
					</div>
				</div>
				<div class="order-modal__lead-engineer">
					<div class="radio-block order-modal__select">
						<label class="radio-block__wrp">
							<input class="radio-block__field" type="radio" name="easteregg" value="{{-worker.id}}" />
							<i class="radio-block__icon">
								<span class="radio-block__dot"></span>
							</i>
							<!--<span class="radio-block__cap">Лорем ипсум и компания</span>-->
						</label>
					</div>
				</div>
			</li>
			{{ end }}
		</ul>
		<div class="order-modal__actions order-modal__actions-awaiting">
			<button class="btn btn_size_lg btn_color_primary order-modal__submit-btn" data-url="/orders/" data-params='{}' type="button">
				<span class="btn__cap">Приступить к выполнению</span>
			</button>
			<button class="btn btn_size_lg order-modal__refuse-btn" data-url="/orders/" data-params='{}' type="button">
				<span class="btn__cap">Отменить заказ</span>
			</button>
		</div>
		<div class="order-modal__actions order-modal__actions-wip">
			<button class="btn btn_size_lg btn_color_green order-modal__done-btn" data-url="/orders/" data-params='{}' type="button">
				<span class="btn__cap">Заказ выполнен</span>
			</button>
		</div>
		#form-messages#
	</div>
</form>