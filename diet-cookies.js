const m = {}; 

m.is_set = require('isset');

function Cookies($) {
	this.__proto__.header = [];
	this.__proto__.signal = $;
	var cookieHeader = $.header('cookie');

	if (cookieHeader) {
		var split = cookieHeader.split(';');
		var split_length = split.length;

		for (var i = 0; i < split_length; i++) {
			var item = split[i];
			var cookieSplit = item.split('=');

			if (cookieSplit.length) {
				var name = cookieSplit[0]?cookieSplit[0].trim():'';

				this[name] = cookieSplit[1]?cookieSplit[1].trim():'';
			}
		}
	}
	return this;
}

Cookies.prototype.set = function(name, value, options){
	options = options || {};
	var domain = (!m.is_set(options.domain))?'':' Domain=' + options.domain + '; ';
	var secure = (m.is_set(options.secure))?'; secure ':'';
	var httpOnly = (m.is_set(options.httpOnly))?'; httpOnly ':'';
	var path = (!m.is_set(options.path))?'/':options.path;
	var expires;
	if (m.is_set(options.expire)) {
		var days = options.expire[0]*1000*60*60*24;
		var hours = options.expire[1] ? options.expire[1]*1000*60*60 : 0 ;
		var minutes = options.expire[2] ? options.expire[2]*1000*60 : 0;
		var future = days+hours+minutes;
		var now = new Date().getTime();
		expires = ' Expires=' + (new Date(now + future)).toGMTString() + ' ';
	} else {
		expires = '';
	}
	var header_string = name + '=' + value
		+ '; Path=' + path + ';'
		+ domain
		+ expires
		+ secure
		+ httpOnly;
	this[name] = value;
	this.header.push(header_string);
	this.signal.header('set-cookie', this.header);
};

Cookies.prototype.delete = function(name, options){
	options = (m.is_set(options))?options:{} ;
	var domain = (!m.is_set(options.domain))?'':' Domain=' + options.domain + '; ';
	var path = (!m.is_set(options.path))?'/':options.path;
	delete this[name];
	this.header.push(name + '=;' + domain + ' expires=Thu, 10 Mar 1994 01:00:00 UTC; path=' + path);
	this.signal.header('set-cookie', this.header);
};

module.exports = function($){
	$.cookies = new Cookies($);
	$.return();
};