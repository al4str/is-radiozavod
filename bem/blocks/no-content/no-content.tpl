<div class="no-content">
	<form method="post" action="" data-sim="attributes(action,$data.form-action)">
		<i class="no-content__item no-content__icon">
			<svg class="no-content__svg" viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg">
				<g fill="none" stroke="inherit" stroke-miterlimit="10" stroke-width="2">
					<circle cx="36" cy="36" r="35"></circle>
					<line x1="70.65" y1="1.35" x2="1.35" y2="70.65"></line>
				</g>
			</svg>
		</i>
		<p class="no-content__item no-content__message" data-sim="content($data.title)">Нет добавленных для сравнения компаний</p>
		<button class="btn no-content__item no-content__action" type="button" name="" value="" data-sim="if(NOT $data.button-title ? ignore()); attributes(type,$data.button-type); attributes(name,$data.button-name); attributes(value,$data.button-value);">
			<span class="btn__cap" data-sim="content($data.button-title)">Добавить к сравнению</span>
		</button>
	</form>
</div>