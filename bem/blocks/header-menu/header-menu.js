/**
 * Основное меню внешней части сайта
 *
 * @namespace App.c.blocks
 * @class HeaderMenu
 * @returns {object}
 */
App.c.blocks.HeaderMenu = function() {
	var self = this;

	self.opt = {};
	self.dom = {};
	self.show = function() {
		self.dom.$block.style.height = '0px';
		setTimeout(function(){
			self.opt.is_shown = true;
			self.dom.$block.setAttribute('data-is-shown', '');
			self.dom.$block.style.height = self.opt.initial_height + 'px';
			App.h.trigger(self.dom.$block, 'is-shown');
		}, 50);
	};
	self.hide = function() {
		self.opt.is_shown = false;
		self.dom.$block.style.height = '0px';
		App.h.trigger(self.dom.$block, 'is-hidden');
		setTimeout(function() {
			self.dom.$block.removeAttribute('data-is-shown');
		}, 150);
	};
	self.initiate = function() {
		self.dom.$block = App.d.d.getElementById('header-menu');
		if (!self.dom.$block) {
			self = null;
			return false;
		}
		self.opt.is_shown = self.dom.$block.hasAttribute('data-is-shown');
		self.opt.initial_height = self.dom.$block.clientHeight;
		self.dom.$toggle_btn = App.d.d.querySelector('.toggle-menu-btn');
		// self.dom.$toggle_btn.addEventListener('click', self.on_toggle, false);
		if (self.dom.$toggle_btn) {
			self.dom.$toggle_btn.addEventListener('is-shown', self.show, false);
			self.dom.$toggle_btn.addEventListener('is-hidden', self.hide, false);
		}
	};
	self.initiate();
	return {self: self};
};