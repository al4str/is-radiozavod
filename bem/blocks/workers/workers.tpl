<section class="workers">
	<div class="fixed-block">
		{{for own department_name, department of this['workers'].departments :}}
		<div class="island workers__island">
			<header class="island__header">
				<p class="island__title">{{-department_name}}</p>
			</header>
			<ul class="workers__list">
				{{for worker in department :}}
				<li class="workers__item">
					<!--suppress HtmlUnknownTarget -->
					<img class="workers__image" src="graphics-worker-{{-worker.avatar}}.svg" alt="{{-worker.name}}" />
					<p class="workers__name">{{-worker.name}}</p>
					<p class="workers__spec">{{-worker.spec}}</p>
				</li>
				{{end}}
			</ul>
		</div>
		{{end}}
	</div>
</section>