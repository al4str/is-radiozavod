<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>{{- this['html-head'].title }}</title>
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.75, user-scalable=yes" />
	#styles-place#
</head>
<body class="page">