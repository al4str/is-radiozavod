const m = {};

m.fs = require('fs');
m.path = require('path');
m.mime = require('mime');
m.zlib = require('zlib');

const cache = {};

module.exports = function(options) {
	return function($) {
		var pathname = $.url.pathname;
		var mimeType = m.mime.lookup(pathname);
		var extension = m.path.extname(pathname);

		if (!extension) {
			$.return();
			return;
		}
		$.header('Content-Type', mimeType);
		$.status(200);
		var source = options.path + $.url.pathname;

		m.fs.stat(source, function(stat_errors, stats) {
			if (stat_errors) {
				if (stat_errors.type != 'ENOENT') {
					$.status(stat_errors.status || 500, 'File not found');
					$.return();
					return;
				}
				else {
					throw stat_errors;
				}
			}
			//$.header('Last-Modified', stats.mtime);
			//$.header('Expires', new Date(new Date().getTime() + 604800000).toUTCString());
			$.header('Cache-Control', 'public');
			var modified_since = new Date($.headers['if-modified-since']).getTime();
			var last_modified = new Date(stats.mtime).getTime();
			if (!$.headers['if-modified-since'] || last_modified > modified_since) {
				m.fs.readFile(
					source,
					function(read_errors, data) {
						if (read_errors) throw read_errors;
						if (mimeType == 'text/css' || mimeType == 'application/javascript') {
							var buffer = new Buffer(data);

							m.zlib.gzip(buffer, function(zipping_errors, gzip) {
								if (zipping_errors) throw zipping_errors;
								cache[source] = gzip;
								$.header('Content-Encoding', 'gzip');
								$.header('Vary', 'Accept-Encoding');
								$.passed = false;
								$.responded = true;
								$.response.end(gzip);
								$.return();
							})
						} else {
							$.passed = false;
							$.responded = true;
							$.response.end(data);
							$.return();
						}
					}
				);
			}
			else {
				$.status(304);
				$.responded = true;
				$.response.end();
				$.return();
			}
		});
	};
};